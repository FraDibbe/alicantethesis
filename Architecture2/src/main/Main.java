package main;

import resources.Resource;

/** JADE CONTAINER BOOT BASED ON STRING COMMANDS*/
/*commands
 * -nothing (start a Main container)
 * -container
 * -backup
 */

public class Main {

	public static void main(String[] args) {
		
		String[] param = null;

		
		
		if(args != null && args.length <= 0) { //START A Main container with Fault protection
		
		param= new String[7];
		System.out.println("----------- / Starting as a MAIN CONTAINER / --------------");
		
		param[0]="-gui";
		param[1]="-name";
		param[2]="DomoDAI";
		param[3]="-services";
		/**Start Replication [Fault tolerant platform]*/
		param[4]="jade.core.replication.MainReplicationService;jade.core.replication.AddressNotificationService";
		/**Start Topic,Notification and Mobility*/
		//param[2]= "jade.core.messaging.TopicManagementService;jade.core.event.NotificationService;jade.core.mobility.AgentMobilityService";
		param[5]="-agents";
		param[6]="_VocalMager_:voiceAgent.VoiceAgent;";
		
		}
		else //is not a main container
		{
			for(String cmd: args)
			{
				if(cmd.equalsIgnoreCase("-BACKUP")) //BACKUP host
				{
					System.out.println("----------- / Starting as a BACKUP / --------------");
					param= new String[11];
					param[0]="-backupmain"; 
					param[1]="-local-port";
					param[2]="1234";
					param[3]="-host";
					param[4]=Resource.ReplicaIPaddress;
					param[5]="-port";
					param[6]="1099";
					param[7]="-name";
					param[8]="DomoDAI";
					param[9]="-services";
					param[10]="jade.core.replication.MainReplicationService;jade.core.replication.AddressNotificationService;";
				}
				else if(cmd.equalsIgnoreCase("-CONTAINER"))
				{
					System.out.println("----------- / Starting as a CONTAINER / --------------");
					param= new String[10];
					param[0]="-container";
					param[1]="-host";
					param[2]=Resource.HostipAddress;
					param[3]="-port";
					param[4]="1099";
					param[5]="-name";
					param[6]="DomoDAI";
					param[7]="-services jade.core.replication.AddressNotificationService;";
					param[8]="-agents";
					//param[9]="_Touch_:touchAgent.TouchAgent;";
					param[9]="_LightREST_:lightAgent.LightRESTAgent;_LightRasp_:lightAgent.LightRaspAgent;";
				}
				/*
				else if(cmd.equalsIgnoreCase("-CONTAINER2")) //TODO remove
				{
					System.out.println("----------- / Starting as a CONTAINER / --------------");
					param= new String[10];
					param[0]="-container";
					param[1]="-host";
					param[2]=Resource.HostipAddress;
					param[3]="-port";
					param[4]="1099";
					param[5]="-name";
					param[6]="DomoDAI";
					param[7]="-services jade.core.replication.AddressNotificationService;";
					param[8]="-agents";
					param[9]="_Touch_:touchAgent.TouchAgent;";
				}*/
			}
		}
		
		if(param != null)
	 	//start jade with above parameters
	 	jade.Boot.main(param);
		
	}
	
}