package voiceAgent;


import java.util.HashMap;

import basicAgent.BasicAgent;
import resources.Resource;
import resources.Resource.Idioms;
import jade.domain.FIPAAgentManagement.NotUnderstoodException;
import jade.domain.FIPAAgentManagement.RefuseException;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.proto.SimpleAchieveREResponder;

@SuppressWarnings("serial")
public class VocalCommandsResponder extends SimpleAchieveREResponder {

	private BasicAgent myAgent;
	private String elaboratedCommands = "";
	private String service2call;
	
	public VocalCommandsResponder(BasicAgent a, MessageTemplate mt, String service2call) {
		super(a, mt);
		this.myAgent=a;
		this.service2call=service2call;
	}

	@Override
	protected ACLMessage prepareResponse(ACLMessage request)
			throws NotUnderstoodException, RefuseException {
		return null;
	}
	
	protected ACLMessage prepareResultNotification(ACLMessage request, ACLMessage response)
	{
		
		/* ***PROCESSING DELLA VOCE*** */
			
		String VoiceString=request.getContent();
		String[] command_ordered = new String[3];	
		
		String AgentSenderResponseString="Voice Data used!";
		
		//System.out.println("*In vocal Commands Responder**____CLEAN:"+ VoiceString);
		
		HashMap<String,String> vocabulary = (HashMap<String, String>) Resource.getVoiceVocabulary(Idioms.Spanish);
				
		//dividi commandMessage in sotto-stringhe ogni volta che c'� uno spazio " "
		String[] splitted = VoiceString.split(" ");
		
		
		//REST 'command' construction
		for(int k=0; k< splitted.length; k++)
			{
		 	if(vocabulary.containsKey(splitted[k]))
		 		{
		 			String associated_Value=vocabulary.get(splitted[k]);
		 			String values[]=associated_Value.split("-");
		 			int pos=Integer.parseInt(values[1]);
		 			command_ordered[pos]=values[0];
		 		}
			}
		if(command_ordered[0] != null && command_ordered[1] != null && command_ordered[2] != null)
		{		
			for(String elem:command_ordered)
			{
				elaboratedCommands+=elem+":";
			}
			
			elaboratedCommands=elaboratedCommands.substring(0, elaboratedCommands.length()-1);
			myAgent.Initiate_FIPA_SimpleRE(service2call, elaboratedCommands);
			elaboratedCommands="";
		}
		else
		{
			AgentSenderResponseString="Not enough voice data to send a request!";
			System.out.println(AgentSenderResponseString);
		}
	
		/* ***Risponde automaticamente al protocollo FIPA** */
		return myAgent.respondFIPA_RE(request, response, AgentSenderResponseString);
	}

	
	@Override
	public int onEnd() {
		//send the eleborated informatio to some 'HW agent'
		myAgent.Initiate_FIPA_SimpleRE(service2call, elaboratedCommands);
		return super.onEnd();
	}

}
