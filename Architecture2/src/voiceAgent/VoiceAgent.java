package voiceAgent;

import resources.ServicesNamespace;
import jade.core.behaviours.Behaviour;
import jade.lang.acl.MessageTemplate;
import basicAgent.BasicAgent;

public class VoiceAgent extends BasicAgent {

	private static final long serialVersionUID = -2734551475916757147L;

	@Override
	public String[] setService2Search() {
		String[] services={ServicesNamespace.Illumination};
		return services;
	}

	@Override
	public String[] setServiceOffered() {
		String[] services={ServicesNamespace.VocalCommands};
		return services;
	}

	@Override
	public Behaviour[] set_FIPA_ResponderBehaviour() {
		Behaviour[] responders={
				new VocalCommandsResponder(this,MessageTemplate.MatchConversationId(ServicesNamespace.VocalCommands), ServicesNamespace.Illumination),
			  };
		return responders;
	}

	
	@Override
	protected void setup()
	{
		super.setup();
	}
}
