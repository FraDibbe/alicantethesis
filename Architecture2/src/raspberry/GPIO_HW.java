package raspberry;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioFactory;
import com.pi4j.io.gpio.GpioPin;
import com.pi4j.io.gpio.GpioPinDigitalInput;
import com.pi4j.io.gpio.GpioPinDigitalOutput;
import com.pi4j.io.gpio.Pin;
import com.pi4j.io.gpio.PinPullResistance;
import com.pi4j.io.gpio.PinState;

public class GPIO_HW implements GPIO_HW_If{

	private final static int DelayTime=1000; 
	GpioController controller;
	//private Map<String,Pin> providedPin;
	
	
	public GPIO_HW()
	{
		this.controller = GpioFactory.getInstance();
	}
	
	@Override
	public GpioController getController() {
		return this.controller;
	}
	
	@Override
	public void pinLow(Pin pin) {
		GpioPinDigitalOutput pinC=controller.provisionDigitalOutputPin(pin, pin.getName(), PinState.LOW);
		pinC.low();
		commit_delay();
		controller.unprovisionPin(pinC);
	}

	@Override
	public void pinHigh(Pin pin) {
		GpioPinDigitalOutput pinC=controller.provisionDigitalOutputPin(pin, pin.getName(), PinState.LOW);
		pinC.high();
		commit_delay();
		controller.unprovisionPin(pinC);
	}

	@Override
	public void pinToggle(Pin pin) {
		GpioPinDigitalOutput pinC=controller.provisionDigitalOutputPin(pin, pin.getName(), PinState.LOW);
		pinC.toggle();
		commit_delay();
		controller.unprovisionPin(pinC);
	}

	@Override
	public void releaseGPIOcontroller(GpioController controller) {
		this.controller.shutdown();
		commit_delay();
	}

	
	private void commit_delay()
	{
		try {
			Thread.sleep(DelayTime);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
}
