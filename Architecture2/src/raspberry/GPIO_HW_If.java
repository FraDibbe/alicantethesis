package raspberry;

import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioPinDigitalInput;
import com.pi4j.io.gpio.GpioPinDigitalOutput;
import com.pi4j.io.gpio.Pin;
import com.pi4j.io.gpio.PinPullResistance;
import com.pi4j.io.gpio.PinState;

public interface GPIO_HW_If {

	public GpioController getController();
	public void pinLow(Pin pin);
	public void pinToggle(Pin pin);
	public void pinHigh(Pin pin);
	public void releaseGPIOcontroller(GpioController controller);
}
