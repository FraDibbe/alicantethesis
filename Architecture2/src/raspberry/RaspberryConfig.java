package raspberry;

import java.util.HashMap;

import com.pi4j.io.gpio.Pin;
import com.pi4j.io.gpio.RaspiPin;

public class RaspberryConfig {
	
	public static HashMap<String,Pin> PinConfiguration; //a map between deviceId and Raspberry physical pin
	static
    {

		RaspberryConfig.PinConfiguration= new HashMap<String,Pin>();

		PinConfiguration.put("15", RaspiPin.GPIO_01); //luz cocina
		PinConfiguration.put("29", RaspiPin.GPIO_02); //luz sala
		PinConfiguration.put("32", RaspiPin.GPIO_03); //persiana dereche
    }
											

	
}
