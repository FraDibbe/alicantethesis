package lightAgent;


import java.io.IOException;

import Client.DeviceCommandClient;
import Client.exception.AuthRequiredException;
import basicAgent.BasicAgent;
import jade.core.behaviours.OneShotBehaviour;


@SuppressWarnings("serial")
public class CommandWorkBeh extends OneShotBehaviour{

	private DeviceCommandClient<String> client;
	private BasicAgent myAgent;
	private String commands;
	
	public CommandWorkBeh(BasicAgent a, String Commands) {
		super(a);
		this.myAgent=a;
		this.commands=Commands;
	}
	
	@Override
	public void action() {
		
		//try to get connection from the agent  (this should be __fixed__(nel senso di un pasaggio obbligatorio tra i 2 behav-conncet e azione-) in an Sequental behav?)
		client=(DeviceCommandClient<String>)myAgent.getfromAgentStore("CommandClient");
		
		//Send REST request
		if(client != null){
	
			String[] subCommands=commands.split(":"); //"parse" commands
	
			try {
			//send the command to DAI Middleware
				client.giveCommand(Integer.parseInt(subCommands[1]), subCommands[0], subCommands[2]);
		
			} catch (NumberFormatException | IOException | AuthRequiredException e) {
				e.printStackTrace();
			}
					
		};
		
	
	}

}
