package touchAgent;

import edu.wpi.rail.jrosbridge.Ros;
import edu.wpi.rail.jrosbridge.messages.Message;
import basicAgent.BasicAgent;
import resources.Resource;
import resources.ServicesNamespace;
import ros.ROSCallback;
import ros.RosClient;
import jade.core.behaviours.OneShotBehaviour;


@SuppressWarnings("serial")
public class TouchWorkBeh extends OneShotBehaviour{

	private RosClient client;
	private BasicAgent myAgent;
	
	public TouchWorkBeh(BasicAgent a) {
		super(a);
		this.myAgent=a;
	}
	
	@Override
	public void action() {
		
		//try to get connection from the agent  (this should be __fixed__(nel senso di un pasaggio obbligatorio tra i 2 behav-conncet e azione-) in an Sequental behav?)
		client=new RosClient((Ros)myAgent.getfromAgentStore("ROS"));
		
		if(client != null){
		
			//subscribe to ROS topics
			client.subscribeROStopic(Resource.Touch_Airos_topicName, Resource.Touch_Airos_messageType, new ROSCallback<Message>() {
				@Override
				public void handleROStopic(Message message) {
					//TODO: here MESSAGE can be elaborated
						//then....
					//...create a new fipa initiator conversation	
					myAgent.Initiate_FIPA_SimpleRE(ServicesNamespace.Illumination, message.toString());
					
				}});
		
		}
	}

}
