package touchAgent;

import resources.ServicesNamespace;
import ros.AisoyConnectorBeh;
import jade.core.behaviours.Behaviour;
import basicAgent.BasicAgent;

public class TouchAgent extends BasicAgent {

	/////////////////////////////
	// SET UP
	//////////////////////////
	private static final long serialVersionUID = -2734551475916757147L;

	@Override
	public String[] setService2Search() {
		String[] services={ServicesNamespace.Illumination};
		return services;
	}

	@Override
	public String[] setServiceOffered() {
		return null;
	}

	@Override
	public Behaviour[] set_FIPA_ResponderBehaviour() {
		return null;
	}

	
	/////////////////////////////
	// MAIN
	//////////////////////////
	@SuppressWarnings("serial")
	@Override
	protected void setup()
	{
		super.setup();
		
		//create the sequential Behaviour to connect and send messages to ROS (subscribe to a topic)
		//TODO: NOW THEM ARE SEPARATED
		
		addBehaviour(new AisoyConnectorBeh(this){
	          @Override
	          public int onEnd() { //in this way agent and behaviurs are not coupled
	                  int ret = super.onEnd();
	                  addtoAgentStore("ROS", getResult()); //add it to the internal agent store
	                  return ret;
	          }         
		    });
	   
		 addBehaviour(new TouchWorkBeh(this));
	/*	 
		addBehaviour(new TickerBehaviour(this, 4000) {
				
				@Override
				protected void onTick() {
					Initiate_FIPA_SimpleRE(ServicesNamespace.Illumination, "{\"widget\": on}");					
				}
			});*/
		 }
		
}
