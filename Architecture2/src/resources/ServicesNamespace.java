package resources;

public final class ServicesNamespace {

	/* SERVICE IDs for application agents	*/
	public static final String Illumination="srv:Illumination";
	public static final String ASR="srv:ASR";
	public final static String VocalCommands="grp02:s-n";

	
	
	/* JADE topics IDs */
	public static final String ASR_JADE_Topic="tpc:asrJADE";
	
	public static final String restLOG_JADE_Topic="tpc:rest:LogJADE";
	public static final String Touch_JADE_Topic = "tpc:touchJADE";
}
