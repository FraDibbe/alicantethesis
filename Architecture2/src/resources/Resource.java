package resources;

import java.util.HashMap;
import java.util.Map;

/*It's a class of Resources. 
A Resorce is treated as a "define", in terms of C, and it was used here in the same way.

*/

public class Resource {

	public enum Idioms { //TODO: ADD MORE
	   English, Spanish
	}

		/* DEFINE for "SERVER" REMOTE CONTAINER	*/
	public static final String HostipAddress="10.41.4.3";
	public static final String ReplicaIPaddress="10.41.4.3";

	/* AIROS 4 services and topic names*/
	public static final String ASR_Airos_topicName="/airos4/asr/recognition";
	public static final String ASR_Airos_messageType="std_msgs/String";
	
	public static final String Touch_Airos_topicName="/airos4/touch/touch";
	public static final String Touch_Airos_messageType="airos4_msgs/Touch";
	
	public static final String IsSpeaking_Airos_topicName="/airos4/tts/is_speaking";
	public static final String IsSpeaking_messageType="std_msgs/Bool";
	
	public static final String Say_Airos_ServiceName="/airos4/tts/say";
	public static final String Say_Airos_messageType="airos4_msgs/Say";
	
	/*REST WEB SERVICEs*/
	public static final String Server_Hostname="shanon.iuii.ua.es/s/rest/";
	public static final String username="fra.dibenedetto1@gmail.com";
	public static final String password="27uaj45lM";
	
	/*NOTE: the Map is String to (String-Int). The first one is rapresent the command that the person need to say,
	*the pair (String-Int) instead rapresents: 
	*	- String: the command to be submitted to REST Web Service
	*   - Int. the position in the command. Command is a 3 place string, where each pices are separated by ':' and them means
	*   		func:deviceId:value
		So saying "switch-0" means 'switch' is the func of the command because it is in the 0th position
	*/
	private static HashMap<String,String> vocabularyEng; 
	private static HashMap<String,String> vocabularySpan;
	static
    {
    	Resource.vocabularyEng= new HashMap<String,String>();
		Resource.vocabularySpan= new HashMap<String,String>();
		/* ***fill the vocabularies*/	
												/**-English*/
		vocabularyEng.put("light", "switch-0");
		
		
												/**-Spanish*/
		//func
		vocabularySpan.put("luz", "switch-0");
		vocabularySpan.put("persiana", "moveBlind-0");
		
		//devices
		vocabularySpan.put("televisor", "17-1"); //luz
		vocabularySpan.put("sala", "29-1"); //luz
		vocabularySpan.put("cocina", "15-1"); //luz
		vocabularySpan.put("derecha", "32-1"); //persiana
		vocabularySpan.put("izquierda", "31-1"); //persiana
		
		
		//values 
		vocabularySpan.put("encender", "true-2"); //luz
		vocabularySpan.put("apagar", "false-2"); //luz
		vocabularySpan.put("bajar", "2-2");  //persiana
		vocabularySpan.put("levantar", "1-2"); //persiana
		vocabularySpan.put("parar", "0-2"); //persiana	
	}
	
	
	private static HashMap<String,String> getVocabularyEng()
	{
		return vocabularyEng;	
	}
	
	private static HashMap<String,String> getVocabularySpan()
	{
		return vocabularySpan;	
	}
	
	public static Map<String, String> getVoiceVocabulary(Idioms language)
	{
		HashMap<String,String> vocabulary=null;
		
		switch(language)
		{
		case English:
			vocabulary= getVocabularyEng();
			break;
			
		case Spanish:
			vocabulary= getVocabularySpan();
			break;
		}
		
		return vocabulary;
	}
	
}