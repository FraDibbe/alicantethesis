package lightAgent;

import com.pi4j.io.gpio.Pin;
import com.pi4j.io.gpio.PinState;

import raspberry.GPIO_HW;
import raspberry.RaspberryConfig;
import basicAgent.BasicAgent;
import jade.domain.FIPAAgentManagement.NotUnderstoodException;
import jade.domain.FIPAAgentManagement.RefuseException;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.proto.SimpleAchieveREResponder;




@SuppressWarnings("serial")
public class IlluminationHWResponder extends SimpleAchieveREResponder {

	private BasicAgent myAgent;
	
	public IlluminationHWResponder(BasicAgent a, MessageTemplate mt) {
		super(a, mt);
		this.myAgent=a;
	}

	@Override
	protected ACLMessage prepareResponse(ACLMessage request)
			throws NotUnderstoodException, RefuseException {
		return null;
	}
	
	protected ACLMessage prepareResultNotification(ACLMessage request, ACLMessage response)
	{
		/* *** ATTIVAZIONE LUCE VIA WORKER HW BEH ***/
		
		//Light commands are in the form '  func:deviceId:value  '
		String commands=request.getContent();
		
		String[] commandsPart = commands.split(":");
	
		if(commandsPart.length>2)
		{		
			//HW interaction
			PinState action;
			GPIO_HW gpio = new GPIO_HW();
			
			//func type conversion
			if( commandsPart[2].equalsIgnoreCase("TRUE") ) action=PinState.HIGH;
			else action=PinState.LOW;
		
			//device Id conversion
			Pin pin = RaspberryConfig.PinConfiguration.get(commandsPart[1]);
		
			
			//Actuate on HW
			if(action.equals(PinState.HIGH)) gpio.pinHigh(pin);
			else if(action.equals(PinState.LOW)) gpio.pinLow(pin);
			
			
			
			return myAgent.respondFIPA_RE(request, response, "Voice Data used!");
		}
		/* ***Risponde automaticamente al protocollo FIPA** */
		return myAgent.respondFIPA_RE(request, response, "Voice Data don't used (commands too shorts)!");
	}

}
