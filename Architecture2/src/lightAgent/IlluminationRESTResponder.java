package lightAgent;

import basicAgent.BasicAgent;
import jade.domain.FIPAAgentManagement.NotUnderstoodException;
import jade.domain.FIPAAgentManagement.RefuseException;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.proto.SimpleAchieveREResponder;

@SuppressWarnings("serial")
public class IlluminationRESTResponder extends SimpleAchieveREResponder {

	private BasicAgent myAgent;
	
	public IlluminationRESTResponder(BasicAgent a, MessageTemplate mt) {
		super(a, mt);
		this.myAgent=a;
	}

	@Override
	protected ACLMessage prepareResponse(ACLMessage request)
			throws NotUnderstoodException, RefuseException {
		return null;
	}
	
	protected ACLMessage prepareResultNotification(ACLMessage request, ACLMessage response)
	{
		/*
		/* ***PROCESSING DEL TESTO DEL TOUCH PER ATTIVAZIONE LUCE VIA WORKER BEH /
		Map<String,String> touchData; 
		boolean isChangeNeeded=false;
		
		touchData= new HashMap<String,String>(JSONparser.flattern(request.getContent()));
		
		String valueH=touchData.get("head");
		String valueR=touchData.get("right");
		String valueL=touchData.get("left");
		
		String commands="switch:";
		
		if(valueH.equalsIgnoreCase("true"))
		{
			commands+="Panel:true";
			isChangeNeeded=true;
		}
		else if(valueR.equalsIgnoreCase("true"))
		{
			commands+="15:false";	
			isChangeNeeded=true;
		}
		else if(valueL.equalsIgnoreCase("true"))
		{
			commands+="15:true";	
			isChangeNeeded=true;
		}
		if(isChangeNeeded)myAgent.addBehaviour(new CommandWorkBeh(myAgent, commands));
		*/
		String commands=request.getContent();
		
		//activate HW behaviour
		myAgent.addBehaviour(new CommandWorkBeh(myAgent, commands));
		
		//debug only: System.out.println("Agent: "+myAgent.getLocalName()+ " recived in the responder "+ request.getContent() );
		
		/* ***Risponde automaticamente al protocollo FIPA** */
		return myAgent.respondFIPA_RE(request, response, "Touch Data used!");
	}

}
