package lightAgent;

import resources.Resource;
import resources.ServicesNamespace;
import restBeh.RESTconnFactoryBeh;
import restBeh.RESTconnFactoryBeh.REST_Client_Types;
import jade.core.behaviours.Behaviour;
import jade.lang.acl.MessageTemplate;
import basicAgent.BasicAgent;

public class LightRESTAgent extends BasicAgent {

	private static final long serialVersionUID = -2734551475916757147L;

	@Override
	public String[] setService2Search() {
		String[] services={ServicesNamespace.VocalCommands};
		return services;
	}

	@Override
	public String[] setServiceOffered() {
		String[] services={ServicesNamespace.Illumination};
		return services;
	}

	@Override
	public Behaviour[] set_FIPA_ResponderBehaviour() {
		Behaviour[] responders={
				new IlluminationRESTResponder(this,MessageTemplate.MatchConversationId(ServicesNamespace.Illumination)),
			  };
		return responders;
	}

	
	@SuppressWarnings("serial")
	@Override
	protected void setup()
	{
		super.setup();
		
		addBehaviour(new RESTconnFactoryBeh(REST_Client_Types.DEV_COMMANDS,Resource.Server_Hostname,
											Resource.username,Resource.password){
	          @Override
	          public int onEnd() { //in this way agent and behaviurs are not coupled
	                  int ret = super.onEnd();
	                  addtoAgentStore("CommandClient", getResult()); //add it to the internal agent store
	                  return ret;
	          }         
		    });
		addBehaviour(new RESTconnFactoryBeh(REST_Client_Types.DEVICES,Resource.Server_Hostname,
				Resource.username,Resource.password){
				@Override
				public int onEnd() { //in this way agent and behaviurs are not coupled
				int ret = super.onEnd();
				addtoAgentStore("DeviceClient", getResult()); //add it to the internal agent store
				return ret;
				}         
});
	}
}
