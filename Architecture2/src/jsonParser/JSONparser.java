package jsonParser;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

public class JSONparser {
	
	
	
	public static Map<String,String> flattern(String toParse)
	{

		Map<String,String> parsedObject=new HashMap<String,String>();
		
	    JSONObject object = (JSONObject) JSONValue.parse(toParse);

	    // then
	    @SuppressWarnings("unchecked")
	    Set<String> keySet = object.keySet();
	    for (String key : keySet) {
	        Object value = object.get(key);
	        if(!value.getClass().getSimpleName().equalsIgnoreCase("JSONObject"))
	        {
	        	parsedObject.put(key, value.toString());
	        }
	        else
	        {
	        	parsedObject.putAll(JSONparser.flattern(value.toString()));
	        }
	    }
	    
	   	    
		return parsedObject;
	}
	
	
}



