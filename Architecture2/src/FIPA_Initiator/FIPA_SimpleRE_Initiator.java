package FIPA_Initiator;

import jade.core.Agent;
import jade.lang.acl.ACLMessage;
import jade.proto.SimpleAchieveREInitiator;

/*Is need for the FIPA Archive-RE protocol, the initiator is used to to handle the Inform and Refuse message from the Responder Agent
This class is unique for all the agent that partecipate to the Archive-RE protocol.
*/

@SuppressWarnings("serial")
public class FIPA_SimpleRE_Initiator extends SimpleAchieveREInitiator {

	Agent a;
	public FIPA_SimpleRE_Initiator(Agent a, ACLMessage msg) {
		super(a, msg);
		this.a=a;
	}
	

	@Override
	protected void handleInform(ACLMessage inform) {
		super.handleInform(inform);
		System.out.println("** "+myAgent.getLocalName()+" INITIATOR **: ho ricevuto ' " + inform.getContent() +"' da " +inform.getSender().getLocalName());
		a.removeBehaviour(this);
	
	}
	
	
	
	@Override
	protected void handleRefuse(ACLMessage msg) {
		System.out.println(this.myAgent.getLocalName()+": � stata rifiutata la mia richiesta da" + msg.getSender().getLocalName());		
		a.removeBehaviour(this);
	}
}


