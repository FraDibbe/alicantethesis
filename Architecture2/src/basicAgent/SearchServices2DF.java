package basicAgent;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.WakerBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;

/*This class register services at an application agent. It is developed like a waker behavior, but every 2 minutes it restarts. 
* So an agent can discovery, and register, new services -and new agents- while the application is running   */
@SuppressWarnings("serial")
public class SearchServices2DF extends WakerBehaviour {

	private String[] service2Search;
	private Map<String,List<AID>> services = new HashMap<String,List<AID>>();
	
	
	public SearchServices2DF(Agent a,long timeout, String[] service2Search,Map<String,List<AID>> services) {
		super(a, timeout);
		this.service2Search=service2Search;
		this.services = services;
	}
	
	
	
	public Map<String, List<AID>> getResult()
	{
		return services;
	}
	
	
	@Override
	public void onWake() {
		
		//System.out.println(" ** -> Waker di ricerca servizi per l'agente " + myAgent.getLocalName() +"  attivato");

		//TODO: COTROLLO SU NULL DEGLI ARRAY!!! SE � NULL NON FARE questo
		if(service2Search != null)
		{
		//ricerco nel DF ogni servizio impostato in 'service2Search'
			for (int i=0; i< service2Search.length;i++){
				DFAgentDescription dfd = new DFAgentDescription();
				
				ServiceDescription sdreq = new ServiceDescription();
				sdreq.setType(service2Search[i]);
				dfd.addServices(sdreq);
				//ricerca nel DF un ServiceDescription uguale a 'sdreq'

				try {
					DFAgentDescription[] res = DFService.search(myAgent, dfd);

					if(res != null) {
						String message=myAgent.getLocalName();
						
							for(int k=0; k< res.length;k++){ 
									//aggiungo il servizio se e sono se esso non � gi� presente 
								
									if(!services.containsKey(service2Search[i])){ //new list
										List<AID> l = new ArrayList<AID>();
										l.add(res[k].getName());
										services.put(service2Search[i], l);
										message+=": Aggiunto servizio " + service2Search[i] + " fornito da "+ res[k].getName();
										System.out.println(message);
										}
									else //update list
										{
										List<AID> l=services.get(service2Search[i]);
										if(!l.contains(res[k].getName()))
										{
										l.add(res[k].getName());
										services.replace(service2Search[i], l);
										message+=": Aggiunto servizio " + service2Search[i] + " fornito da "+ res[k].getName();
										System.out.println(message);
										}
										}
									
									}
								}	
				} catch (FIPAException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
								
				reset(15000); //1 min sleep				
	}
	
}