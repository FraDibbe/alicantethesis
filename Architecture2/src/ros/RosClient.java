package ros;

import edu.wpi.rail.jrosbridge.Ros;
import edu.wpi.rail.jrosbridge.Service;
import edu.wpi.rail.jrosbridge.Topic;
import edu.wpi.rail.jrosbridge.callback.TopicCallback;
import edu.wpi.rail.jrosbridge.messages.Message;
import edu.wpi.rail.jrosbridge.services.ServiceRequest;
import edu.wpi.rail.jrosbridge.services.ServiceResponse;

public class RosClient implements RosClientIf {

	private  Ros rosConnection;
	//private final static String DEFAULT_HOST ="aisoy1.local";
	//private String host;
	
	public RosClient(Ros connection) {
		this.rosConnection=connection;
	}

	
	/*
	 * public RosClient(String host)
	{
		this.host=host;
	}
	
	
	public static Ros getClientIstance() {
			String toConnect;
		   if(host != null) toConnect=host;
		   else toConnect=DEFAULT_HOST;
		
		    rosConnection = new Ros(toConnect);
		    return rosConnection;
	}*/
	
	@Override
	public void subscribeROStopic(String topic, String msgType,final ROSCallback<Message> callback) {
		//qua ci va l'implementazione che sta dentro il Beheviour
		
		 if( rosConnection != null && rosConnection.isConnected() )
			{
				Topic listener = new Topic(rosConnection, topic, msgType);
			    listener.subscribe(new TopicCallback() {	
					public void handleMessage(Message message) {
						callback.handleROStopic(message);
					}
				});
			}
	
	}


	@Override
	public boolean isConnectionEstablished() {
		return rosConnection.isConnected();
	}


	@Override
	public void callROSservice(String service, String srvType, String JSONreq,
			ROSCallback<ServiceResponse> callback) {
		
		 if( rosConnection != null && rosConnection.isConnected() )
			{
				Service myService = new Service(rosConnection, service, srvType);
			    ServiceRequest request = new ServiceRequest(JSONreq);
			    ServiceResponse response = myService.callServiceAndWait(request);
			    
			    callback.handleROStopic(response);
				
			}
		
	}





}
