package classes;

import inteface.AisoyCallInterface;

public class AisoyManager {
	
	private AisoyCallInterface ie;
	private boolean somethingHappened;
	
	public AisoyManager(AisoyCallInterface event)
	{
		// Save the event object for later use.
		ie = event;
		//nothing happened right now
		somethingHappened=false;
	}
	
	
	public void callService()
	{
		//execute code
		
		//something happened
		somethingHappened=true;
		
		//notify
		if(somethingHappened)
		{
			ie.ASRCallback();
		}
	}
	
}
