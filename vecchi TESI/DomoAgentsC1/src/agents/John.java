package agents;

import jade.core.Agent;
import jade.core.behaviours.TickerBehaviour;

public class John extends Agent {

	private static final long serialVersionUID = -2858064942726090797L;

	@Override
	public void setup()
	{
		System.out.println("Hello! Agent"+getAID().getName()+ " is ready" + "And i'm : "+ here().getAddress());
		
		addBehaviour(new TickerBehaviour(this,5000) {
			private static final long serialVersionUID = -7434425001141212347L;

			@Override
			protected void onTick() {
				System.out.println("Agent"+myAgent.getName()+ " is Ticking!");
			}
		});
	}
	
}
