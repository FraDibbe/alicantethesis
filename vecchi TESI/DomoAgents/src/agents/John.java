package agents;

import java.util.LinkedList;
import java.util.List;

import jade.core.Agent;

public class John extends Agent {

	private static final long serialVersionUID = -2858064942726090797L;

	private List<String> myList;
	
	public John()
	{
		super();
		myList = new LinkedList<String>();
	}
	
	public List<String> getList(){
		return myList;		
	}
	
	public void setList(List<String> Li)
	{
		this.myList=Li;
	}
	
	
	@Override
	public void setup()
	{
		System.out.println("Hello! Agent"+getAID().getName()+ " is ready" + "And i'm : "+ here().getAddress());
		
		addBehaviour(new Behav(this,5000));
	}
	
}
