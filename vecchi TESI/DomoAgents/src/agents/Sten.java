package agents;

import jade.core.Agent;
import jade.core.behaviours.TickerBehaviour;

public class Sten extends Agent {

	private static final long serialVersionUID = -4578165790608647306L;

	@Override
	public void setup()
	{
		System.out.println("Hello! Agent"+getAID().getName()+ " is ready" + "And i'm : "+ here().getAddress());
		
		addBehaviour(new TickerBehaviour(this,1000) {
			private static final long serialVersionUID = -2577260292302140230L;

			@Override
			protected void onTick() {
				System.out.println("Agent"+myAgent.getName()+ " is Ticking!");
			}
		});
	}
	
}
