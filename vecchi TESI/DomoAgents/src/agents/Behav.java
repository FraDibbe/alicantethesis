package agents;

import jade.core.Agent;
import jade.core.behaviours.TickerBehaviour;

import java.util.List;

public class Behav extends TickerBehaviour {
	
	John agent;
	
	public Behav(Agent a, long period) {
		super(a, period);
		this.agent = (John)a;
	}

	private static final long serialVersionUID = -7434425001141212347L;

	@Override
	protected void onTick() {
		List<String> l=agent.getList();
		l.add("1");
		agent.setList(l);
		System.out.println("Agent"+agent.getName()+ " List is: "+l.toString());
	}
}
