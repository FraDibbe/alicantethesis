package agents;

import java.util.LinkedList;
import java.util.List;

import jade.core.Agent;
import jade.core.behaviours.OneShotBehaviour;
import jade.core.behaviours.TickerBehaviour;

public class JohnData extends Agent {

	private static final long serialVersionUID = -2858064942726090797L;

	private List<String> myList;
	
	public JohnData()
	{
		super();
		myList = new LinkedList<String>();
	}
	
	
	@SuppressWarnings("serial")
	@Override
	public void setup()
	{
		System.out.println("Hello! Agent"+getAID().getName()+ " is ready" + "And i'm : "+ here().getAddress());
		
		
		addBehaviour(new TickerBehaviour(this, 1000) {
			
			@Override
			protected void onTick() {
				myList.add("1");
				System.out.println("Agent"+myAgent.getName()+ " List is: "+myList.toString());
			}
		});
	}
	
}
