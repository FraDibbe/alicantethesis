package launch;

import resources.Resource;

public class Main {

	public static void main(String[] args) {
		// i parametri commentati servono a fare partire JADE in locale, gli altri sono usati per far partire jade come container su macchina remota (Services.HostipAddres)
		String[] param = new String[2];
		param[0]="-gui";
		//param[0]="-container";
	//	param[1]="-host";
	//	param[2]=Resource.HostipAddress;
	 	param[1]="JohnAg:agents.John";
		//param[1]="grp02FlexSender:agents.AgentFlex;grp02Navigation:agents.AgentNavigation;Air;grp02Smarthy:agents.AgentSmartphone";
		jade.Boot.main(param);
		
	}
	
}