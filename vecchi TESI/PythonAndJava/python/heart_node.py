#!/usr/bin/env python

from airos4.heart import Heart
#from heart_dummy import HEART
import rospy
import roslib
roslib.load_manifest('airos4_msgs')
from airos4_msgs.srv import SetColor, SetColorResponse
from airos4_msgs.srv import GetHeartColor, GetHeartColorResponse
from airos4_msgs.srv import HeartBeat, HeartBeatResponse
from airos4_msgs.srv import HeartFlashColor, HeartFlashColorResponse
from airos4_msgs.srv import StopAll, StopAllResponse

class AisoyHeart(object):
	"""
	Ros node that make calls the python heart library
	"""
	def __init__(self):
		rospy.on_shutdown(self.shutdown)	# Cleanup when termniating the node
		rospy.Service( rospy.get_param("~service_set_color"), SetColor, self.SetColorHandler )
		rospy.Service( rospy.get_param("~service_get_color"), GetHeartColor, self.GetHeartColorHandler )
		rospy.Service( rospy.get_param("~service_heart_beat"), HeartBeat, self.HeartBeatHandler )
		rospy.Service( rospy.get_param("~service_flash_color"), HeartFlashColor, self.HeartFlashColorHandler )
		rospy.Service( rospy.get_param("~service_stop_all"), StopAll, self.StopAllHandler )
		self.heart=Heart()

	def SetColorHandler(self, req):
		try:
			self.heart.setColor(req.red, req.green, req.blue, req.time)	
		except Exception, e:
			rospy.logerr(e)
			return SetColorResponse(False)
		return SetColorResponse(True)

	def GetHeartColorHandler(self, req):
		try:
			color = self.heart.getColor()	
		except Exception, e:
			rospy.logerr(e)
			return GetHeartColorResponse([])
		return GetHeartColorResponse(color[0],color[1],color[2])

	def HeartBeatHandler(self, req):
		try:
			self.heart.heart_beat(req.red, req.green, req.blue, req.time, req.wait)	
		except Exception, e:
			rospy.logerr(e)
			return HeartBeatResponse(False)
		return HeartBeatResponse(True)

	def HeartFlashColorHandler(self, req):
		try:
			self.heart.flash_color(req.red, req.green, req.blue, req.time_color, req.time_flash)	
		except Exception, e:
			rospy.logerr(e)
			return HeartFlashColorResponse(False)
		return HeartFlashColorResponse(True)

	def StopAllHandler(self, req):
		try:
			self.heart.stop_all()	
		except Exception, e:
			rospy.logerr(e)
			return StopAllResponse(False)
		return StopAllResponse(True)

	def shutdown(self): # Stop Node
		try:
			rospy.loginfo("Stopping heart_node...")
		except:
			pass

if __name__ == '__main__':
	try:
		rospy.init_node('heart_node')
		heart=AisoyHeart()
		rospy.spin()
	except Exception, e:
		rospy.logerr(e)



