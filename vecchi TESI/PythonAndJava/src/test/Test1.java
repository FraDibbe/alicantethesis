package test;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

import org.python.core.PyInstance;
import org.python.core.PyString;
import org.python.util.PythonInterpreter;
 
 
public class Test1
{
 
   PythonInterpreter interpreter = null;
 
 
   public Test1()
   {
      PythonInterpreter.initialize(System.getProperties(),
                                   System.getProperties(), new String[0]);
 
      this.interpreter = new PythonInterpreter();
   }
 
   void execfile( final InputStream in )
   {
      this.interpreter.execfile(in);
   }
   
   /*void execfile( final String in )
   {
      this.interpreter.execfile(in);
   }*/
 
   PyInstance createClass( final String className, final String opts )
   {
      return (PyInstance) this.interpreter.eval(className + "(" + opts + ")");
   }
 
   public static void main( String gargs[] ) throws FileNotFoundException
   {
      Test1 ie = new Test1();
 
    // ie.execfile("python/heart_node.py");
      
      InputStream in = new FileInputStream("/opt/ros/indigo/lib/airos4_heart/heart_node.py");
     
      ie.execfile(in);
      
      PyInstance touchNode = ie.createClass("TEST", "None");
 
      touchNode.invoke("SetColorHandler",new PyString("0 0 255 1"));
   }
}