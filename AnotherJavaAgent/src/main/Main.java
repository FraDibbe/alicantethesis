package main;

import resources.Resource;

public class Main {

	public static void main(String[] args) {
		// start a Main Container with one agent (called JohnAg) @ localhost
		String[] param = new String[5];
		//to start a normal Container: param[0]="-container"; //	param[1]="-host"; //param[2]=Resource.HostipAddress;
		param[0]="-gui";
		param[1]="-services";
		param[2]= "jade.core.messaging.TopicManagementService;jade.core.event.NotificationService;jade.core.mobility.AgentMobilityService";
		param[3]="-agents";
		param[4]="_ASR_:asrAgent.ASR_Agent;_Log_:logAgent.Log_Agent";
		
	 	//start jade with above parameters
	 	jade.Boot.main(param);
		
	}
	
}