package resources;

public class Resource {

/*It's a class of Resources. 
A Resorce is treated as a "define", in terms of C, and it was used here in the same way.

*/

		/* DEFINE for SERIAL PORT OF THE PANDABOARD	*
	public static final String SerialPort="/dev/ttyO3";
	*/
		/* DEFINE for "SERVER" REMOTE CONTAINER	*/
	public static final String HostipAddress="90.147.45.118";


	/* AIROS 4 services and topic names*/
	public static final String ASR_Airos_topicName="/airos4/asr/recognition";
	public static final String ASR_Airos_messageType="std_msgs/String";
	
	public static final String Touch_Airos_topicName="/airos4/touch/touch";
	public static final String Touch_Airos_messageType="airos4_msgs/Touch";
	
	public static final String IsSpeaking_Airos_topicName="/airos4/tts/is_speaking";
	public static final String IsSpeaking_messageType="std_msgs/Bool";
	
	public static final String Say_Airos_ServiceName="/airos4/tts/say";
	public static final String Say_Airos_messageType="airos4_msgs/Say";
	
	/*REST WEB SERVICEs*/
	public static final String Server_Hostname="shanon.iuii.ua.es/s/rest/log";
	public static final String username="fra.dibenedetto1@gmail.com";
	public static final String password="27uaj45lM";
	
}