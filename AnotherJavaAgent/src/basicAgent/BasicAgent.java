package basicAgent;


import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.Vector;

import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

/**
 * <b>BasicAgent</b> � esteso da tutte le altre classi Agente.
 * Fornisce i Behaviours base per ogni agente che sono fondamentalmente le operazioni base per registrare e cercare servizi presso un DF
 * e per rimuovere dal DF tali servizi dopo la "morte" dell'agente.
 * <ul>
 * <>
 * <>
 *</ul>
 * Quando un agente estende il basic agent è forzato ad implementare i metodi abstract:
 *	setService2Search();
 *	setServiceOffered();
 *	setResponderBehaviour();
 * e quindi a partecipare correttamente al protocollo di comunicazione Servizi Offerti/ Servizi Ricercati qui adottato 
 *
 */

@SuppressWarnings("serial")
public abstract class BasicAgent extends Agent implements ServicesInterface {
	//Insieme di service providers, è una MAP servizio <-> AID 
	protected Map<String,AID> serviceProviders = new HashMap<String,AID>();
	protected String[] service2Search;
	protected String[] serviceOffered;
	private Map<String,Object> AgentStore= new HashMap<String,Object>(); //an array that acts as common memory between agents and behaviours
	
	@Override
	protected void setup()
	{
		service2Search=setService2Search();
		serviceOffered=setServiceOffered();
		if(serviceOffered != null)  this.registerService2DF();

		this.addBehaviour(new SearchServices2DF(this, 0,service2Search, serviceProviders) {
			            @Override
			            public int onEnd() { 
			                    int ret = super.onEnd();
			                    serviceProviders = getResult();//get the service providers HashMap from the behaviur
			                    return ret;
			            }
					});


		System.out.println("*****************************");
		System.out.println("******-- "+ this.getLocalName() + " started! -- ********");
		System.out.println("*****************************");
		System.out.println("");
	}

///////////////////////////////////////////
///// Access to Service Provider Map /////	
//////////////////////////////////////////		
	public AID getServiceProvider(String service){
		return serviceProviders.get(service);
	}

	public Vector<AID> getAllServiceProviders(String service){
		Vector<AID>  servers = new Vector<AID>();

		Set<String> list  = serviceProviders.keySet();
		if(list != null && !list.isEmpty()){
			Iterator<String> iter = list.iterator();

			while(iter.hasNext() && iter!=null) {
				String key = (String) iter.next();
				if(key.equals(service))   servers.add(serviceProviders.get(key)); 
			}

		}
		return servers;
	}

///////////////////////////////////////////
///// AgentStore Map methods /////	
//////////////////////////////////////////	
	 public void addtoAgentStore(String Key,Object obj) { 
		 this.AgentStore.put(Key, obj);
		}

	public Object getfromAgentStore(String key) {
			return this.AgentStore.get(key);
		}
	
	public boolean removefromAgentStore(String key) {
		 if(this.AgentStore.remove(key) != null) return true;
		 else return false;
	}

///////////////////////////////////////////
///// Standard Register/Deregister to DF //	
//////////////////////////////////////////
	
protected void registerService2DF(){

		DFAgentDescription dfd = new DFAgentDescription();

		//crea un ServiceDescription per ogni servizio presente 'serviceOffered'

		for (int i=0; i< serviceOffered.length;i++){

			ServiceDescription sd = new ServiceDescription();
			sd.setType(serviceOffered[i]);
			sd.setName(serviceOffered[i]);
			dfd.addServices(sd);
		}
		//registra ogni servizio presente in 'serviceOffered'
		try {
			DFService.register(this, dfd);
			System.out.println(this.getLocalName()+": I servizi ");
			@SuppressWarnings("rawtypes")
			Iterator it=dfd.getAllServices();

			while(it.hasNext()){
				ServiceDescription element = (ServiceDescription)it.next();
				System.out.println(element.getName() + " ");
			}
			System.out.println("sono stati registrati");
			System.out.println("-----------------------------------");
		}
		catch (FIPAException fe) {
			fe.printStackTrace();
		}
	}

@Override
protected void takeDown() {
	//Every agent who dies has to deregister first it's services to the DF
	if(serviceOffered != null){
		DFAgentDescription dfd = new DFAgentDescription();
		dfd.setName(this.getAID());	

		//crea un ServiceDescription per ogni servizio presente 'serviceOffered'

		for (int i=0; i< serviceOffered.length;i++){

			ServiceDescription sd = new ServiceDescription();
			sd.setType(serviceOffered[i]);
			sd.setName(serviceOffered[i]);
			dfd.addServices(sd);
		}
		try {
			DFService.deregister(this, this.getDefaultDF(), dfd);
			System.out.println("** "+ this.getLocalName() +" ha deregistrato i sui servizi");
		} catch (FIPAException e) {
			System.out.println("Impossibile deregistrare i servizi di: "+ this.getLocalName());
		}		
	}	
}

/////////////////////////////////////////////////////////////////////////
///// FIPA Archive RE Conversation Utility methods to create messages //	
///////////////////////////////////////////////////////////////////////

public ACLMessage prepareSendFIPA_RE(String service, String content){
	//prepare message		
	ACLMessage inform = new ACLMessage(ACLMessage.INFORM);
		//Here, using an architecture based on the registration of the services done by every agent,
		//i search for every service provider and for everyone i start an instance of the ARCHIVE_RE protocol. 
		//Then only the agents that offer a service with the same 'Service ID' of the one contained in 'service' can respond to the protocol request  
			if(this.getServiceProvider(service) != null){
				
				inform.setConversationId(service);
				inform.setContent(content);
				for(int i=0;  i< this.getAllServiceProviders(service).size();i++ ){ 
					AID provediver_i = this.getAllServiceProviders(service).get(i);
					if(provediver_i != null);
					inform.addReceiver(provediver_i);
				}
			
			}
return inform;			
}

public  ACLMessage respondFIPA_RE(ACLMessage  request, ACLMessage response, String replyContent)
{
	ACLMessage reply = request.createReply();
	reply.setPerformative(ACLMessage.INFORM);
	reply.setContent(replyContent);
	System.out.println(this.getLocalName()+": ho risposto al messaggio di"+request.getSender()+"dal contenuto"+request.getContent());
	return reply;
}


//agent class end
}



