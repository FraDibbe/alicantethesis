package swAgents;

import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.OneShotBehaviour;
import jade.lang.acl.ACLMessage;

public  class TopicOneShotPubBehav extends OneShotBehaviour {

	private static final long serialVersionUID = 4158255186362817730L;
	private AID topic;
	private String content;
	private Object Objcontent; //TODO

	public TopicOneShotPubBehav(Agent a, AID topic, String content) {
		super(a);
		this.topic=topic;
		this.content=content;
	}
	
	public TopicOneShotPubBehav(Agent a, AID topic, Object Objcontent) { //TODO
		super(a);
		this.topic=topic;
		this.Objcontent=Objcontent;
	}

	@Override
	public void action() {
	//	System.out.println("Agent "+myAgent.getLocalName()+": Sending message about topic "+topic.getLocalName());
		ACLMessage msg = new ACLMessage(ACLMessage.INFORM);
		msg.addReceiver(topic);
		if(Objcontent == null)	msg.setContent(content);
		//else msg.setContentObject(Objcontent);//TODO
			
		myAgent.send(msg);
		
	}

}
