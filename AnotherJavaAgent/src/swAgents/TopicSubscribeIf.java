package swAgents;

public interface TopicSubscribeIf<T> {
	
	public void handleTopicSubscribe(T message);
	
}
