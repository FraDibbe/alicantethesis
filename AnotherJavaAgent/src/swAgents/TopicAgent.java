package swAgents;

import java.util.Vector;

import basicAgent.BasicAgent;
import jade.core.AID;
import jade.core.behaviours.CyclicBehaviour;
import jade.core.messaging.TopicManagementHelper;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

public abstract class TopicAgent extends BasicAgent {

	private static final long serialVersionUID = -8717639351673581996L;

	private String topicName;
	private AID topicCreated;
	
	private Vector<Object> stringhedArgs=new Vector<Object>();
	
	public TopicAgent(String comunicationChannel)
	{
		super();
		this.setTopicName(comunicationChannel);	
	}
	
	
	@Override
	 protected void setup() {
	   super.setup();
	  if(this.topicName != null)    this.createTopic();
	  else
	  {
		  //Take the arguments for the agent, if any. (normally the communication channel if the agent is created by a SW agent)
		    Object[] args=getArguments();
		 	if(args != null){
		 	stringhedArgs=new Vector<Object>(args.length);
		 	for(int k=0;k<args.length;k++) stringhedArgs.add(k, (AID)args[k]);
		 	
		 	//use the first argument to set up the communication channel AID
		 	setTopic((AID) stringhedArgs.elementAt(0));
		 	}
	  }
	}
	
	
	private void createTopic()
	{
		try {
			//  create the topic "topicName"
			TopicManagementHelper topicHelper = (TopicManagementHelper) getHelper(TopicManagementHelper.SERVICE_NAME);
			topicCreated = topicHelper.createTopic(this.getTopicName());
			
		}
		catch (Exception e) {
			System.err.println("Agent "+getLocalName()+": ERROR creating topic "+ this.getTopicName());
			e.printStackTrace();
		}
	}
	
@SuppressWarnings("serial")
public void subscribeJADETopic(final TopicSubscribeIf<String> subscribeCallback)
{
	try {
		TopicManagementHelper topicHelper = (TopicManagementHelper) getHelper(TopicManagementHelper.SERVICE_NAME);
		topicHelper.register(topicCreated);		
		// Add a behaviour collecting messages about topic "JADE"
		addBehaviour(new CyclicBehaviour(this) {
			public void action() {
				ACLMessage msg = myAgent.receive(MessageTemplate.MatchTopic(topicCreated));
				if (msg != null) {
					//execute the callback fuction
					subscribeCallback.handleTopicSubscribe(msg.getContent());
				}
				else {
					block();
				}
			}
		} );
	}
	catch (Exception e) {
		System.err.println("Agent "+getLocalName()+": ERROR registering to topic "+ this.getTopicName());
		e.printStackTrace();
	}
}
	
public void publishJADETopic(long period, String content)
	{

		TopicTickerPubBehav publisherBehaviour = new TopicTickerPubBehav(this, period, this.getTopic(), content);
		addBehaviour(publisherBehaviour);	
}

public void publishJADETopic(String content)
	{
			TopicOneShotPubBehav publisherBehaviour = new TopicOneShotPubBehav(this, this.getTopic(), content);
			addBehaviour(publisherBehaviour);		
	}
	
public void deregisterJADETopic()
	{
		try {
			//  messages about topic "topicName"
			TopicManagementHelper topicHelper = (TopicManagementHelper) getHelper(TopicManagementHelper.SERVICE_NAME);
			topicHelper.deregister(this.getTopic());
		}
		catch (Exception e) {
			System.err.println("Agent "+getLocalName()+": ERROR deregistering to topic: "+ this.getTopicName());
			e.printStackTrace();
		}
	}

	protected String getTopicName() {
		return topicName;
	}

	protected void setTopicName(String topicName) {
		this.topicName = topicName;
	}
	
	protected AID getTopic() {
		return this.topicCreated;
	}
	
	protected void setTopic(AID topicAID ) {
		this.topicCreated=topicAID;
		setTopicName(topicAID.getLocalName());
	}
	
}
