package swAgents;

import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.TickerBehaviour;
import jade.lang.acl.ACLMessage;

public  class TopicTickerPubBehav extends TickerBehaviour {

	private static final long serialVersionUID = -7233263706196070117L;
	
	private AID topic;
	private String content;
	private Object Objcontent; //TODO

	public TopicTickerPubBehav(Agent a, long period, AID topic, String content) {
		super(a, period);
		this.topic=topic;
		this.content=content;
	}
	
	public TopicTickerPubBehav(Agent a, long period, AID topic, Object Objcontent) { //TODO
		super(a, period);
		this.topic=topic;
		this.Objcontent=Objcontent;
	}

	@Override
	protected void onTick() {
		//	System.out.println("Agent "+myAgent.getLocalName()+": Sending message about topic "+topic.getLocalName());
			ACLMessage msg = new ACLMessage(ACLMessage.INFORM);
			msg.addReceiver(topic);
			if(Objcontent == null)	msg.setContent(content);
			//else msg.setContentObject(Objcontent);//TODO
				
			myAgent.send(msg);
	}

}
