package swAgents;

import basicAgent.MessageHandlers;
import jade.core.behaviours.Behaviour;
import jade.core.behaviours.CyclicBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

public abstract class DomoticAgent extends TopicAgent {

	private static final long serialVersionUID = -5908449449498634937L;

	private Behaviour[] Responder_FIPA_Behaviours=null;
	private Behaviour[] HW_Behaviour_Pool=null;
	private String commChann;
	
	public DomoticAgent(String comunicationChannel) {
		super(comunicationChannel);
		this.commChann=comunicationChannel;
				}

	
	@Override
	 protected void setup() {
	   super.setup();
	    	   
	   
	    /**add FIPA Responder protocol behaviors pool*/  
	    //get the Responder behavior list FIPA-RE compliance
	   Responder_FIPA_Behaviours = set_FIPA_Responder_Behaviour_pool();
	    //add them to the agent
	    if (Responder_FIPA_Behaviours != null) addBehaviour_pool(Responder_FIPA_Behaviours);
	 	
	 	/**add HW behaviours pool*/  
	    HW_Behaviour_Pool = set_HW_Behaviour_pool();
	 	if (HW_Behaviour_Pool != null) addBehaviour_pool(HW_Behaviour_Pool);
	   
	 
	 	/**subscribe to JADE topic*/
	     subscribeJADETopic(new TopicSubscribeIf<String>() {				
						@Override
						public void handleTopicSubscribe(String message) {
							MessageArrivedTopic msgTh=set_JADE_topicHandle_Behaviour();
							if(msgTh!=null){
							msgTh.setTopicMessage(message);
							addBehaviour(msgTh);
							}
						} });
		    
	 }//end of action() 


	
	public abstract Behaviour[] set_FIPA_Responder_Behaviour_pool();
	public abstract Behaviour[] set_HW_Behaviour_pool();
	public abstract MessageArrivedTopic set_JADE_topicHandle_Behaviour();

	private void addBehaviour_pool(Behaviour[] B_pool) {
		
		for(int behavior=0; behavior < B_pool.length; behavior++) 
			addBehaviour(B_pool[behavior]);
	}
		
	
/////////////////////////////////////////////////////////////////////////
///// Inner Dispatcher class to implement a responder //	
///////////////////////////////////////////////////////////////////////

abstract class GenericResponder extends CyclicBehaviour  implements MessageHandlers {

	private static final long serialVersionUID = -7798763846771062795L;

@Override
public void action() {

MessageTemplate mt = null;

for(String elem : serviceOffered) {
if(mt == null) mt=MessageTemplate.MatchConversationId(elem);
else mt=MessageTemplate.or(MessageTemplate.MatchConversationId(elem), mt);
}
ACLMessage msg = myAgent.receive(mt);
if (msg != null) {
handleMessage(msg);
}
else {
block();
}
}

public abstract void handleMessage(ACLMessage msg);

}//end of inner abstrcat class

	
}
