package sceneAgent;

import resources.Resource;
import restBehaviours.RESTconnFactoryBeh;
import restBehaviours.RESTconnFactoryBeh.REST_Client_Types;
import agentShare.TopicPubAgent;


public class Scene_HW_Agent extends TopicPubAgent {
	
	private static final long serialVersionUID = -8983859139111804738L;

	//private SceneClient client;
	
	@Override
	public String[] setService2Search() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String[] setServiceOffered() {
		// TODO Auto-generated method stub
		return null;
	}

	
	////////////////////
	// Main Action
	///////////////////
	@SuppressWarnings("serial")
	@Override
	 protected void setup() {
	   super.setup();
	   
	   //get client Instance
	   	 addBehaviour(new RESTconnFactoryBeh(REST_Client_Types.SCENES, Resource.Server_Hostname, Resource.username, Resource.password)
	   			 {
	   		 		@Override
	   		 		public int onEnd() {
	  // 		 			client= (SceneClient)getResult();
	   		 			return super.onEnd();
	   		 		}
	   			 });  
	    
	   	 
	   	 //TODO: instantiate a behavior responder for the messages? Example: a message that say shout down the light is received
	   	 //so in the cyclic behavior there will be a call to client.execute('url/to/light/down')
	   	
	   	 
	   	 
	 }//end of action() 
	 
	 
////////////////////
// Private Methods
/////////////////// 

	 
}
