package sceneAgent;

import resources.Resource;
import agentShare.TopicSubAgent;
import agentShare.TopicSubscribeIf;

public class Scene_SW_Agent extends TopicSubAgent {

	private static final long serialVersionUID = 8472754644137654981L;
	
	@Override
	public String[] setService2Search() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String[] setServiceOffered() {
		// TODO Auto-generated method stub
		return null;
	}

	
	
	////////////////////
	// Main Action
	///////////////////
	@Override
	 protected void setup() {
	   super.setup();
	     
	    //subscribe to JADE topics to take ASR strings:
	   subscribeJADETopic(Resource.ASR_Airos_topicName, new TopicSubscribeIf<String>() {				
					@Override
					public void handleTopicSubscribe(String message) {
						//addBehaviour(b); //TODO: inizia un Protocollo di comunicazione con altri agenti?						
					}
	   	});
	    
	 }//end of action() 
	 
	 	 
}
