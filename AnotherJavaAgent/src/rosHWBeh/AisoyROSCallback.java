package rosHWBeh;

public interface AisoyROSCallback<T> {

	public void handleROStopic(T message);
}
