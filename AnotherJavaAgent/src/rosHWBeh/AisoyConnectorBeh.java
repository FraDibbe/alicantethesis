package rosHWBeh;

import edu.wpi.rail.jrosbridge.Ros;
import jade.core.Agent;
import jade.core.behaviours.OneShotBehaviour;

public class AisoyConnectorBeh extends OneShotBehaviour {

	private static final long serialVersionUID = -1556755791965948855L;
	
	private final static String DEFAULT_HOST ="aisoy1.local";
	private String host;
	private Ros ros;
	
	public AisoyConnectorBeh(Agent a,String host)
	{
		super(a);
		this.host=host;
	}
	
	public AisoyConnectorBeh(Agent a) {
		super(a);
	}
	
	//connects to Aisoy using WebSockets and jroslib
	@Override
	public void action() {
		String toConnect;
		
		if(this.host != null) toConnect=this.host;
		else toConnect=DEFAULT_HOST;
		
			ros = new Ros(toConnect);
		    boolean c=ros.connect();
		    if(c) System.out.println("ROS connected");
	}

	
	public Ros getResult(){
		return this.ros;
	}

}
