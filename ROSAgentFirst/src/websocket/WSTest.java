package websocket;

import edu.wpi.rail.jrosbridge.Ros;
import edu.wpi.rail.jrosbridge.Service;
import edu.wpi.rail.jrosbridge.Topic;
import edu.wpi.rail.jrosbridge.callback.TopicCallback;
import edu.wpi.rail.jrosbridge.messages.Message;
import edu.wpi.rail.jrosbridge.services.ServiceRequest;
import edu.wpi.rail.jrosbridge.services.ServiceResponse;

public class WSTest {

	public void test(){
			
		Ros ros = new Ros("aisoy1.local");
	    boolean c=ros.connect();

	    if(c) System.out.println("ROS connected");
	    
	    
	    System.out.println("*** Subscriving to a TOPIC ***");
	    
	    Topic listener = new Topic(ros, "/airos4/tts/is_speaking", "std_msgs/Bool");
	    listener.subscribe(new TopicCallback() {	
			public void handleMessage(Message message) {
				System.out.println("From ROS: " + message.toString());			
			}
		});
	    
	    System.out.println("*** Done! ***");
	    
	    System.out.println("*** Call two Services ***");
	    
	    System.out.println("*** Change TTS language ***");
	    Service lan = new Service(ros, "/airos4/tts/set_language", "airos4_msgs/SetString");
	    ServiceRequest request1 = new ServiceRequest("{\"data\": \"en-US\"}");
	    ServiceResponse response = lan.callServiceAndWait(request1);
	    
	    System.out.println("*** Say Hello Horld! ***");
	    Service speak = new Service(ros, "/airos4/tts/say", "airos4_msgs/Say");
	    ServiceRequest request = new ServiceRequest("{\"sentence\": \"Hello Horld!\", \"moveMouth\": true}");
	     response = speak.callServiceAndWait(request);
	   
	    System.out.println(response.toString());

	    System.out.println("***Services called***");
	    
	    ros.disconnect();
	}
	
}
