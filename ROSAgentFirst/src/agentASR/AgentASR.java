package agentASR;

import agentsShare.BasicAgent;
import resources.ServicesNamespace;

public class AgentASR extends BasicAgent {
	private static final long serialVersionUID = 125765415315926245L;

	///// attributes
	
	private int value;
	
	////methods
	
	public String[] setService2Search() {
		String[] services={ServicesNamespace.Giggiolo};
		return services;
	}

	public String[] setServiceOffered() {
		String[] services={ServicesNamespace.SmartphoneNavigation};
		return services;
	}
	

	@SuppressWarnings("serial")
	@Override    
	protected void setup() {
	    super.setup();
	    
	    ////// logical behaviours
	    
	    addBehaviour(new ROSConnectorBeh());
	    
	    addBehaviour(new ROSTopicBeh(new AIsoyManager()){
		            @Override
		            public int onEnd() { 
		                    int ret = super.onEnd();
		                    value = getResult();
		                    return ret;
		            }
				});
	}
	
	
	
//class end	
}
