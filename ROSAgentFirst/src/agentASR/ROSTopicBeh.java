package agentASR;

import behavShare.OneShotWorkBeh;


@SuppressWarnings("serial")
public class ROSTopicBeh extends OneShotWorkBeh<ROSTopicIntf, Integer>{

	
	public ROSTopicBeh(ROSTopicIntf mod) {
		super(mod);
		}

	@Override
	public void action() {
	
	   module.subscribe();
	   
	}
	
	
}
