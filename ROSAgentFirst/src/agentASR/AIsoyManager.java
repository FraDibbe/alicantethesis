package agentASR;

import edu.wpi.rail.jrosbridge.Ros;
import edu.wpi.rail.jrosbridge.Topic;
import edu.wpi.rail.jrosbridge.callback.TopicCallback;
import edu.wpi.rail.jrosbridge.messages.Message;

public class AIsoyManager implements ROSTopicIntf, ROSServiceIntf{

	Ros connection;
	
	public AIsoyManager(Ros connection)
	{
		this.connection=connection;
	}
	
	
	///////////////////////////////////////
    // Methods of ROSTopicIntf
	//////////////////////////////////////
	public void subscribe(String topic, String msgType) 
	{ 
		if(this.connection != null)
		{
			Topic listener = new Topic(connection, "/airos4/tts/is_speaking", "std_msgs/Bool");
		    listener.subscribe(new TopicCallback() {	
				public void handleMessage(Message message) {
					System.out.println("From ROS: " + message.toString());			
				}
		    });
		}
	}

	public void create() {
		// TODO Auto-generated method stub
		
	}

///////////////////////////////////////
// Methods of ROSServiceIntf
//////////////////////////////////////
	public int call() {
		// TODO Auto-generated method stub
		return 0;
	}

}
