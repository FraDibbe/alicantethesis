package behavShare;

import jade.core.behaviours.OneShotBehaviour;


@SuppressWarnings("serial")
public abstract class OneShotWorkBeh<ModuleType,ResultType> extends OneShotBehaviour{
	
	protected ModuleType module=null;
	private ResultType result=null; 
	
	public OneShotWorkBeh(ModuleType mod)
	{
		super();
		this.module=mod;
	}
	
	public abstract void action();
	
	
	public ModuleType getModule()
	{
		return module;
	}
				

	public ResultType getResult()
	{
		if(module != null) 	return result;
		else {
			System.out.println("You need to istantiate a Module object, returning NULL");
			return null;
		}
	}
	
}
