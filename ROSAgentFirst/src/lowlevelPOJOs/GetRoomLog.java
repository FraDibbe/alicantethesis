package lowlevelPOJOs;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import Client.exception.AuthRequiredException;
import Pojos.Log;
import Pojos.Room;

public interface GetRoomLog {
	
	public List<Log> getLog(Date start, Date end, Room room) throws IOException, AuthRequiredException;

}
