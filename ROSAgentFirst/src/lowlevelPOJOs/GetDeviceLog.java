package lowlevelPOJOs;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import Client.exception.AuthRequiredException;
import Pojos.Device;
import Pojos.Log;

public interface GetDeviceLog {

	public List<Log> getLog(Date start, Date end, Device device) throws IOException, AuthRequiredException;
}
