package lowlevelPOJOs;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import Client.exception.AuthRequiredException;
import Pojos.Log;

public interface GetDayLog {

	public List<Log> getTodayLog() throws IOException, AuthRequiredException;
	public List<Log> getLog(Date start, Date end) throws IOException, AuthRequiredException; 
	
}
