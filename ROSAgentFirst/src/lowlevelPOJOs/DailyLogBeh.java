package lowlevelPOJOs;

import agentASR.DailyLogIn;
import jade.core.behaviours.OneShotBehaviour;

@SuppressWarnings("serial")
public class DailyLogBeh extends OneShotBehaviour {

	private DailyLogIn module;
	private Log result; //exit point of the behaviur
	
	DailyLogBeh(DailyLogIn object)
	{
		this.module = object;
	}
	
	@Override
	public void action() {
		
		//launch a command to retrive the Today Log
		result=module.getTodayLog();
		
		//launch a command to retrive the Date Log
		result=module.getLog(start, end);
		
	}

	
	public Log getResult()
	{
		return this.result;
	}

}
