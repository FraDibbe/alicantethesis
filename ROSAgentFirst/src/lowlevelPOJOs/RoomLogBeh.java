package lowlevelPOJOs;

import jade.core.behaviours.OneShotBehaviour;

@SuppressWarnings("serial")
public class RoomLogBeh extends OneShotBehaviour {

	private GetRoomLog module;
	private Log result; //exit point of the behaviur
	
	RoomLogBeh(GetRoomLog object)
	{
		this.module = object;
	}
	
	@Override
	public void action() {
		
		//launch a command to retrive the Room Log
		result=this.module.getLog(start, end, room);	
	}

	
	public Log getResult()
	{
		return this.result;
	}

}
