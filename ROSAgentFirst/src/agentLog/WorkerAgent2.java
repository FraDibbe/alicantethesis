package agentLog;

import agentsShare.BasicAgent;
import jade.lang.acl.ACLMessage;
import resources.ServicesNamespace;

public class WorkerAgent2 extends BasicAgent {
	private static final long serialVersionUID = 125765415315536245L;

	
	/*** VEDERE BENE, FORSE IL WORKING AGENT NON DEVE ESTENDERE QUESTI, 
	 * MA METTERE A DISPOSIZIONE SOLO LE INTERFACCE PER ACCEDERERE AI WOrK BEH*/
	public String[] setService2Search() {
		String[] services={ServicesNamespace.SmartphoneNavigation};
		return services;
	}

	public String[] setServiceOffered() {
		String[] services={ServicesNamespace.Giggiolo};
		return services;
	}
	
	
	@SuppressWarnings("serial")
	@Override    
	protected void setup() {
	    super.setup();
	    addBehaviour(new GenericResponder(){

			public void handleMessage(ACLMessage msg) {
				System.out.println("Agent:" + myAgent.getName() + "recived:" + msg.getContent());
				
				addBehaviour(new RoomLogBeh(new LogClientpojo()) {
		            @Override
		            public int onEnd() { 
		                    int ret = super.onEnd();
		                    int = getResult();//get the service providers HashMap from the behaviur
		                    return ret;
		            }
				});
			}
		});
	
	}
	
//class end
}
