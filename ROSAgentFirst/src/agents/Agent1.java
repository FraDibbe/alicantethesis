package agents;

import agentsShare.BasicAgent;
import jade.core.behaviours.TickerBehaviour;
import jade.lang.acl.ACLMessage;
import resources.ServicesNamespace;

public class Agent1 extends BasicAgent {
	private static final long serialVersionUID = 125765415315926245L;

	
	/*** VEDERE BENE, FORSE IL WORKING AGENT NON DEVE ESTENDERE QUESTI, 
	 * MA METTERE A DISPOSIZIONE SOLO LE INTERFACCE PER ACCEDERERE AI WOrK BEH*/
	public String[] setService2Search() {
		String[] services={ServicesNamespace.Giggiolo};
		return services;
	}

	public String[] setServiceOffered() {
		String[] services={ServicesNamespace.SmartphoneNavigation};
		return services;
	}
	

	@SuppressWarnings("serial")
	@Override    
	protected void setup() {
	    super.setup();
	    
	    
	    addBehaviour(new TickerBehaviour(this,1000) {
			
			@Override
			protected void onTick() {
				ACLMessage inform=prepareSendFIPA_RE(ServicesNamespace.Giggiolo,"HELLO!!!");
				send(inform);
				System.out.println("Agent "+ myAgent.getName() + "sent HELLO");
			}
		});
	    
	    
	    
	
	}
	
	
	
//class end	
}
