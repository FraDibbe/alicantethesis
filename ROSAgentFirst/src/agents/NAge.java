package agents;

import java.util.ArrayList;
import java.util.List;

import jade.core.Agent;
import jade.core.behaviours.OneShotBehaviour;

@SuppressWarnings("serial")

/*** the correct one!! **/

public class NAge extends Agent{

    private List<Integer> list;

    @Override
    protected void setup() {
            super.setup();
            
            list= new ArrayList<Integer>();
            
            addBehaviour(new BehCaire(list, 10) {
                    @Override
                    public int onEnd() { //in this way agent and behaviurs are not coupled
                            int ret = super.onEnd();
                            list = getResult();
                            return ret;
                    }
            });
            
            
            addBehaviour(new OneShotBehaviour() { //a behaviur that use the list produced by the other behaviur
				
				@Override
				public void action() {
					System.out.println("In Beha 2, List is "+list.size());
					
					for(Integer elem : list) {
			            System.out.println(elem.toString());
			        }
				}
			});
    }
    
    
    
    //the takedown use the data
    @Override
    protected void takeDown() {
    	   	
    	//print the private 'list' element of the agent before killing it
    	
    	System.out.println("In takedown");
    	
    	for(Integer elem : list) {
            System.out.println(elem.toString());
        }
    	
    }
    
}