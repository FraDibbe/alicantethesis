package agentsShare;

import jade.lang.acl.ACLMessage;

public interface MessageHandlers {

	public abstract void handleMessage(ACLMessage msg);
	
}
