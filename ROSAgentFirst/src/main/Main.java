package main;

import resources.Resource;

public class Main {

	public static void main(String[] args) {
		// start a Main Container with one agent (called JohnAg) @ localhost
		String[] param = new String[2];
		param[0]="-gui";
		//to start a normal Container: param[0]="-container"; //	param[1]="-host"; //param[2]=Resource.HostipAddress;
	 	param[1]="Seth:agents.Agent1;John:agents.WorkerAgent2";
		
	 	//start jade with above parameters
	 	jade.Boot.main(param);
		
	}
	
}