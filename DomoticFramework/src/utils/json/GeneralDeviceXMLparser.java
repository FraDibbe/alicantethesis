package utils.json;

import ontology.concepts.DeviceConcept;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

public class GeneralDeviceXMLparser extends XML_SAX_parser{
	
	private DeviceConcept currentDevice;
	
	private boolean elementDeviceCreation = false;
	
	private int currentId;
	private String currentName;
	private String currentDevType;
	
	public DeviceConcept getParsedData() {
		return currentDevice;
	}

	public GeneralDeviceXMLparser() {
		super();
	}

	@Override
	public void startElement(String namespaceURI, String localName, String qName, Attributes atts) throws SAXException
	{
		super.startElement(namespaceURI, localName, qName, atts);
		
		// Process Device Info
		if (localName.equalsIgnoreCase("DEVICE")&&!elementDeviceCreation)	elementDeviceCreation=true;	
		manageAttributes(atts);
	}
	

	@Override
	public void endElement(String namespaceURI, String localName, String qName)	throws SAXException {
		super.endElement(namespaceURI, localName, qName);
		
		// Process Device Info
        if (localName.equalsIgnoreCase("DEVICE") && elementDeviceCreation)
        {
        	//construct the element
        	currentDevice= new DeviceConcept();
        	
        	currentDevice.setDeviceId(currentId);
        	currentDevice.setDeviceName(currentName);
        	currentDevice.setType(currentDevType);
        
        	elementDeviceCreation=false;
        }
	}

	@Override
	public void characters(char ch[], int start, int length) throws SAXException {
		super.characters(ch, start, length);
	}
	
	
	private void manageAttributes(Attributes attr)
	{
		int lenght= attr.getLength();
		
		for(int i=0; i< lenght;i++)
		{
			switch(attr.getLocalName(i).toLowerCase())
			{
			case "id":		
				currentId=Integer.parseInt(attr.getValue(i));
				break;
			case "name":
				currentName=attr.getValue(i);
				break;		
			case "devicetype":
				currentDevType=attr.getValue(i);
				break;		
	
			}	
		}
	}

}
