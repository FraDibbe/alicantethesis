package utils.json;

import ontology.concepts.EnergyDeviceConcept;
import ontology.concepts.RoomConcept;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

public class EnergyDeviceXMLparser extends XML_SAX_parser{
	
	private EnergyDeviceConcept currentDevice;
	
	private boolean elementDeviceCreation = false;
	
	private int currentId;
	private String currentName;
	private String currentTech;
	private String currentPlace;
	private String currentCmpType;
	private String currentDevType;
	private int currentPowerC;
	private int currentPriority;
	private String currentHWComm;
	
	private String label;

	public EnergyDeviceConcept getParsedData() {
		return currentDevice;
	}

	public EnergyDeviceXMLparser() {
		super();
	}

	@Override
	public void startElement(String namespaceURI, String localName, String qName, Attributes atts) throws SAXException
	{
		super.startElement(namespaceURI, localName, qName, atts);
		
		// Process Device Info
		if (localName.equalsIgnoreCase("DEVICE")&&!elementDeviceCreation)	elementDeviceCreation=true;	
		//save the label value, used in this.characters() method
		label=localName.toLowerCase();
		if(!(label.equalsIgnoreCase("properties") || label.equalsIgnoreCase("property")) )manageAttributes(atts);
	}
	

	@Override
	public void endElement(String namespaceURI, String localName, String qName)	throws SAXException {
		super.endElement(namespaceURI, localName, qName);
		
		// Process Device Info
        if (localName.equalsIgnoreCase("DEVICE") && elementDeviceCreation)
        {
        	//construct the element
        	currentDevice= new EnergyDeviceConcept();
        	
        	currentDevice.setDeviceId(currentId);
        	currentDevice.setDeviceName(currentName);
        	currentDevice.setPriority(currentPriority);
        	currentDevice.setType(currentDevType);
        	currentDevice.setInstantPower(currentPowerC);

        	RoomConcept room= new RoomConcept();
        	
        	room.setRoomID(1);//TODO: aggiustare in qualche modo, cercare su un altro XML che stanza �?(parsing)
        	
        	room.setRoomName(currentPlace);
        	
        	currentDevice.setPosition(room);
        
        	elementDeviceCreation=false;
        }
	}

	@Override
	public void characters(char ch[], int start, int length) throws SAXException {
		super.characters(ch, start, length);
        	
		String readed = new String(ch, start, length).toLowerCase();
		
		switch(label)
		{
		case "technology":		
			currentTech=readed;
			break;
		case "priority":
			currentPriority=Integer.parseInt(readed);
			break;		
		case "place":
			currentPlace=readed;
			break;		
		case "powerconsuption":
			currentPowerC=Integer.parseInt(readed);
			break;		
		case "command":
			currentHWComm=readed;
			break;		
		}
	}
	
	
	private void manageAttributes(Attributes attr)
	{
		int lenght= attr.getLength();
		
		for(int i=0; i< lenght;i++)
		{
			switch(attr.getLocalName(i).toLowerCase())
			{
			case "id":		
				currentId=Integer.parseInt(attr.getValue(i));
				break;
			case "name":
				currentName=attr.getValue(i);
				break;		
			case "devicetype":
				currentDevType=attr.getValue(i);
				break;		
			case "componenttype":
				currentCmpType=attr.getValue(i);
				break;		
			}
		
		}	
	}


}
