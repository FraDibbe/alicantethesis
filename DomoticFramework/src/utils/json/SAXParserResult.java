package utils.json;

import jade.content.Concept;

public interface SAXParserResult {

	public Concept getParsedData();
}
