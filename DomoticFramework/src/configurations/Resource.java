package configurations;


/*It's a class of Resources. 
A Resorce is treated as a "define", in terms of C, and it was used here in the same way.

*/

public class Resource {

	/* AIROS 4 services and topic names*/
	public static final String ASR_Airos_topicName="/airos4/asr/recognition";
	public static final String ASR_Airos_messageType="std_msgs/String";
	
	public static final String ASR_grammar_Airos_ServiceName="/airos4/asr/set_grammar";
	public static final String ASR_grammar_Airos_messageType="std_msgs/SetString";
	
	public static final String ASR_language_Airos_ServiceName="/airos4/asr/set_language";
	public static final String ASR_language_Airos_messageType="std_msgs/SetString";
	
	public static final String Touch_Airos_topicName="/airos4/touch/touch";
	public static final String Touch_Airos_messageType="airos4_msgs/Touch";
	
	public static final String IsSpeaking_Airos_topicName="/airos4/tts/is_speaking";
	public static final String IsSpeaking_messageType="std_msgs/Bool";
	
	public static final String Say_Airos_ServiceName="/airos4/tts/say";
	public static final String Say_Airos_messageType="airos4_msgs/Say";
	
	/*REST WEB SERVICEs*/
	public static final String Server_Hostname="shanon.iuii.ua.es/s/rest/";
	public static final String username="fra.dibenedetto1@gmail.com";
	public static final String password="27uaj45lM";
	
	
}