package configurations;

public final class ServicesNamespace {

	/* SERVICE IDs for application agents	*/
	public static final String Illumination="srv:Illumination";
	public static final String ASR="srv:ASR";
	public final static String VocalCommands="srv:Vocal";
	public final static String Presence="srv:Presence";	
	public final static String EnergySaving="srv:EnergySav";	
	public final static String Logger="srv:Log";
	public static final String ButtonControl = "srv:ButtonController";
	public static final String EnergyModify = "srv:EnergyModify";
	public static final String REST_General_ACT ="srv:REST_gen_act";
	public static final String Scene ="srv:scene";
	public static final String TTS ="srv:tts";

}
