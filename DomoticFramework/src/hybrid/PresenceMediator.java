package hybrid;



import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;

import ontology.actions.Do;
import ontology.actions.Order;
import ontology.actions.ShutDown;
import ontology.actions.TurnOn;
import ontology.actions.Order.OrderType;
import ontology.concepts.DeviceConcept;
import ontology.concepts.RoomConcept;
import configurations.ServicesNamespace;
import agents.basicAgent.baseBehaviour.Decision;
import agents.typos.MediatorAgent;


public class PresenceMediator extends MediatorAgent {
	private static final long serialVersionUID = 5192110064932207033L;
		
	private boolean connectionOpened=false;
	
    private String DB_URL = "jdbc:h2:~/"+getLocalName()+"Presence_DB";
    
    //  Database credentials
    private String USER = "";
    static final String PASS = "";
    
    //Database connection
    Connection conn = null;
	
	@Override
	protected void main() {
		//this.setLogicSpeed(3000);	
		connect();
	}
	
	@Override
	public Decision decide(Order order) {
	
		Decision decision= new Decision();
		
		//extract device information
		DeviceConcept  presence=((DeviceConcept)order.getWhat());
		
		if (!isConnectionOpened()) connect();
		
		if(order.getType() == OrderType.WRITE) 
		{
			try {
				write(presence);
				decision.setDecisionExitus(true);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		else if (order.getType() == OrderType.READ) 
		{
			try {
				boolean readOk=false;
				ResultSet set = read(presence); //read from DB
					
				String[] values=new String[2];
				//TAKE DATA HERE FROM THE DB
				try {
					if(this.getRow(set) > 0) {
						values[0]= set.getString("VALUE");
						values[1]= set.getString("ROOM_ID");
						decision.setDecisionExitus(true);
						readOk=true;
					}
				} catch (SQLException e) {
					e.printStackTrace();
				}
				
				//decision.setStringValueOfDecision(values);
				
				if(readOk){
				/*send data & commands to actuator*/
				Do act;
				
				if(values[0].equalsIgnoreCase("1")) act=new TurnOn();
				else act=new ShutDown();
				
				RoomConcept room = new RoomConcept();
				room.setRoomID(Integer.parseInt(values[1]));
			
				presence.setPosition(room);
				
				act.setWhat(presence);
				act.setService(ServicesNamespace.Presence);
				act.setHW_dependend_command("read");
				
				try {
					sendToActuators(act,true);
				} catch (Exception e) {
					e.printStackTrace();
				}
				}
				
			} catch (Exception e) {
				e.printStackTrace();
			}
			
		}
		
		
		return decision;
	}
	


	@Override
	public String[] setService2Search() {
		String[] services={ServicesNamespace.Presence};
		return services;
	}

	@Override
	public String[] setServiceOffered() {
		String[] services={ServicesNamespace.Presence};
		return services;
	}

	
	///////////////////////
	/// DB storage Methods ////
	/////////////////////////

	private void write(DeviceConcept devPres) throws Exception {
			
		String sql="MERGE INTO SENSORS(PRESENCE_SENSOR,VALUE,ROOM_ID,TIME) VALUES('" +devPres.getDeviceName() +"',"+devPres.getActualValue() +","+devPres.getPosition().getRoomID() +", NOW());";
        
		if(executeQuery(sql)<0) throw new Exception("LOG WRITE ERROR");
	}

	private ResultSet read(DeviceConcept presence) {
		
		String sql="select * from SENSORS where PRESENCE_SENSOR='"+presence.getDeviceName()+"';";
					
		return queryDB(sql);
		
	}
	
	
	private void connect() {
		
        //Open a connection
        try {
			conn = DriverManager.getConnection(DB_URL, USER, PASS);  //if DB doesn't exist, it create a new one
		} catch (SQLException e) {
			e.printStackTrace();
		}
	
        String sql="CREATE TABLE IF NOT EXISTS SENSORS(PRESENCE_SENSOR VARCHAR(255) PRIMARY KEY, VALUE INT NOT NULL , ROOM_ID INT  NOT NULL, TIME TIMESTAMP NOT NULL);";
        
        executeQuery(sql);
        
		//write log created
		this.addPermanently2AgentStore("Connection "+new Date().toString(), "Opened");
		//connection OK!
		setConnectionOpened(true);
	}
	
	
	private int getRow(ResultSet rs) {
		int size =0;
		if (rs != null) 
		{
			try{
		  rs.beforeFirst();
		  rs.last();
		  size = rs.getRow();
			}
			catch(Exception e){e.printStackTrace();}
		}
		return size;
	}
	
	//////////////////////////
	//// getter setter //////
	///////////////////////
	
	
	
	public boolean isConnectionOpened() {
		return connectionOpened;
	}

	public void setConnectionOpened(boolean connectionOpened) {
		this.connectionOpened = connectionOpened;
	}

	
	
			///////////////////////
			/// DB queryng ////
			/////////////////////////
	
	private int executeQuery(String sql){
		
		 Statement stmt = null;
		 int ret = 0;
	    
	       try {
	   		stmt = conn.createStatement();
	   	} catch (SQLException e) {
	   		e.printStackTrace();
	   	}

	       try {
	   		ret=stmt.executeUpdate(sql);
	   	} catch (SQLException e) {
	   		e.printStackTrace();
	   	}
		return ret;
	}
	
	
	private ResultSet queryDB(String sql){
		
		 Statement stmt = null;
		 ResultSet ret = null;
	    
	       try {
	   		stmt = conn.createStatement();
	   	} catch (SQLException e) {
	   		e.printStackTrace();
	   	}

	       try {
	   		ret=stmt.executeQuery(sql);
	   	} catch (SQLException e) {
	   		e.printStackTrace();
	   	}
		return ret;
	}
	
}

