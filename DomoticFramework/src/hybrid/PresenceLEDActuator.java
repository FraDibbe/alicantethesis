package hybrid;


import java.util.ArrayList;

import jade.core.behaviours.TickerBehaviour;
import ontology.actions.Order;
import ontology.actions.Order.OrderType;
import ontology.actions.ShutDown;
import ontology.actions.TurnOn;
import ontology.concepts.Actuation;
import ontology.concepts.DeviceConcept;
import ontology.concepts.StringMessage;
import utils.json.DeviceXMLparser;
import hardware.raspberry.GPIO_HW;
import hardware.raspberry.RaspberryConfig;

import com.pi4j.io.gpio.Pin;
import com.pi4j.io.gpio.PinState;

import configurations.ServicesNamespace;
import agents.basicAgent.baseBehaviour.Decision;
import agents.typos.ActuatorAgent;


public class PresenceLEDActuator extends ActuatorAgent {
	private static final long serialVersionUID = 5192110064932207033L;

	private GPIO_HW gpioController=null;
	private ArrayList<String> presenceDevNameList;
	public int index=0;
	
	@Override
	protected void main() {
		//set the log service
		this.setLogService(ServicesNamespace.Logger);
		//set the device info path
		this.setPath("devicesXML/");
		
		//add the sensor presence sensor to monitor
		presenceDevNameList = new ArrayList<String>();
		
		presenceDevNameList.add("ledPresence1");
		presenceDevNameList.add("ledPresence2");
		
		//add a behavior that ask cyclically data sensor to the mediator (DB) and then actuate on those data
		addBehaviour(new TickerBehaviour(this, 3000) { //refresh each minute
		
			private static final long serialVersionUID = 5538851158320362023L;

			@Override
			protected void onTick(){
				System.out.println("++++ ASK for dev");
				
				//ask to the mediator for presence sensors data
				if(index >= presenceDevNameList.size()) index =0;
				
					
				//get the device info
					DeviceConcept devC = (DeviceConcept) findDevice(presenceDevNameList.get(index), new DeviceXMLparser());
				
					Order ord = new Order();
					ord.setWhat(devC);
					ord.setType(OrderType.READ);
					ord.setService(ServicesNamespace.Presence);
					
					try {
						sendToMediators(ord,true);
						index++;
					} catch (Exception e) {
						e.printStackTrace();
					}			
							
			}
		});
	}
	
	@Override
	protected void actuatorSetup() {
		//Take the control of GPIO HW controller
		gpioController = new GPIO_HW();
	}

	@Override
	public Decision actuate(Actuation toActuate) {
		
		Decision dec= new Decision();
		PinState action=null;
		
		//extract the actuation action
		if(toActuate.getActionRequested() instanceof TurnOn)
		{
			action=PinState.HIGH;
		}
		else if (toActuate.getActionRequested() instanceof ShutDown)
		{
			action=PinState.LOW;
		}
		
		//extract device information
		String devName = ((DeviceConcept)toActuate.getActionRequested().getWhat()).getDeviceName();
			
		//device Id conversion
		Pin pin = RaspberryConfig.PinConfiguration.get(devName);
	
		if(pin != null && action != null){
			//Actuate on HW
			if(action.equals(PinState.HIGH)) gpioController.pinHigh(pin);
			else if(action.equals(PinState.LOW)) gpioController.pinLow(pin);
			dec.setDecisionExitus(true);
		}
		
		try {
			StringMessage log=new StringMessage();

	 		log.setMessage("LED "+devName+" state modified!");
			sendToLoggers(log);
		} catch (Exception e) {
			e.printStackTrace();
		}
	
		return dec;
	}

	@Override
	public String[] setService2Search() {
		String[] services={ServicesNamespace.Presence};
		return services;
	}

	@Override
	public String[] setServiceOffered() {
		String[] services={ServicesNamespace.Presence};
		return services;
	}


	
}

