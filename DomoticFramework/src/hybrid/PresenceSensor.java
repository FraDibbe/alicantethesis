package hybrid;

import java.util.ArrayList;
import java.util.Map.Entry;

import com.pi4j.io.gpio.Pin;

import configurations.ServicesNamespace;
import hardware.raspberry.GPIO_HW;
import hardware.raspberry.RaspberryConfig;
import jade.content.Concept;
import ontology.actions.Order;
import ontology.actions.Order.OrderType;
import ontology.concepts.Convertor;
import ontology.concepts.DeviceConcept;
import utils.json.DeviceXMLparser;
import agents.typos.SensorPollAgent;

public class PresenceSensor extends SensorPollAgent{

	private static final long serialVersionUID = -3671829557911534762L;

	private GPIO_HW gpio;
	private ArrayList<Pin> presencePinList;
	private int index = 0; 
	
	@Override
	protected void main() {
		this.setLogicSpeed(3000);//each 16 sec poll the led states
		presencePinList = new ArrayList<Pin>();
		
		//set the device info path
		this.setPath("devicesXML/");
	}
	
	@Override
	protected void sensorSetup() {
		
		//HW interaction
		gpio = new GPIO_HW();
		
		Pin presence1=RaspberryConfig.PinConfiguration.get("pinPresence1");
		Pin presence2=RaspberryConfig.PinConfiguration.get("pinPresence2");
		
		presencePinList.add(presence1);
		presencePinList.add(presence2);
		
	}

	@Override
	public Convertor percept() {
		
		Convertor cvt = null;
		System.out.println("--------Percept for dev");
		
		//pool presence using the pin state
		if(index >= presencePinList.size()) index = 0;
		
		Pin pin = presencePinList.get(index);
				
			//get the Functionality corresponding to the GPIO eventName
			String presenceButt = null;
					
				for ( Entry<String, Pin> entry : RaspberryConfig.PinConfiguration.entrySet()) {
					if( entry.getValue().equals( pin ) )
						{
							presenceButt= entry.getKey();
							break;
						}
				    }
				
			if(presenceButt !=null){
		 	//match the button name with the its functionality
				String deviceName=RaspberryConfig.functionality2device.get(presenceButt);
				
				String[] params = new String[2];
				params[0]=deviceName;
					if(gpio.isDigitalPinHIGH(pin)) //if is HIGH: someone is in the room
					{
						params[1]="1";
					}
					else //if is LOW: room is empty
					{
						params[1]="0";
					}
				index++;	
				cvt = new Convertor((Object[])params);
			}
			
	//	}
		
		return cvt;
	}
	
	@Override
	public Concept convert(Convertor conv) {
		
		//obtain the 'right' Conversion object
		 String[] cmds = (String[])conv.getObjectsOfConvertion();
		 
		 //datas
		 String deviceName=cmds[0];
		 String value=cmds[1];
		 	
		 // Prepare the content of msg to be used with the ontology
	    DeviceConcept deviceObj= (DeviceConcept) findDevice(deviceName,new DeviceXMLparser());	
	    deviceObj.setActualValue(value);
	    
	    //send to mediator
	    Order order= new Order();
	    order.setWhat(deviceObj);
	    order.setType(OrderType.WRITE);
	    order.setService(ServicesNamespace.Presence);
	    return order;
	}

	@Override
	public String[] setService2Search() {
		String[] srv={ServicesNamespace.Presence};
		return srv;
	}

	@Override
	public String[] setServiceOffered() {
		return null;
	}


}
