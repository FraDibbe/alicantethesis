package fueraFrm;


import ontology.concepts.Actuation;
import ontology.concepts.DeviceConcept;
import configurations.ServicesNamespace;
import agents.basicAgent.baseBehaviour.Decision;
import agents.typos.ActuatorAgent;


public class StringherActuator extends ActuatorAgent {
	private static final long serialVersionUID = 5192110064932207033L;
		
	@Override
	protected void main() {
		//this.setLogicSpeed(30);	
	}
	
	@Override
	protected void actuatorSetup() {
		System.out.println("HI! I'm the actuator Stringher!");
	}

	@Override
	public Decision actuate(Actuation toActuate) {
			
		Decision dec = new Decision();
		
		//extract device information
		int rdm = ((DeviceConcept)toActuate.getActionRequested().getWhat()).getDeviceId();
		
		System.out.println("Ricevuto:"+ rdm);
		
		this.addPermanently2AgentStore(String.valueOf(rdm), "sasas");
		
		dec.setDecisionExitus(true);
		
		String[] str= {"Inviato numero "+ rdm};
		dec.setStringValueOfDecision(str);
		
		return dec;
	}

	@Override
	public String[] setService2Search() {
		return null;
	}

	@Override
	public String[] setServiceOffered() {
		String[] services={ServicesNamespace.ASR};
		return services;
	}

	
}

