package fueraFrm;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;

import ontology.concepts.Actuation;
import ontology.concepts.StringMessage;
import configurations.ServicesNamespace;
import agents.basicAgent.baseBehaviour.Decision;
import agents.typos.ActuatorAgent;


public class LogActuator extends ActuatorAgent {
	private static final long serialVersionUID = 5192110064932207033L;
		
	private boolean connectionOpened=false;
	
    private String DB_URL = "jdbc:h2:~/"+getLocalName()+"LOG_DB";
    
    //  Database credentials
    private String USER = "";
    static final String PASS = "";
    
    //Database connection
    Connection conn = null;
	
	@Override
	protected void main() {
		this.setLogicSpeed(3000);	
	}
	
	@Override
	protected void actuatorSetup() {}

	@Override
	public Decision actuate(Actuation toActuate) {
		
		Decision dec = new Decision();
		
		//extract device information
		StringMessage logMessage=((StringMessage)toActuate.getActionRequested().getWhat());
				
		//DB connection
		if (!isConnectionOpened()) connect();
		
		//DB write log message
		try {
			writeLog(logMessage);
			dec.setDecisionExitus(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
		/*
		try {
			dumpLog();
		} catch (SQLException e) {
			e.printStackTrace();
		}*/
		
		return dec;
	}
	
	
	@Override
	public String[] setService2Search() {
		return null;
	}

	@Override
	public String[] setServiceOffered() {
		String[] services={ServicesNamespace.Logger};
		return services;
	}

	
	///////////////////////
	/// Logging Methods ////
	/////////////////////////

	private void dumpLog() throws SQLException {
		
		String sql="Select * from LOG";
        
		ResultSet set=queryDB(sql);
		
		if (!set.next()) {       //if rs.next() returns false, then there are no rows.
				System.out.println("No records found");
				}
		else {
				do {
					String rowDump="Row:"+set.getRow()+"|| Agent: "+set.getString("AGENT_NAME")+ "# Message: "+set.getString("MESSAGE")+"# TIME: "+set.getTimestamp("TIME");
					System.out.println(rowDump);
					} while (set.next());
			}
		
}

	private void writeLog(StringMessage logMessage) throws Exception {
			
		String sql="INSERT INTO LOG(AGENT_NAME,MESSAGE,TIME) VALUES('" +logMessage.getSender() +"','"+logMessage.getMessage()+"',NOW());";
        
		if(executeQuery(sql)<0) throw new Exception("LOG WRITE ERROR");
	}

	private void connect() {
		
        //Open a connection
        try {
			conn = DriverManager.getConnection(DB_URL, USER, PASS);  //if DB doesn't exist, it create a new one
		} catch (SQLException e) {
			e.printStackTrace();
		}
	
        String sql="CREATE TABLE IF NOT EXISTS LOG (ID INT AUTO_INCREMENT PRIMARY KEY, AGENT_NAME VARCHAR(255)  NOT NULL , MESSAGE VARCHAR(255)  NOT NULL, TIME TIMESTAMP NOT NULL);";
        
        executeQuery(sql);
        
		//write log created
		this.addPermanently2AgentStore("Connection "+new Date().toString(), "Opened");
		//connection OK!
		setConnectionOpened(true);
	}
	
	
	//////////////////////////
	//// getter setter //////
	///////////////////////
	
	
	
	public boolean isConnectionOpened() {
		return connectionOpened;
	}

	public void setConnectionOpened(boolean connectionOpened) {
		this.connectionOpened = connectionOpened;
	}

	
	
			///////////////////////
			/// DB queryng ////
			/////////////////////////
	
	private int executeQuery(String sql){
		
		 Statement stmt = null;
		 int ret = 0;
	    
	       try {
	   		stmt = conn.createStatement();
	   	} catch (SQLException e) {
	   		e.printStackTrace();
	   	}

	       try {
	   		ret=stmt.executeUpdate(sql);
	   	} catch (SQLException e) {
	   		e.printStackTrace();
	   	}
		return ret;
	}
	
	
	private ResultSet queryDB(String sql){
		
		 Statement stmt = null;
		 ResultSet ret = null;
	    
	       try {
	   		stmt = conn.createStatement();
	   	} catch (SQLException e) {
	   		e.printStackTrace();
	   	}

	       try {
	   		ret=stmt.executeQuery(sql);
	   	} catch (SQLException e) {
	   		e.printStackTrace();
	   	}
		return ret;
	}
}

