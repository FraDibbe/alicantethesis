package fueraFrm;

import java.util.HashMap;
import java.util.Map;

public class VoiceVocabulary {

	public enum Idioms { 
		   English, Spanish
		}
	
	private static HashMap<String,String> vocabularyEng; 
	private static HashMap<String,String> vocabularySpan;
	static
    {
		
		/*NOTE: the Map is String to (String-Int). The first one is rapresent the command that the person need to say,
		*the pair (String-Int) instead rapresents: 
		*	- String: the command to be submitted to REST Web Service
		*   - Int. the position in the command. Command is a 3 place string, where each pices are separated by ':' and them means
		*   		func:deviceId:value
			So saying "switch-0" means 'switch' is the func of the command because it is in the 0th position
		*/
		
		
    	vocabularyEng= new HashMap<String,String>();
    	vocabularySpan= new HashMap<String,String>();
		/* ***fill the vocabularies*/	
												/**-English*/
    	//func
    	vocabularyEng.put("light", "switch-0");
		
		//devices
    	vocabularyEng.put("washing machine", "washingmachine-1"); //luz
    	vocabularyEng.put("fridge", "fridge-1"); //luz

    	
		//values 
    	vocabularyEng.put("on", "true-2"); //luz
    	vocabularyEng.put("off", "false-2"); //luz
		
												/**-Spanish*/
		//func
		vocabularySpan.put("luz", "switch-0");
		vocabularySpan.put("persiana", "moveBlind-0");
		
		//devices
		vocabularySpan.put("televisor", "televisor-1");
		vocabularySpan.put("frigo", "fridge-1");
		vocabularySpan.put("lavadora", "washingmachine-1");
		
		//vocabularySpan.put("televisor", "17-1"); //luz
		//vocabularySpan.put("sala", "29-1"); //luz
		//vocabularySpan.put("cocina", "15-1"); //luz
		//vocabularySpan.put("derecha", "32-1"); //persiana
		//vocabularySpan.put("izquierda", "31-1"); //persiana
				
		//values 
		vocabularySpan.put("encender", "true-2"); //luz
		vocabularySpan.put("apagar", "false-2"); //luz
		
		vocabularySpan.put("bajar", "2-2");  //persiana
		vocabularySpan.put("levantar", "1-2"); //persiana
		vocabularySpan.put("parar", "0-2"); //persiana	
	}
		
	private VoiceVocabulary(){}//singleton: use getInstance
	
	private static HashMap<String,String> getVocabularyEng()
	{
		return vocabularyEng;	
	}
	
	private static HashMap<String,String> getVocabularySpan()
	{
		return vocabularySpan;	
	}
	
	public static Map<String, String> getInstance(Idioms language)
	{
		Map<String,String> vocabulary=null;
		
		switch(language)
		{
		case English:
			vocabulary= getVocabularyEng();
			break;
			
		case Spanish:
			vocabulary= getVocabularySpan();
			break;
		}
		
		return vocabulary;
	}
	
}
