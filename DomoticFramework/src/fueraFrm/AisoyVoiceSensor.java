package fueraFrm;

import hardware.ros.ROSCallback;
import hardware.ros.RosClient;
import ontology.actions.SetScene;
import ontology.concepts.Convertor;
import ontology.concepts.Perception;
import ontology.concepts.StringMessage;
import configurations.Resource;
import configurations.ServicesNamespace;
import edu.wpi.rail.jrosbridge.JRosbridge;
import edu.wpi.rail.jrosbridge.Ros;
import edu.wpi.rail.jrosbridge.messages.Message;
import edu.wpi.rail.jrosbridge.services.ServiceResponse;
import jade.content.Concept;
import jade.lang.acl.ACLMessage;
import agents.typos.SensorAgent;

public class AisoyVoiceSensor extends SensorAgent {
	private static final long serialVersionUID = 6152284136284068652L;
	
	private final static String DEFAULT_HOST ="aisoy1.local";
	private String host;
	private Ros ros;
	private RosClient rosClient;
	private String grammar="";
	private boolean isConnected;
	
	@Override
	protected void main() {
		//this.setLogicSpeed(100);
		
		//define host equals to Default_Host (change it here, if needed)
		host=DEFAULT_HOST;
		
		//load the grammar
		grammar=" \"Escena de Entrada \"| \"Escena de salida\"| \" Baja el estor \" ";
		
	}
		
	@Override
	protected void sensorSetup() {
	
		//connect to ROS
			ros = new Ros(host,9090, JRosbridge.WebSocketType.wss);
		    isConnected=ros.connect();
		    if(isConnected){
		    	System.out.println("ROS connected");		
		    
		
		//create ROS client
		    rosClient = new RosClient(ros);
		      
	     //start ASR service: set language of the grammar  
		    rosClient.callROSservice(Resource.ASR_grammar_Airos_ServiceName, 
   					Resource.ASR_grammar_Airos_messageType, 
   					"es",
   					new ROSCallback<ServiceResponse>(){
						@Override
						public void handleROStopic(ServiceResponse message) {
						//do nothing here	
						}}); 

		    
		 //start ASR service: send a message to the robot with the "grammar" a list of words or phrases to be recognized   
		   rosClient.callROSservice(Resource.ASR_grammar_Airos_ServiceName, 
				   					Resource.ASR_grammar_Airos_messageType, 
				   					grammar,
				   					new ROSCallback<ServiceResponse>(){
										@Override
										public void handleROStopic(ServiceResponse message) {
										//do nothing here	
										}}); 
		    
		   
		   
		  //subscribe to ROS topic: ASR (add the perception when something is recognized)
		    rosClient.subscribeROStopic(Resource.ASR_Airos_topicName, Resource.ASR_Airos_messageType, new ROSCallback<Message>() {
				@Override
				public void handleROStopic(Message message) {
				
					//add the perception to the agent perceptions list
					add_RAW_Perception(new Perception(message));
				}});
		   
		    }
	}

	@Override
	public Convertor elaboratePerception(Perception percept) {
		
		//obtain the 'right' perception
		Message msg = (Message)percept.getPerception();
		
		String order=msg.toString();
		
		
        return new Convertor(order);
		}

	
	@Override
	public Concept convert(Convertor toConvert) {
		
		//obtain the 'right' Conversion object
		 String[] cmds = (String[])toConvert.getObjectsOfConvertion();
		 
		 StringMessage strCmd = new StringMessage();
		 
		 strCmd.setMessage(cmds[0]);
		 strCmd.setSeparted(false);
		 
		 //create a message for actuators agents
		 
		SetScene action=new SetScene();
	 	action.setWhat(strCmd);
	 	action.setService(ServicesNamespace.Scene);
 	
		 		 
   return action;		
}
		 	

	@Override
	public String[] setService2Search() {
		String[] services={
		           ServicesNamespace.Scene
				   };
	return services;
	}

	@Override
	public String[] setServiceOffered() {
		return null;
	}

	
}

