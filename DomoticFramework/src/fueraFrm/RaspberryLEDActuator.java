package fueraFrm;


import ontology.actions.ShutDown;
import ontology.actions.TurnOn;
import ontology.concepts.Actuation;
import ontology.concepts.DeviceConcept;
import ontology.concepts.StringMessage;
import hardware.raspberry.GPIO_HW;
import hardware.raspberry.RaspberryConfig;

import com.pi4j.io.gpio.Pin;
import com.pi4j.io.gpio.PinState;

import configurations.ServicesNamespace;
import agents.basicAgent.baseBehaviour.Decision;
import agents.typos.ActuatorAgent;


public class RaspberryLEDActuator extends ActuatorAgent {
	private static final long serialVersionUID = 5192110064932207033L;

	private GPIO_HW gpioController=null;
	
	@Override
	protected void main() {
		this.setLogicSpeed(70);
		this.setLogService(ServicesNamespace.Logger);
	}
	
	@Override
	protected void actuatorSetup() {
		//Take the control of GPIO HW controller
		gpioController = new GPIO_HW();
	}

	@Override
	public Decision actuate(Actuation toActuate) {
		
		Decision dec = new Decision();
		
		PinState action=null;
		
		//extract the actuation action
		if(toActuate.getActionRequested() instanceof TurnOn)
		{
			action=PinState.HIGH;
		}
		else if (toActuate.getActionRequested() instanceof ShutDown)
		{
			action=PinState.LOW;
		}
		
		//extract device information
		String devName = ((DeviceConcept)toActuate.getActionRequested().getWhat()).getDeviceName();
		
		//EnergyDeviceConcept edc= (EnergyDeviceConcept) findDevice(devID); (if needed)
		
		//device Id conversion
		Pin pin = RaspberryConfig.PinConfiguration.get(devName);
	
		if(pin != null && action != null){
			//Actuate on HW
			if(action.equals(PinState.HIGH)) gpioController.pinHigh(pin);
			else if(action.equals(PinState.LOW)) gpioController.pinLow(pin);
			dec.setDecisionExitus(true);
			//TTS string
			String[] stTTS={"Device "+ devName + " is "+toActuate.getClass()};
			dec.setStringValueOfDecision(stTTS);
		}
		
		try {
			StringMessage log=new StringMessage();

	 		log.setMessage("LED "+devName+" state modified!");
			sendToLoggers(log);
		} catch (Exception e) {
			e.printStackTrace();
		}
	
		return dec;
	}

	@Override
	public String[] setService2Search() {
		return null;
	}

	@Override
	public String[] setServiceOffered() {
		String[] services={ServicesNamespace.Illumination};
		return services;
	}


	
}

