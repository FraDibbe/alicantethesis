package fueraFrm;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Set;

import configurations.Resource;
import configurations.ServicesNamespace;
import jade.content.Concept;
import jade.core.behaviours.TickerBehaviour;
import ontology.concepts.Convertor;
import ontology.concepts.StringMessage;
import Client.DeviceCommandClient;
import Client.exception.AuthRequiredException;
import agents.typos.SensorPollAgent;

public class RESTSensorAgent extends SensorPollAgent {
	
	private static final long serialVersionUID = -1181652795819377983L;
	private DeviceCommandClient<String> devCommClient;
	private HashMap<Integer,String> monitoredDeviceID;
	
	
	@Override
	protected void main() {
		this.setLogicSpeed(50000);
		this.setLogService(ServicesNamespace.Logger);
		
		//fill this array with all the device ID this agent should monitor
		monitoredDeviceID = new HashMap<Integer,String>(7);
		monitoredDeviceID.put(17,"dataSwitch");
		monitoredDeviceID.put(18,"dataSwitch");
		monitoredDeviceID.put(19,"dataSwitch");
		monitoredDeviceID.put(20,"dataSwitch");
		monitoredDeviceID.put(21,"dataSwitch");
		monitoredDeviceID.put(22,"dataHumidity");
		monitoredDeviceID.put(24,"dataSensor");
		
		/*this is the backup to DB behavior*/
		addBehaviour(new TickerBehaviour(this, getLogicSpeed()*10) {
			
			private static final long serialVersionUID = 7667996668654401396L;

			@Override
			protected void onTick() {
				//get the temporary agent store and save to the DB (permanently)
				
				Set<Entry<Integer, String>> thisSet=monitoredDeviceID.entrySet();
				Iterator<Entry<Integer, String>> it=thisSet.iterator();
				
				while(it.hasNext())
				{
					Entry<Integer, String> entr=it.next();
					
					String key=String.valueOf(entr.getKey());
					
					//get from the permanent agent store
					Object data=getfromAgentStore(key);
					
					//save to the DB
					addPermanently2AgentStore(key, data);	
				}
				
			}
		});
		
	}
	
	
	@Override
	protected void sensorSetup() {
		//create the LogClient object and configure it (authorizing)
		try {
				this.devCommClient=new DeviceCommandClient<String>(Resource.Server_Hostname);
				devCommClient.setAuth(Resource.username, Resource.password);
			} catch (URISyntaxException e) {
				e.printStackTrace();
			}
	}

	@Override
	public Convertor percept() {
		
		Set<Entry<Integer, String>> thisSet=monitoredDeviceID.entrySet();
		Iterator<Entry<Integer, String>> it=thisSet.iterator();
		
		while(it.hasNext())
		{
			Entry<Integer, String> entr=it.next();
			try {
				
				//get the data
				String data=devCommClient.getData( entr.getKey(),entr.getValue() );
				
				String key=String.valueOf(entr.getKey());
				
				//save them in a no permanent way (for speed reasons)
				if( getfromAgentStore(key) != null 
						&& 
						( !((String)getfromAgentStore(key)).equals(data) ) )
				{
					//send an "alarm" to the log				
					try {
				 		StringMessage log=new StringMessage();

				 		log.setMessage("Detect modify in device"+ key + " , now is: "+ data);
						sendToLoggers(log);
					} catch (Exception e) {
						e.printStackTrace();
					}
					
				}
				//modify the value
				this.addtoAgentStore(String.valueOf(entr.getKey()), (Object)data);
				
				
			} catch (IOException | AuthRequiredException e) {
				e.printStackTrace();
			}
		}
		
		return null; //i don't send the data to other agents
	}

	
	@Override
	public Concept convert(Convertor conv) {
		return null;
	}

	@Override
	public String[] setService2Search() {
		return null;
	}

	@Override
	public String[] setServiceOffered() {
		return null;
	}

}
