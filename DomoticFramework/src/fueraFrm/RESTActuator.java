package fueraFrm;


import java.io.IOException;
import java.net.URISyntaxException;

import ontology.concepts.Actuation;
import ontology.concepts.DeviceConcept;
import configurations.Resource;
import configurations.ServicesNamespace;
import Client.DeviceCommandClient;
import Client.exception.AuthRequiredException;
import agents.basicAgent.baseBehaviour.Decision;
import agents.typos.ActuatorAgent;


public class RESTActuator extends ActuatorAgent {
	private static final long serialVersionUID = 5192110064932207033L;

	private DeviceCommandClient<String> devCommClient;
		
	@Override
	protected void main() {
		this.setLogicSpeed(70);
		
	}
	
	@Override
	protected void actuatorSetup() {
		//create the LogClient object and configure it (authorizingg)
		try {
			this.devCommClient=new DeviceCommandClient<String>(Resource.Server_Hostname);
			devCommClient.setAuth(Resource.username, Resource.password);
			
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}
	}

	@Override
	public Decision actuate(Actuation toActuate) {
		
		Decision dec = new Decision();
		
		//extract device information
		int devID = ((DeviceConcept)toActuate.getActionRequested().getWhat()).getDeviceId();
		
		//EnergyDeviceConcept edc= (EnergyDeviceConcept) findDevice(devID);
		
		//extract the actuation action
			try {
				devCommClient.giveCommand(devID, toActuate.getActionRequested().getHW_dependend_command(), toActuate.getActionRequested().getValue());
				dec.setDecisionExitus(true);
			} catch (IOException| AuthRequiredException e) {
				e.printStackTrace();
			}	
			
		return dec;	
	}

	@Override
	public String[] setService2Search() {
		return null;
	}

	@Override
	public String[] setServiceOffered() {
		String[] services={ServicesNamespace.REST_General_ACT};
		return services;
	}

	
}

