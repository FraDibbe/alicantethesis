package fueraFrm;

import configurations.ServicesNamespace;
import ontology.actions.Do;
import ontology.actions.Order;
import ontology.actions.Say;
import ontology.concepts.DeviceConcept;
import ontology.concepts.StringMessage;
import ontology.predicates.Acepted;
import ontology.predicates.FIPADecison;
import agents.basicAgent.baseBehaviour.Decision;
import agents.typos.MediatorAgent;

public class StringerMediator extends MediatorAgent {

	private static final long serialVersionUID = -7001771688600516927L;

	@Override
	protected void main() {
	}

	@Override
	public Decision decide(Order order) {
		
		Decision dec = new Decision();
		
		int rdm = ((DeviceConcept)order.getWhat()).getDeviceId();
		
		System.out.println("manager Ricevuto:"+ rdm);
		
		if(rdm > 50) 
			{
				
			Do act= new Do();
			
			act.setWhat(order.getWhat());
			act.setHW_dependend_command("sas");
			act.setService(ServicesNamespace.ASR);

			try {
				sendToActuators(act,true);
				dec.setDecisionExitus(true);
				return dec;
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			}

	return dec;
	}

	
	@Override
	public void handleFIPAresponse(FIPADecison predicate) {
		if (predicate instanceof Acepted)
		{
			String[] sa = predicate.getWhat();
			
			if (sa.length > 0)
			{
				/*Actuation message*/
				Say sy = new Say();
				
				StringMessage strCmd = new StringMessage();
				 
				strCmd.setMessage(predicate.getWhat()[0]);
				strCmd.setSeparted(false);
				 
				sy.setWhat(strCmd);
				sy.setService(ServicesNamespace.TTS);
				
				try {
					sendToActuators(sy,false);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	};
	
	@Override
	public String[] setService2Search() {
		String[] services={ServicesNamespace.ASR, ServicesNamespace.TTS};
		return services;
	}

	@Override
	public String[] setServiceOffered() {
		String[] services={ServicesNamespace.ButtonControl};
		return services;
	}

}
