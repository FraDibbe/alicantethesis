package fueraFrm;


import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

import ontology.concepts.Actuation;
import ontology.concepts.StringMessage;
import configurations.ServicesNamespace;
import Client.exception.AuthRequiredException;
import agents.basicAgent.baseBehaviour.Decision;
import agents.typos.ActuatorAgent;


public class TTSActuator extends ActuatorAgent {

	private static final long serialVersionUID = 8207521939593175962L;
	
	private final static String Default_Google_URL ="https://translate.google.com/translate_tts?ie=utf-8&tl=";
	
	private String language;
	private File myMp3;
	private FileOutputStream fop = null;
	
	CloseableHttpClient client;
	
	@Override
	protected void main() {
		
	}
	
	@Override
	protected void actuatorSetup() {
		
		client=HttpClients.createDefault();
		
		language="en";    
	}

	@Override
	public Decision actuate(Actuation toActuate) {
		
		Decision dec = new Decision();
		
		String toSpeach=((StringMessage)toActuate.getActionRequested().getWhat()).getMessage();
				
		/*create the TTS URL string*/
		String TTString = Default_Google_URL+language+"&q="+toSpeach.replace(" ", "%20");
		
		CloseableHttpResponse response = null;
		
		try {
			 response = executeRequest(client,TTString);
			 
			 	myMp3 = new File("mp3temp.mp3");
				fop = new FileOutputStream(myMp3);
	 
				// if file doesnt exists, then create it
				if (!myMp3.exists()) {
					myMp3.createNewFile();
				}
				
			 //save the response in a temporary mp3 file
			 response.getEntity().writeTo(fop);
			 
			 fop.flush();
			 fop.close();
			 
		} catch (IOException | AuthRequiredException e) {
			e.printStackTrace();
		}
		
		//i have a response, so extract the MP3 content
		if(response != null)
		{
			try {
				Runtime.getRuntime().exec("mplayer mp3temp.mp3 -af extrastereo=0 &");
				dec.setDecisionExitus(true);
				myMp3.delete();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		return dec;
	}

	@Override
	public String[] setService2Search() {
		return null;
	}

	@Override
	public String[] setServiceOffered() {
		String[] services={ServicesNamespace.TTS};
		return services;
	}


///////////////////////////////
/// AGENT SPECIFIC METHODS ///
/////////////////////////////
	
	private CloseableHttpResponse executeRequest(CloseableHttpClient client, String request) throws IOException, AuthRequiredException
    {		
		HttpGet httpget = new HttpGet(request);
		
		
		CloseableHttpResponse response = client.execute(httpget);

		int responseCode = response.getStatusLine().getStatusCode();
			
		if (responseCode == 401) {
			client.close();
			throw new AuthRequiredException("Server require authentication");
		}
		
		return response;
    }
}

