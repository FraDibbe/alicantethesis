package fueraFrm;

import java.util.ArrayList;
import java.util.Map.Entry;

import ontology.actions.Order;
import ontology.actions.Order.OrderType;
import ontology.concepts.Convertor;
import ontology.concepts.EnergyDeviceConcept;
import ontology.concepts.Perception;
import ontology.concepts.StringMessage;
import ontology.predicates.Acepted;
import ontology.predicates.FIPADecison;
import utils.json.EnergyDeviceXMLparser;
import hardware.raspberry.GPIO_HW;
import hardware.raspberry.RaspberryConfig;

import com.pi4j.io.gpio.Pin;
import com.pi4j.io.gpio.event.GpioPinDigitalStateChangeEvent;
import com.pi4j.io.gpio.event.GpioPinListenerDigital;

import configurations.ServicesNamespace;
import jade.content.Concept;
import jade.lang.acl.ACLMessage;
import agents.typos.SensorAgent;

public class RaspberryButtonSensor extends SensorAgent {
	
	private static final long serialVersionUID = 5192110064932207033L;

	 private String value="true";
	 private String buttonPressed;
	 private GPIO_HW gpio;
	
	@Override
	protected void main() {
	//	this.setLogicSpeed(100);
		this.setPath("devicesXML/");
		this.setLogService(ServicesNamespace.Logger);
	}
	
	@Override
	protected void sensorSetup() {
		
		/** DEFINIRE QUI GLI HANDLER PER LA PRESSIONE DEI BOTTONI */
		//HW interaction
		gpio = new GPIO_HW();
		
		
		
		//get all the buttons
		Pin button1=RaspberryConfig.PinConfiguration.get("buttonWashingMachine");
		Pin button2=RaspberryConfig.PinConfiguration.get("buttonFridge");
		Pin button3=RaspberryConfig.PinConfiguration.get("buttonAirConditioning");
		Pin button4=RaspberryConfig.PinConfiguration.get("buttonBoiler");
		
		ArrayList<Pin> listPin = new ArrayList<Pin>(4);
		listPin.add(button1);
		listPin.add(button2);
		listPin.add(button3);
		listPin.add(button4);
		
		this.checkLedState(listPin);
		
		/*start to sense them, link the perception to GPIO's events:*/
		
		//washing machine
		gpio.sensePin(button1, new GpioPinListenerDigital() {
			@Override
			public void handleGpioPinDigitalStateChangeEvent(
					GpioPinDigitalStateChangeEvent event) {
				
				//save the object inside the agent perceptions
				add_RAW_Perception(new Perception(event));
			}
		});
		
		//fridge
		gpio.sensePin(button2, new GpioPinListenerDigital() {
			@Override
			public void handleGpioPinDigitalStateChangeEvent(
					GpioPinDigitalStateChangeEvent event) {
				
				//save the object inside the agent perceptions
				add_RAW_Perception(new Perception(event));
			}
		});
		
		//air conditiong
		gpio.sensePin(button3, new GpioPinListenerDigital() {
			@Override
			public void handleGpioPinDigitalStateChangeEvent(
					GpioPinDigitalStateChangeEvent event) {
				
				//save the object inside the agent perceptions
				add_RAW_Perception(new Perception(event));
			}
		});
		
		
		//boiler
		gpio.sensePin(button4, new GpioPinListenerDigital() {
			@Override
			public void handleGpioPinDigitalStateChangeEvent(
					GpioPinDigitalStateChangeEvent event) {
				
				//save the object inside the agent perceptions
				add_RAW_Perception(new Perception(event));
			}
		});
		
	}


	@Override
	public Convertor elaboratePerception(Perception percept) {
		
		//obtain the 'right' perception
		GpioPinDigitalStateChangeEvent event = (GpioPinDigitalStateChangeEvent)percept.getPerception();
		
		//get the Pin corresponding to the GPIO eventName
		buttonPressed = null;
			 
			for ( Entry<String, Pin> entry : RaspberryConfig.PinConfiguration.entrySet()) {
				if( entry.getValue().equals( event.getPin().getPin()) ) 
					{
						buttonPressed= entry.getKey();
						break;
					}
			    }
			
			if(buttonPressed !=null)
			{
			//match the button name with the its functionality
			String deviceName=RaspberryConfig.functionality2device.get(buttonPressed);
					
			if(event.getState().isHigh() ) //actuate only when the button is pressed! 
				{
				  				   
				   String temp=findPermanentlyINAgentStore(buttonPressed); //is only a toggle button: so let see it previous state [saved into the agent]	
				  
				   if( temp != null && temp.equalsIgnoreCase("TRUE") ) value="false";
				   else if (temp != null ) value="true";
				  	 
				   String[] params={"switch",deviceName,value,buttonPressed};
				   
				   return new Convertor((Object[])params);
				}
			}
			return null;	
		}

	@Override
	public Concept convert(Convertor convert) {
		
		//obtain the 'right' Conversion object
		 String[] cmds = (String[])convert.getObjectsOfConvertion();
		 
		 //datas
		 	String function=cmds[0];
		 	String deviceName=cmds[1];
		 	String value=cmds[2];
		 	String button=cmds[3];
		 	
		 	
		 	// Prepare the content of msg to be used with the ontology
		   EnergyDeviceConcept deviceObj= (EnergyDeviceConcept) findDevice(deviceName,new EnergyDeviceXMLparser());	
		  		   
		 	/* Use this to communicate  direcly with an Actuator/
		 	Do action=new Do();
		 	
		 	if(function.equalsIgnoreCase("switch") && value.equalsIgnoreCase("true"))		
		 		action=new TurnOn();
		 	else if(function.equalsIgnoreCase("switch") && value.equalsIgnoreCase("false"))
		 		action=new ShutDown();
		
	 		 action.setWhat(deviceObj);
	 		 action.setHW_dependend_command("switch");
	 		 action.setService(ServicesNamespace.Illumination);
	 		action.setPerformative(ACLMessage.INFORM);
	 		*/

		
		 	/*Use this to communicate to a Mediator */
		 	Order ord =new Order();
		 	
		 	ord.setWhat(deviceObj);
		 	ord.setPriority(deviceObj.getPriority());
		 	ord.setService(ServicesNamespace.EnergyModify);
		   	
		 	if(value.equalsIgnoreCase("true"))	ord.setType(OrderType.DIRECT);
		 	else ord.setType(OrderType.INDIRECT);
		   	
		 	try {
		 		StringMessage log=new StringMessage();

		 		log.setMessage("Order Sent! _ "+deviceObj.getDeviceName());
				sendToLoggers(log);
			} catch (Exception e) {
				e.printStackTrace();
			}
		 	
   return ord;		
}
		 	
	/*Update the knowledge of the agent, based on the answers of Actuators/Mediators [FIPA A-RE protocol]*/
	@Override
	public void handleFIPAresponse(FIPADecison predicate) {
		
	if(predicate instanceof Acepted) //change the knowledge of the agent only if the action is accepted by other agents
		{
			addPermanently2AgentStore(buttonPressed, value); //update agent knowledge about the state of this button 
		}
						
	};
	

	@Override
	public String[] setService2Search() {
		String[] services={
				           ServicesNamespace.EnergyModify,
						   ServicesNamespace.Illumination
						   };
		return services;
	}

	@Override
	public String[] setServiceOffered() {
		return null;
	}

///////////////////////////////
/// AGENT SPECIFIC METHODS ///
/////////////////////////////
	
	private void checkLedState(ArrayList<Pin> listPin) 
	{
		
		String button=null;
		
		for(Pin pin : listPin)
		{
			for ( Entry<String, Pin> entry : RaspberryConfig.PinConfiguration.entrySet()) {
				if( entry.getValue().equals(pin) ) 
					{
						button= entry.getKey();
						break;
					}
			    }
			
		if(button != null){	
			//sense pin state
			if(gpio.isDigitalPinHIGH(pin)) {
				addPermanently2AgentStore(button, "true");
			}
			else
				addPermanently2AgentStore(button, "false");
			}
		}	
	
	}

	
	
}

