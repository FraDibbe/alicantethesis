package fueraFrm;

import java.util.HashMap;
import java.util.Map;

public class VoiceVocabulary2 {

	public enum Idioms { 
		   English, Spanish
		}
	
	private static HashMap<String,String> vocabularyEng; 
	private static HashMap<String,String> vocabularySpan;
	static
    {
		
	
    	vocabularyEng= new HashMap<String,String>();
    	vocabularySpan= new HashMap<String,String>();
		/* ***fill the vocabularies*/	
												/**-English*/
    			
												/**-Spanish*/
    	/*
		//func
		vocabularySpan.put("encender", "0-switch-2-true");
		vocabularySpan.put("apagar", "0-switch-2-false");
		vocabularySpan.put("bajar", "0-moveBlind-2-2");
		vocabularySpan.put("parar", "0-moveBlind-2-0");
		vocabularySpan.put("levantar", "0-moveBlind-2-1");
		
		vocabularySpan.put("lavadora", "1-washingmachine");
		vocabularySpan.put("frigo", "1-fridge");
		vocabularySpan.put("boiler", "1-boiler");
		
		//TODO: how to manage 2 words nouns? (like air conditioning) or (luz sala)
		vocabularySpan.put("aireacondicionada", "1-airconditioning");
		
		vocabularySpan.put("luzsala", "1-halllight");
		vocabularySpan.put("luzpanel1", "1-panel1light");
		vocabularySpan.put("luzpanel2", "1-panel2light");
		vocabularySpan.put("luzpanel3", "1-panel3light");
		
		vocabularySpan.put("persianaderecha", "1-persianaderecha");
		vocabularySpan.put("persianaizquierda", "1-persianaizquerda");
	*/	
	
    	vocabularySpan.put("encender luz panel 1", "0-switch-1-panel1light-2-true");
		vocabularySpan.put("encender luz panel 2", "0-switch-1-panel2light-2-true");
		vocabularySpan.put("encender luz panel 3", "0-switch-1-panel3light-2-true");
		
		vocabularySpan.put("apagar luz panel 1", "0-switch-1-panel1light-2-false");
		vocabularySpan.put("apagar luz panel 2", "0-switch-1-panel2light-2-false");
		vocabularySpan.put("apagar luz panel 3", "0-switch-1-panel3light-2-false");
		
		vocabularySpan.put("bajar persiana derecha", "0-moveBlind-1-persianaderecha-2-2");
		vocabularySpan.put("bajar persiana izquierda", "0-moveBlind-1-persianaizquerda-2-2");
		
		vocabularySpan.put("levantar persiana derecha", "0-moveBlind-1-persianaderecha-2-1");
		vocabularySpan.put("levantar persiana izquierda", "0-moveBlind-1-persianaizquerda-2-1");
		
		vocabularySpan.put("parar persiana derecha", "0-moveBlind-1-persianaderecha-2-0");
		vocabularySpan.put("parar persiana izquierda", "0-moveBlind-1-persianaizquerda-2-0");
		
	}
		
	private VoiceVocabulary2(){}//singleton: use getInstance
	
	private static HashMap<String,String> getVocabularyEng()
	{
		return vocabularyEng;	
	}
	
	private static HashMap<String,String> getVocabularySpan()
	{
		return vocabularySpan;	
	}
	
	public static Map<String, String> getInstance(Idioms language)
	{
		Map<String,String> vocabulary=null;
		
		switch(language)
		{
		case English:
			vocabulary= getVocabularyEng();
			break;
			
		case Spanish:
			vocabulary= getVocabularySpan();
			break;
		}
		
		return vocabulary;
	}
	
}
