package fueraFrm;

import java.net.URISyntaxException;
import java.util.ArrayList;

import ontology.actions.Say;
import ontology.concepts.Actuation;
import ontology.concepts.StringMessage;
import configurations.Resource;
import configurations.ServicesNamespace;
import Client.DeviceCommandClient;
import agents.basicAgent.baseBehaviour.Decision;
import agents.typos.ActuatorAgent;


public class RESTSceneActuator extends ActuatorAgent {

	private static final long serialVersionUID = 5677160747136836762L;
	
	private DeviceCommandClient<String> devCommClient;
		
	@Override
	protected void main() {
		this.setLogicSpeed(2000);	
	}
	
	@Override
	protected void actuatorSetup() {
		//create the LogClient object and configure it (authorizingg)
		try {
			this.devCommClient=new DeviceCommandClient<String>(Resource.Server_Hostname);
			devCommClient.setAuth(Resource.username, Resource.password);
			
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}
	}

	@Override
	public Decision actuate(Actuation toActuate) {
		
		Decision dec = new Decision();
		
		//extract device information
		String scene = ((StringMessage)toActuate.getActionRequested().getWhat()).getMessage();
		
		
		//select the scene
		
		ArrayList<String> actions= actionsScene(scene);
		
		for(int i=0;i<actions.size();i++)
		{
			String[] cmd = actions.get(i).split("-");
			try {
				devCommClient.giveCommand(Integer.parseInt(cmd[0]),cmd[1], cmd[2]);
				
				 StringMessage speakCmd = new StringMessage();
			 	 
				 speakCmd.setMessage("Activada escena"+ scene);
				 	
				 Say speak= new Say();
				 	
				 speak.setWhat(speak);
				 speak.setService(ServicesNamespace.TTS);
				
				 sendToActuators(speak,true);
				 
				dec.setDecisionExitus(true);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		
		return dec;
			
	}

	@Override
	public String[] setService2Search() {
		String[] services={ServicesNamespace.TTS};
		return services;
	}

	@Override
	public String[] setServiceOffered() {
		String[] services={ServicesNamespace.Scene};
		return services;
	}

	
	private ArrayList<String> actionsScene(String sceneType)
	{
		ArrayList<String> acts= new ArrayList<String>();
		
		if(sceneType.equalsIgnoreCase("Escena de entrada"))
		{
			acts.add("29-switch-true");
			acts.add("30-switch-true");
			acts.add("31-moveBlind-2");
			acts.add("32-moveBlind-2");
			
		}
		else if (sceneType.equalsIgnoreCase("Escena de salida"))
		{
			acts.add("29-switch-false");
			acts.add("30-switch-false");
			acts.add("31-moveBlind-1");
			acts.add("32-moveBlind-1");
		}
		else if(sceneType.equalsIgnoreCase("Baja el estor"))
		{
			acts.add("31-moveBlind-1");
			acts.add("32-moveBlind-1");
		}
		
		return acts;
	}
	
	
}

