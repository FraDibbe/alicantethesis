package fueraFrm;


import ontology.concepts.Actuation;
import ontology.concepts.StringMessage;
import hardware.ros.ROSCallback;
import hardware.ros.RosClient;
import configurations.Resource;
import configurations.ServicesNamespace;
import edu.wpi.rail.jrosbridge.Ros;
import edu.wpi.rail.jrosbridge.services.ServiceResponse;
import agents.basicAgent.baseBehaviour.Decision;
import agents.typos.ActuatorAgent;


public class AisoyVoiceActuator extends ActuatorAgent {
	private static final long serialVersionUID = 5192110064932207033L;

	private final static String DEFAULT_HOST ="aisoy1.local";
	private String host;
	private Ros ros;
	private RosClient rosClient;
	private boolean isConnected;
	
	@Override
	protected void main() {
		this.setLogicSpeed(100);
		
		//define host equals to Default_Host (change it here, if needed)
		host=DEFAULT_HOST;
	}
	
	@Override
	protected void actuatorSetup() {
		
		//connect to ROS
		ros = new Ros(host);
	     isConnected=ros.connect();
	    if(isConnected)
	    {
	    System.out.println("ROS connected");		
	    
	    //create ROS client
	    rosClient = new RosClient(ros);
	    }
	    
	    
	}

	@Override
	public Decision actuate(Actuation toActuate) {
		
		Decision dec = new Decision();
		
		String toSpeach=((StringMessage)toActuate.getActionRequested().getWhat()).getMessage();
		
		if(isConnected)
		{
		 //say something   
		   rosClient.callROSservice(Resource.Say_Airos_ServiceName, 
				   					Resource.Say_Airos_messageType, 
				   					" \" "+toSpeach+ "\" true",
				   					new ROSCallback<ServiceResponse>(){
										@Override
										public void handleROStopic(ServiceResponse message) {
										//do nothing here	
										}}); 
		  dec.setDecisionExitus(true);	
		}
		return dec;
	}

	@Override
	public String[] setService2Search() {
		return null;
	}

	@Override
	public String[] setServiceOffered() {
		String[] services={ServicesNamespace.TTS};
		return services;
	}


	
}

