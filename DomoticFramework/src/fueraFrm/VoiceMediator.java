package fueraFrm;

import java.util.HashMap;

import configurations.ServicesNamespace;
import ontology.actions.Do;
import ontology.actions.Order;
import ontology.actions.Order.OrderType;
import ontology.concepts.DeviceConcept;
import ontology.concepts.EnergyDeviceConcept;
import ontology.concepts.StringMessage;
import utils.json.DeviceXMLparser;
import utils.json.EnergyDeviceXMLparser;
import utils.json.GeneralDeviceXMLparser;
import agents.basicAgent.baseBehaviour.Decision;
import agents.typos.MediatorAgent;

public class VoiceMediator extends MediatorAgent {

	private static final long serialVersionUID = -8338510234235435949L;
	
	/* Vocabulary Map*/
	HashMap<String, String> vocabulary;
	

	@Override
	protected void main() {
		//setLogicSpeed(5000);
		setLogService(ServicesNamespace.Logger);
		vocabulary = (HashMap<String, String>) VoiceVocabulary2.getInstance(fueraFrm.VoiceVocabulary2.Idioms.Spanish);
		//this.setPath("C:/Users/Fra/Desktop/dev/");
		this.setPath("devicesXML/");
	}

	@Override
	public Decision decide(Order order) {
		
		StringMessage msgOrder=(StringMessage)order.getWhat();
		
		String msg=msgOrder.getMessage();

		String separator=" "; //define a 'default' vocal command separator....
		
		//...now let's check if another separator is used!
		if(msgOrder.isSeparted()) separator=msgOrder.getSeparator(); //set the new separator
		
		//separate the phrase in words
		String[] splitted = msg.split(separator);
	
		//this array contains a ordered and complete command: [0] Function - [1] Dev Name - [2] Value
		String[] command_ordered = new String[3];	
		
		/* ---> Old one: for vocabulary 1
		//check the worlds against the vocabulary to extract commands
		for(int k=0; k< splitted.length; k++)
		{
	 	if(vocabulary.containsKey(splitted[k]))
	 		{
	 			String associated_Value=vocabulary.get(splitted[k]);
	 			String values[]=associated_Value.split("-");
	 			int pos=Integer.parseInt(values[1]);
	 			command_ordered[pos]=values[0];
	 		}
		}*/
		
		/*
		/* ---> NEW one: for vocabulary 2/
		for(int k=0; k< splitted.length; k++)
		{
	 	if(vocabulary.containsKey(splitted[k]))
	 		{
	 			String associated_Value=vocabulary.get(splitted[k]);
	 			String values[]=associated_Value.split("-");
	 					
	 			int pos=0;
	 			for(int i=0; i<values.length; i++)
	 			{
	 				if((i % 2) == 0) 
	 					pos=Integer.parseInt(values[i]);
	 				else
	 				{
	 					command_ordered[pos]=values[i];
	 				}
	 			}
	 		}
		}
		*/
		
		/*remove white space at the end*/
		msg="";
		for(int k=0; k< splitted.length;k++){
			if(k == splitted.length-1) msg+=splitted[k];
			else msg+=splitted[k]+" ";
		}
		
		if(vocabulary.containsKey(msg))
		{
			String associated_Value=vocabulary.get(msg);
			String values[]=associated_Value.split("-");
				
 			int pos=0;
 			for(int i=0; i<values.length; i++)
 			{
 				if((i % 2) == 0) 
 					pos=Integer.parseInt(values[i]);
 				else
 				{
 					command_ordered[pos]=values[i];
 				}
 			}
 		}
	
		if(command_ordered[0] != null && command_ordered[1] != null && command_ordered[2] != null)
		{ 
			//create the Ontology object
			
			DeviceConcept general_dev= (DeviceConcept)findDevice(command_ordered[1], new GeneralDeviceXMLparser());
			
			if(general_dev.getType().equalsIgnoreCase("energy")) //it is a device which requires power
			{//Instantiate an EnergyDevice
				
				//get the right object spec
				EnergyDeviceConcept dev= (EnergyDeviceConcept)findDevice(command_ordered[1], new EnergyDeviceXMLparser());
				
				
				/* to mediators*/
				Order ord =new Order();
			 	
			 	ord.setWhat(dev);
			 	ord.setPriority(dev.getPriority());
			 	ord.setService(ServicesNamespace.EnergyModify);
			 	
			 	if(command_ordered[2].equalsIgnoreCase("false")) ord.setType(OrderType.INDIRECT); //the manager won't try to apply his policy
			 	else
			 		ord.setType(OrderType.DIRECT); //the manager will try to apply his policy to the device

			 	try {
					sendToMediators(ord,true);
				} catch (Exception e) {
					e.printStackTrace();
				}
				
			 	
			}
			else
			{//instantiate other things...
				
				DeviceConcept dev= (DeviceConcept)findDevice(command_ordered[1], new DeviceXMLparser());
				
				/* to actuators*/
				
				Do act = new Do();
			
				act.setWhat(dev);
				act.setService(ServicesNamespace.REST_General_ACT);
				act.setHW_dependend_command(command_ordered[0]);
				act.setValue(command_ordered[2]);
				
				try {
					sendToActuators(act,true);
				} catch (Exception e) {
					e.printStackTrace();
				}
				
			}
		
			try {
		 		StringMessage log=new StringMessage();

		 		log.setMessage("Voice command elaborated over device "+ general_dev.getDeviceName());
				
		 		sendToLoggers(log);
			} catch (Exception e) {
				e.printStackTrace();
			}
		
		}
		
		Decision dec = new Decision();
		dec.setDecisionExitus(true);
		
		return dec;//always eliminate the command from decision queue of this agent! If some commands are not recognized, just delete them
	}


	@Override
	public String[] setService2Search() {
		String[] services={
				ServicesNamespace.Illumination,
				ServicesNamespace.EnergyModify,
				ServicesNamespace.REST_General_ACT,
				 };
		return services;
	}

	@Override
	public String[] setServiceOffered() {
		String[] services={
				ServicesNamespace.VocalCommands
				};
		return services;
	}


}

