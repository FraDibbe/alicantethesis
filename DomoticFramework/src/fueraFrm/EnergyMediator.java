package fueraFrm;


import jade.lang.acl.ACLMessage;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import configurations.ServicesNamespace;
import ontology.actions.Order;
import ontology.actions.Say;
import ontology.actions.ShutDown;
import ontology.actions.TurnOn;
import ontology.concepts.EnergyDeviceConcept;
import ontology.concepts.StringMessage;
import ontology.predicates.Acepted;
import ontology.predicates.FIPADecison;
import utils.json.EnergyDeviceXMLparser;
import agents.basicAgent.baseBehaviour.Decision;
import agents.typos.MediatorAgent;

public class EnergyMediator extends MediatorAgent {

	private static final long serialVersionUID = -8338510234235435949L;

	private static final int MaxIstantPower = 3000;
	private HashMap<String, EnergyDeviceConcept> connectedDevices = new HashMap<String, EnergyDeviceConcept>();
	private int totalActualPower=0;
	
	private EnergyDeviceConcept dev;
	
	@Override
	protected void main() {
		this.setPath("devicesXML/");
		setLogService(ServicesNamespace.Logger);
	
		try {
			this.checkDeviceAtStart();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}


	@Override
	public Decision decide(Order order) {
		
		Decision retVal=new Decision();
		
		dev=(EnergyDeviceConcept)order.getWhat();

		if(order.getType() == Order.OrderType.INDIRECT && connectedDevices.containsKey(dev.getDeviceName()) ) {
			retVal.setDecisionExitus(ShutDown_Procedure(dev));
		}
		else
		{
	
		if(dev.getInstantPower()+totalActualPower <= MaxIstantPower )
		{ //Okay, i can light up the device! So prepare a 'Do' command for the actuators
						
			TurnOn action = new TurnOn();
			
			action.setWhat(dev);
			action.setService(ServicesNamespace.Illumination);
			action.setHW_dependend_command("switch");
			
			try {
				sendToActuators(action,true);
			} catch (Exception e) {
				e.printStackTrace();
			}
				
			//add this device to the connectedDevice Map
			this.connectedDevices.put(dev.getDeviceName(), dev);
			addPermanently2AgentStore(dev.getDeviceName(), "1");
			
			//refresh power usage
			refreshTotalPowerUsage();
			
			try {
				StringMessage log=new StringMessage();

		 		log.setMessage("Device: "+dev.getDeviceName()+" Turned ON!");
				sendToLoggers(log);
				
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			retVal.setDecisionExitus(true);
		}
		else	//okay MaxPower Reached, so:
		{
			//i collect in an arraylist all the devices that has priority < then the device i want to light up
			
			EnergyDeviceConcept arr=connectedDevices.entrySet().iterator().next().getValue();
					
			//get the device with lowest priority
			for(EnergyDeviceConcept edc: connectedDevices.values())
			{
				if( edc.getPriority() >= dev.getPriority() ) arr=edc;
			}
			//if the dev has a lower priority then the arr, discard dev
			if(arr.getPriority() < dev.getPriority()) {
				retVal.setDecisionExitus(false);
				return retVal; //discard dev
			}
			
			//otherwise, let the new device enter in the system:
			if(((totalActualPower + dev.getInstantPower()) > MaxIstantPower) && arr != null)
			//then shut down all this devices until 
			{
				ShutDown_Procedure(arr);			
				//Next round of logic should light up the device 'dev'
				decide(order);
				retVal.setDecisionExitus(true);
			}
				
		}
	}
		return retVal;
}

	
	@Override
	public void handleFIPAresponse(FIPADecison predicate) {
		if (predicate instanceof Acepted)
		{
			String[] sa = predicate.getWhat();
			
			if (sa.length > 0)
			{
				/*Actuation message*/
				Say sy = new Say();
				
				StringMessage strCmd = new StringMessage();
				 
				strCmd.setMessage(predicate.getWhat()[0]);
				strCmd.setSeparted(false);
				 
				sy.setWhat(strCmd);
				sy.setService(ServicesNamespace.TTS);
				
				try {
					sendToActuators(sy,false);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	};
	
	

	@Override
	public String[] setService2Search() {
		String[] services={
				ServicesNamespace.Illumination,
				ServicesNamespace.VocalCommands, 
				ServicesNamespace.Presence,
				ServicesNamespace.TTS
						      };
		return services;
	}

	@Override
	public String[] setServiceOffered() {
		String[] services={
				ServicesNamespace.EnergyModify
				};
		return services;
	}

	
///////////////////////////////
/// AGENT SPECIFIC METHODS ///
/////////////////////////////
	
	private boolean ShutDown_Procedure(EnergyDeviceConcept egdev) {
		//this only update the connected devices: a shutDown command was given	
		
		//shout down the device
		ShutDown action = new ShutDown();
		
		action.setWhat( egdev );
		action.setService(ServicesNamespace.Illumination);
		action.setHW_dependend_command("switch");
		
		try {
			sendToActuators(action,true);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		connectedDevices.remove(egdev.getDeviceName());
		addPermanently2AgentStore(egdev.getDeviceName(), "0");
		refreshTotalPowerUsage();
		
		try {
			StringMessage log=new StringMessage();

	 		log.setMessage("Device: "+egdev.getDeviceName()+" Turned OFF!");
			sendToLoggers(log);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return true;
		
	}

	
	private void refreshTotalPowerUsage() {
		totalActualPower=0; //reset it...
		for(EnergyDeviceConcept dev: connectedDevices.values()) totalActualPower+=dev.getInstantPower(); //...then recalculate	
	}
	
	private void checkDeviceAtStart() throws SQLException
	{

		ResultSet set=getAllPermanetAgentStore();
		
		while(set.next()) 
		{
			if(set.getString("VALUE").equalsIgnoreCase("1"))
			{
				// get the device description
			     EnergyDeviceConcept deviceObj= (EnergyDeviceConcept) findDevice(set.getString("KEY"),new EnergyDeviceXMLparser());	
				
			     connectedDevices.put(deviceObj.getDeviceName(), deviceObj);
			}
		}
		
		refreshTotalPowerUsage();
	}
	
}

