package fueraFrm;

import java.util.List;
import java.util.Random;

import configurations.ServicesNamespace;
import jade.content.Concept;
import jade.content.Predicate;
import jade.lang.acl.ACLMessage;
import ontology.actions.Do;
import ontology.actions.Order;
import ontology.actions.Order.OrderType;
import ontology.actions.Say;
import ontology.concepts.Convertor;
import ontology.concepts.DeviceConcept;
import ontology.concepts.StringMessage;
import ontology.predicates.Acepted;
import ontology.predicates.FIPADecison;
import agents.typos.SensorPollAgent;

public class StringherSensor extends SensorPollAgent {

	private static final long serialVersionUID = -7244581030215983860L;

	Random randomGenerator;
	
	@Override
	protected void main() {
		this.setLogicSpeed(16000);
		System.out.println("HI! I'm a Sensor that sends strings!");
	}

	@Override
	protected void sensorSetup() {
		randomGenerator = new Random();
	}

	@Override
	public Convertor percept() {
		 int randomInt = randomGenerator.nextInt(100);
	      System.out.println("Generated : " + randomInt);	
	      
	      //save the result in the DB
	      
	      this.addPermanently2AgentStore("NaNaNa", String.valueOf(randomInt));
	      
	      return new Convertor(randomInt);
	}

	
	@Override
	public Concept convert(Convertor conv) {
		//obtain the 'right' Conversion object
		 Object[] cmd = conv.getObjectsOfConvertion();
		 
		 //datas
		 	int rdm=(int) cmd[0];
			
		  DeviceConcept deviceObj = new DeviceConcept();
		  deviceObj.setDeviceId(rdm);
		  deviceObj.setDeviceName("random");
		  deviceObj.setPriority(5);
		  deviceObj.setType("Light");

		  Order action=new Order();
		 
		  action.setWhat(deviceObj);
		//  action.setPerformative(ACLMessage.REQUEST);
		  action.setType(OrderType.DIRECT);
	 	//  action.setHW_dependend_command("something");
	 	  action.setService(ServicesNamespace.ButtonControl);
	 		 
	 	  String asa =this.findPermanentlyINAgentStore("NaNaNa");
	 	  
	 	  if(asa != null || asa!="") System.out.println(asa);
	 	  
	   return action;		
	}

	
	
	
	@Override
	public String[] setService2Search() {
		String[] services={ServicesNamespace.ButtonControl};
		return services;
	}

	@Override
	public String[] setServiceOffered() {
		return null;
	}

}
