package launcher;

/** JADE CONTAINER BOOT BASED ON STRING COMMANDS*/
/*commands
 * -nothing (start a Main container)
 * -container
 * -container2
 * -container3
 * -host [IP address]
 * -backup
 */

public class Main {

	
	/* DEFINE for "SERVER" REMOTE CONTAINER	*/
	public static String HostipAddress="10.41.4.2";
	public static String ReplicaIPaddress="192.168.0.12";
	
	
	public static void main(String[] args) {
		
		String[] param = null;
		
		param= new String[7];
		System.out.println("----------- / Starting as a MAIN CONTAINER / --------------");
		
		param[0]="-gui";
		param[1]="-name";
		param[2]="DomoDAI";
		param[3]="-services";
		/**Start Replication [Fault tolerant platform]*/
		param[4]="jade.core.replication.MainReplicationService;jade.core.replication.AddressNotificationService";
		/**Start Topic,Notification and Mobility*/
		//param[2]= "jade.core.messaging.TopicManagementService;jade.core.event.NotificationService;jade.core.mobility.AgentMobilityService";
		param[5]="-agents";
		 param[6]="_Scene_Act_:fueraFrm.RESTSceneActuator;"+
		    		"__VoiceMediator__:fueraFrm.VoiceMediator;"+
		    		"_REST_ACT_:fueraFrm.RESTActuator"+
		    		"__Logger__:fueraFrm.LogActuator;"+
		    		"_Google_TTS_:fueraFrm.TTSActuator;"+
		    		"__SNS__:fueraFrm.StringherSensor;_ACT_:fueraFrm.StringherActuator;_MNG_:fueraFrm.StringerMediator;";
		/*
		param[6]="__SNS__:fueraFrm.StringherSensor;_ACT_:fueraFrm.StringherActuator;_MNG_:fueraFrm.StringerMediator;"+
				"__Logger__:fueraFrm.LogActuator;";
		/*param[6]="__Logger__:fueraFrm.LogActuator;"+
				"_Scene_Act_:fueraFrm.RESTSceneActuator;"+
				"_ASR_Sens_:fueraFrm.AisoyVoiceSensor;";
		//		"_TTS_Act_:fueraFrm.AisoyVoiceActuator;";
				//+ "__EnergyManager__:fueraFrm.EnergyMediator;"+
		//			+"__VoiceMediator__:fueraFrm.VoiceMediator";
		
		//param[6]="_REST_dev_:fueraFrm.RESTSensorAgent";*/

		
		
			for(int i=0;i< args.length;i++)
			{
				String cmd = args[i];
				
				if(cmd.equalsIgnoreCase("-BACKUP")) //BACKUP host
				{
					System.out.println("----------- / Starting as a BACKUP / --------------");
					param= new String[11];
					param[0]="-backupmain"; 
					param[1]="-local-port";
					param[2]="1234";
					param[3]="-host";
					param[4]= ReplicaIPaddress;
					param[5]="-port";
					param[6]="1099";
					param[7]="-name";
					param[8]="DomoDAI";
					param[9]="-services";
					param[10]="jade.core.replication.MainReplicationService;jade.core.replication.AddressNotificationService;";
				}
				else if(cmd.equalsIgnoreCase("-CONTAINER"))
				{
					System.out.println("----------- / Starting as a CONTAINER / --------------");
					param= new String[10];
					param[0]="-container";
					param[1]="-host";
					param[2]= HostipAddress;
					param[3]="-port";
					param[4]="1099";
					param[5]="-name";
					param[6]="DomoDAI";
					param[7]="-services jade.core.replication.AddressNotificationService;";
					param[8]="-agents";
					param[9]=//"__REST_LiGHT_act__:fueraFrm.RESTLightActuator;"+
							 "__RASP_sensor_agent__:fueraFrm.RaspberryButtonSensor;"+
							"__EnergyManager__:fueraFrm.EnergyMediator;"+
							 "__RASP_LED_agent__:fueraFrm.RaspberryLEDActuator;";
				}
				
				else if(cmd.equalsIgnoreCase("-CONTAINER2"))
				{
					System.out.println("----------- / Starting as a CONTAINER / --------------");
					param= new String[10];
					param[0]="-container";
					param[1]="-host";
					param[2]= HostipAddress;
					param[3]="-port";
					param[4]="1099";
					param[5]="-name";
					param[6]="DomoDAI";
					param[7]="-services jade.core.replication.AddressNotificationService;";
					param[8]="-agents";
				    param[9]="_Presence_SNS_:hybrid.PresenceSensor;"+
					 "_Presence_MED_:hybrid.PresenceMediator;"+
							  "_Presence_ACT_:hybrid.PresenceLEDActuator;";
								
				}
				else if(cmd.equalsIgnoreCase("-CONTAINER3"))
				{
					System.out.println("----------- / Starting as a CONTAINER / --------------");
					param= new String[10];
					param[0]="-container";
					param[1]="-host";
					param[2]= HostipAddress;
					param[3]="-port";
					param[4]="1099";
					param[5]="-name";
					param[6]="DomoDAI";
					param[7]="-services jade.core.replication.AddressNotificationService;";
					param[8]="-agents";
				    param[9]="_Scene_Act_:fueraFrm.RESTSceneActuator;"+
				    		"__VoiceMediator__:fueraFrm.VoiceMediator"+
				    		"_REST_ACT_:fueraFrm.RESTActuator";
				}
				else if(cmd.equalsIgnoreCase("-HOST"))
				{
					if(args[i+1] != null)
					{
						//take the IP address and use it as the new host
						HostipAddress=args[i+1];
						//find the Hostipaddress in param and change it
						for(int j=0;j<param.length;j++)
						{
							if(param[j].equalsIgnoreCase("-host")){ 
								param[j+1]=args[i+1];break;
								}
						}
					}
				}
			}

		
		if(param != null)
	 	//start jade with above parameters
	 	jade.Boot.main(param);
		
	}
	
}