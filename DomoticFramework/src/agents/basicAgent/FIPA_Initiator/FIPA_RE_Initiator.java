package agents.basicAgent.FIPA_Initiator;

import ontology.predicates.FIPADecison;
import agents.basicAgent.BasicAgent;
import jade.content.ContentElement;
import jade.content.Predicate;
import jade.content.lang.Codec.CodecException;
import jade.content.onto.OntologyException;
import jade.lang.acl.ACLMessage;
import jade.proto.AchieveREInitiator;


/*Is need for the FIPA Archive-RE protocol, the initiator is used to to handle the Inform and Refuse message from the Responder Agent
This class is unique for all the agent that partecipate to the Archive-RE protocol.
*/

@SuppressWarnings("serial")
public class FIPA_RE_Initiator extends AchieveREInitiator {

	BasicAgent a;
	public FIPA_RE_Initiator(BasicAgent a, ACLMessage msg) {
		super(a, msg);
		this.a=a;
	}
	

	@Override
	protected void handleInform(ACLMessage inform) {
		super.handleInform(inform);
		System.out.println("** "+myAgent.getLocalName()+" INITIATOR **: ho ricevuto ' " + inform.getContent() +"' da " +inform.getSender().getLocalName());
		
		
		ContentElement ce = null;
		
		try {
			ce = myAgent.getContentManager().extractContent(inform);
		} catch ( CodecException | OntologyException e) {
			e.printStackTrace();
		}
		
		if(ce != null && ce instanceof FIPADecison)	a.handleFIPAresponse((FIPADecison) ce);
		else System.err.println("Cannot send the datas recived in the Inform of FIPA-ArchiveRE to the agent. Predicate is null!");
		a.removeBehaviour(this);
	}
	
	
	
	@Override
	protected void handleRefuse(ACLMessage msg) {
		System.out.println(this.myAgent.getLocalName()+": � stata rifiutata la mia richiesta da" + msg.getSender().getLocalName());		
		
		
		ContentElement ce = null;
		
		try {
			ce = myAgent.getContentManager().extractContent(msg);
		} catch ( CodecException | OntologyException e) {
			e.printStackTrace();
		}
		
		if(ce != null && ce instanceof FIPADecison)	a.handleFIPAresponse((FIPADecison) ce);
		else System.err.println("Cannot send the datas recived in the Refuse of FIPA-ArchiveRE to the agent. Predicate is null!");
		
		a.removeBehaviour(this);
	}
}


