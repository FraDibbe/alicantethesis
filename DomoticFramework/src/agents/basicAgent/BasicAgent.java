package agents.basicAgent;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Vector;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

import ontology.DomoticOntology;
import ontology.predicates.FIPADecison;
import agents.basicAgent.baseBehaviour.SearchServices2DF;
import agents.typos.MediatorAgent;
import jade.content.Concept;
import jade.content.lang.Codec;
import jade.content.lang.Codec.CodecException;
import jade.content.lang.sl.SLCodec;
import jade.content.onto.Ontology;
import jade.content.onto.OntologyException;
import jade.content.onto.basic.Action;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.Behaviour;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;


/**
 * <b>BasicAgent</b> � esteso da tutte le altre classi Agente.
 * Fornisce i Behaviours base per ogni agente che sono fondamentalmente le operazioni base per registrare e cercare servizi presso un DF
 * e per rimuovere dal DF tali servizi dopo la "morte" dell'agente.
 * <ul>
 * <>
 * <>
 *</ul>
 * Quando un agente estende il basic agent è forzato ad implementare i metodi abstract:
 *	setService2Search();
 *	setServiceOffered();
 *	setResponderBehaviour();
 * e quindi a partecipare correttamente al protocollo di comunicazione Servizi Offerti/ Servizi Ricercati qui adottato 
 *
 */

public abstract class BasicAgent extends Agent {

	private static final long serialVersionUID = -7347012568624911389L;
	
	/*Database connection constants*/
	
	// JDBC driver name and database URL
     private static final String JDBC_DRIVER = "org.h2.Driver"; //org.h2.Driver
     private String DB_URL = "jdbc:h2:~/"+getLocalName();
     
     //  Database credentials
     private String USER = getLocalName();
     static final String PASS = ""; 
     
     //Database connection
     Connection conn = null;

	/*Insieme di service providers, è una MAP servizio <-> AID */
	private Map<String,List<AID>> serviceMediators = new HashMap<String,List<AID>>();
	private Map<String,List<AID>> serviceActuators = new HashMap<String,List<AID>>();
	
	/*Se ci sono argomenti passati all'agente, essi vengono salvati in questo vettore*/
	private Vector<String> stringhedArgs=new Vector<String>();
	
	private String[] service2Search;
	private String[] serviceOffered;
	
	/*a MAP that acts as common memory between agents and behaviours*/
	private Map<String,Object> AgentStore= new HashMap<String,Object>(); 

	/*ontology definition*/
	public Codec codec = new SLCodec();
	public Ontology ontology = DomoticOntology.getInstance();
	
	private Behaviour serviceSearcher;
	
	
	public BlockingQueue<ACLMessage> queue = new ArrayBlockingQueue<ACLMessage>(100);

	
	@Override
	protected void setup()
	{
		//register ontology
		getContentManager().registerLanguage(codec);
		getContentManager().registerOntology(ontology);
		
		//register service names
		service2Search=setService2Search();
		serviceOffered=setServiceOffered();
		if(serviceOffered != null)  registerService2DF();

		//Take the arguments for the agent, if any.
		Object[] args=getArguments();
		if(args != null){
			stringhedArgs=new Vector<String>(args.length);
			for(int k=0;k<args.length;k++) stringhedArgs.add(k, (String)args[k]);
		}

		serviceSearcher = new SearchServices2DF(this, 0,service2Search, serviceMediators,serviceActuators);
		
		this.addBehaviour(serviceSearcher);

		create_or_open_db();
		
	//	cleanPermanentAgentStore(true);
		
		init(); //here every agent put it's setup code!->Template pattern
		
		System.out.println("*****************************");
		System.out.println("******-- "+ this.getLocalName() + " partito! -- ********");
		System.out.println("*****************************");
		System.out.println("");
	}

	
	@Override
	protected void takeDown() {

		//Every agent who dies has to deregister first it's services to the DF
		if(serviceOffered != null){
			DFAgentDescription dfd = new DFAgentDescription();
			dfd.setName(this.getAID());	

			//crea un ServiceDescription per ogni servizio presente 'serviceOffered'

			for (int i=0; i< serviceOffered.length;i++){

				ServiceDescription sd = new ServiceDescription();
				
				String type;
				if(this instanceof MediatorAgent)type="Mediator";
				else type="Actuator";
				
				sd.setType(type);
				sd.setName(serviceOffered[i]);
				dfd.addServices(sd);
			}
			try {
				DFService.deregister(this, this.getDefaultDF(), dfd);
				System.out.println("** "+ this.getLocalName() +" ha deregistrato i sui servizi");
				//TODO: cercare gli agenti che usano i servizi forniti da questo agente e dirgli di eliminare
				//tali servizi dalla sua MAP <String,AID>
			} catch (FIPAException e) {
				System.out.println("Impossibile deregistrare i servizi di: "+ this.getLocalName());
			}		
		}	
	}


	public boolean isUsingService(String service)
	{
		for(String serv: service2Search) 
		{
			if(serv.equalsIgnoreCase(service)) return true;
		}
		return false;	
	}


	
						///////////////////////////////////////////
						///// Service Provider  methods /////	
						//////////////////////////////////////////
	public String[] getServicesOffered()
	{
		return this.serviceOffered;
	}
	
	public List<AID> getAllMediators(String service){
		return serviceMediators.get(service);
	}
	
	public List<AID> getAllActuators(String service){
		return serviceActuators.get(service);
	}
	
	public void addAService2Search(String ActuatorService)
	{
		String[] newSer2Sea;
		if(service2Search != null)	newSer2Sea = new String[service2Search.length+1];
		else newSer2Sea = new String[1];
		
		int i=0;

		//copy the old services
		if(service2Search != null)  for(; i< service2Search.length;i++) newSer2Sea[i] = service2Search[i];
		
		//add the new service at the end of the array
		newSer2Sea[i]=ActuatorService;
		
		//now change the global variable to the new one
		service2Search=newSer2Sea;
		
		//delete the old searcherBehavior
		this.removeBehaviour(serviceSearcher);
		
		//create a new serviceSearcher Behaviour
		serviceSearcher = new SearchServices2DF(this, 0,service2Search, serviceMediators,serviceActuators);
		
		//add the serviceSearcher behaviour agiaun with service2Search updated
		this.addBehaviour(serviceSearcher);
	}
	
	
	protected void registerService2DF(){

		DFAgentDescription dfd = new DFAgentDescription();
		//dfd.setName(this.getDefaultDF());	

		//crea un ServiceDescription per ogni servizio presente 'serviceOffered'

		for (int i=0; i< serviceOffered.length;i++){

			ServiceDescription sd = new ServiceDescription();
			
			String type;
			if(this instanceof MediatorAgent)type="Mediator";
			else type="Actuator";
			
			sd.setType(type);
			sd.setName(serviceOffered[i]);
			dfd.addServices(sd);
		}
		//registra ogni servizio presente in 'serviceOffered'
		try {
			DFService.register(this, dfd);
			System.out.println(this.getLocalName()+": I servizi ");
			@SuppressWarnings("rawtypes")
			Iterator it=dfd.getAllServices();

			while(it.hasNext()){
				ServiceDescription element = (ServiceDescription)it.next();
				System.out.println(element.getName() + " ");
			}
			System.out.println("sono stati registrati");
			System.out.println("-----------------------------------");
		}
		catch (FIPAException fe) {
			fe.printStackTrace();
		}


	}

								///////////////////////////////////////////
								///// AgentStore Map methods /////	
								//////////////////////////////////////////
public void addtoAgentStore(String Key,Object obj) { 
this.AgentStore.put(Key, obj);
}

public Object getfromAgentStore(String key) {
return this.AgentStore.get(key);
}

public boolean removefromAgentStore(String key) {
if(this.AgentStore.remove(key) != null) return true;
else return false;
}

public Set<String> getAllDatasKey()
{
	return this.AgentStore.keySet();
}

private void create_or_open_db() 
{
 
            //Register JDBC driver
            try {
				Class.forName(JDBC_DRIVER);
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}

            //Open a connection
            System.out.println("Connecting to a selected database...");
            try {
				conn = DriverManager.getConnection(DB_URL, USER, PASS);  //if DB doesn't exist, it create a new one
			} catch (SQLException e) {
				e.printStackTrace();
			}
            System.out.println("Connected database successfully...");
		
            String sql="CREATE TABLE IF NOT EXISTS "+getLocalName()+" (KEY VARCHAR(255) PRIMARY KEY, VALUE VARCHAR(255), TIME TIMESTAMP NOT NULL);";
            
            this.executeQuery(sql);       
}

public int addPermanently2AgentStore(String Key,Object obj) { 
	
	int ret=0;
	
	if(obj instanceof String ) {
	String sql="merge into "+getLocalName()+"(KEY, VALUE, TIME) values('"+Key+"', '"+(String)obj+"', NOW() );"; 
	return this.executeQuery(sql);
	}
	else
	{
		System.out.print("OBJECT TYPE SHOULD BE STILL CODED!!!!");
	}
	return ret;
}

public String findPermanentlyINAgentStore(String Key) { 
	
	String sql="select VALUE from "+getLocalName()+" where KEY='"+Key+"';";
	
	String result=null;
	
	ResultSet set=executeReturnQuery(sql);
	
	try {
		if(set.next()) result= set.getString("VALUE");
	} catch (SQLException e) {
		e.printStackTrace();
	}
	
	return result;
}

public void cleanPermanentAgentStore(boolean deleteFiles) {
	String sql="DROP ALL OBJECTS ";
	if(deleteFiles)sql+="DELETE FILES";
	executeQuery(sql);
}

public ResultSet getAllPermanetAgentStore()
{
	String sql="select * from "+getLocalName()+";";
	
	ResultSet set=executeReturnQuery(sql);
		
	return set;
}

private int executeQuery(String sql){
	
	 Statement stmt = null;
	 int ret = 0;
    
       try {
   		stmt = conn.createStatement();
   	} catch (SQLException e) {
   		e.printStackTrace();
   	}

       try {
   		ret=stmt.executeUpdate(sql);
   	} catch (SQLException e) {
   		e.printStackTrace();
   	}
	return ret;
}

private ResultSet executeReturnQuery(String sql){
	
	 Statement stmt = null;
   	 ResultSet rs=null;
   		
      try {
  		stmt = conn.createStatement();
  	} catch (SQLException e) {
  		e.printStackTrace();
  	}

      try {
  		rs=stmt.executeQuery(sql);
  	} catch (SQLException e) {
  		e.printStackTrace();
  	}
        
	return rs;
}

						///////////////////////////////////////////
						///// Common Messaging Utility methods /////	
						//////////////////////////////////////////

	
	public ACLMessage createOntologyMessage(int performative, Concept action) 
			throws CodecException, OntologyException
	{
		ACLMessage msg = new ACLMessage(performative);
		msg.setLanguage(codec.getName());
		msg.setOntology(ontology.getName());
		
		Action agentAction = new Action();
		agentAction.setAction(action);
		agentAction.setActor(this.getAID());
		
		// Let JADE convert from Java objects to string
		getContentManager().fillContent(msg, agentAction);
		return msg;
	}
		
    
	public MessageTemplate createOntlogyMessageTemplate (int performative, String service)
	{
		MessageTemplate mt = MessageTemplate.and(
			    MessageTemplate.MatchLanguage(codec.getName()),
			    MessageTemplate.MatchOntology(ontology.getName()));
		if (performative > -2 && performative < 21) //limites tipos mensajes ACL
			mt = MessageTemplate.and(mt, 
					MessageTemplate.MatchPerformative(performative));
		if(service != null)
			mt = MessageTemplate.MatchConversationId(service);
			
		return mt;
	}
	

	//TODO: remove
	public ACLMessage respondFIPA_RE(ACLMessage request, ACLMessage response, String replyContent)
	{
		ACLMessage reply = request.createReply();
		reply.setPerformative(ACLMessage.INFORM);
				
		reply.setContent(replyContent);
		return reply;
	}
		
	
	public void handleFIPAresponse(FIPADecison predicate){}
	 

	
							///////////////////////////////////////////
							///// Abstract Configuration methods /////	
							//////////////////////////////////////////
	
	protected abstract void init();
	public abstract String[] setService2Search();
	public abstract String[] setServiceOffered();
}


