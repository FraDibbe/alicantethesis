package agents.basicAgent.baseBehaviour;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.WakerBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;

/*This class register services at an application agent. It is developed like a waker behavior, but every 2 minutes it restarts. 
* So an agent can discovery, and register, new services -and new agents- while the application is running   */
@SuppressWarnings("serial")
public class SearchServices2DF extends WakerBehaviour {

	private String[] service2Search;
	private Map<String,List<AID>> servicesMediator = new HashMap<String,List<AID>>();
	private Map<String,List<AID>> servicesActuator = new HashMap<String,List<AID>>();
	
	public SearchServices2DF(Agent a,long timeout, String[] service2Search,Map<String,List<AID>> servicesMed, Map<String,List<AID>> servicesActu) {
		super(a, timeout);
		this.service2Search=service2Search;
		this.servicesMediator = servicesMed;
		this.servicesActuator=servicesActu;
	}
	
	
	
	public ArrayList<Map<String, List<AID>>>  getResult()
	{
		ArrayList<Map<String, List<AID>>> lol= new ArrayList<Map<String, List<AID>>>();
		lol.add(servicesMediator);
		lol.add(servicesMediator);
		return lol;
				
	}
	
	
	@Override
	public void onWake() {
		
		//System.out.println(" ** -> Waker di ricerca servizi per l'agente " + myAgent.getLocalName() +"  attivato");

		if(service2Search != null)
		{
		//ricerco nel DF ogni servizio impostato in 'service2Search'
			for (int i=0; i< service2Search.length;i++){
				DFAgentDescription dfd = new DFAgentDescription();
				
				ServiceDescription sdreq = new ServiceDescription();
				sdreq.setName(service2Search[i]);
				dfd.addServices(sdreq);
				//ricerca nel DF un ServiceDescription uguale a 'sdreq'

				try {
					DFAgentDescription[] res = DFService.search(myAgent, dfd);

					if(res != null) {
						String message=myAgent.getLocalName();
						
							for(int k=0; k< res.length;k++){ 
									//aggiungo il servizio se e sono se esso non � gi� presente 
								
								
								//****************Mediator case
									if(!servicesMediator.containsKey(service2Search[i])){ //new list
										
										if(res[k].getAllServices().hasNext()){
											
											ServiceDescription sd=(ServiceDescription)res[k].getAllServices().next();
											
											if( sd.getType().equalsIgnoreCase("Mediator") )	{
														List<AID> l = new ArrayList<AID>();
														l.add(res[k].getName());
														servicesMediator.put(service2Search[i], l);
														message+=": Aggiunto servizio  AS MEDIATOR" + service2Search[i] + " fornito da "+ res[k].getName();
														System.out.println(message);
													}
											}
										
										}						
									else //update list
										{

										   if(res[k].getAllServices().hasNext()){
											   ServiceDescription sd=(ServiceDescription)res[k].getAllServices().next();
												
												if( sd.getType().equalsIgnoreCase("Mediator") )	{
													List<AID> l=servicesMediator.get(service2Search[i]);
													if(!l.contains(res[k].getName() ) )
													{
														l.add(res[k].getName());
														servicesMediator.replace(service2Search[i], l);
														message+=": Replaced in LIST servizio AS MEDIATOR" + service2Search[i] + " fornito da "+ res[k].getName();
														System.out.println(message);
													}
												}
										   }
										}
									
						//*************Actuator Case
								if(!servicesActuator.containsKey(service2Search[i])){ //new list
										
										if(res[k].getAllServices().hasNext()){
											
											ServiceDescription sd=(ServiceDescription)res[k].getAllServices().next();
											
											if( sd.getType().equalsIgnoreCase("Actuator") )	{
														List<AID> l = new ArrayList<AID>();
														l.add(res[k].getName());
														servicesActuator.put(service2Search[i], l);
														message+=": Aggiunto servizio AS ACTUATOR " + service2Search[i] + " fornito da "+ res[k].getName();
														System.out.println(message);
													}
											}
										
										}						
									else //update list
										{

										   if(res[k].getAllServices().hasNext()){
											   ServiceDescription sd=(ServiceDescription)res[k].getAllServices().next();
												
												if( sd.getType().equalsIgnoreCase("Actuator") )	{
													List<AID> l=servicesActuator.get(service2Search[i]);
													if(!l.contains(res[k].getName() ) )
													{
														l.add(res[k].getName());
														servicesActuator.replace(service2Search[i], l);
														message+=": Replaced in LIST servizio AS ACTUATOR" + service2Search[i] + " fornito da "+ res[k].getName();
														System.out.println(message);
													}
												}
										   }
										}
									
									}
								}	
				} catch (FIPAException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
								
				reset(15000); //1 min sleep				
	}
	
}