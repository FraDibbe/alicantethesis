package agents.basicAgent.baseBehaviour;

public class Decision {

	private boolean decisionExitus=false;
	private String[] stringValueOfDecision;

	
	public String[] getStringValueOfDecision() {
		return stringValueOfDecision;
	}
	public void setStringValueOfDecision(String[] stringValueOfDecision) {
		this.stringValueOfDecision = stringValueOfDecision;
	}
	
	public boolean getDecisionExitus() {
		return decisionExitus;
	}
	public void setDecisionExitus(boolean decisionValue) {
		this.decisionExitus = decisionValue;
	}
	
}
