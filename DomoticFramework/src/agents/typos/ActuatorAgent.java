package agents.typos;


import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

import java.util.concurrent.ConcurrentLinkedQueue;


import agents.basicAgent.baseBehaviour.Decision;
//import agents.typos.actuatorBehaviours.ActuatorLogicBeh;
import agents.typos.actuatorBehaviours.Actuator_ARE_Responder;
import ontology.concepts.Actuation;


/**
 * @author Fra
 *
 * Un Attuatore registra un responder per il tipo di servizio fisico a cui da accesso 
 *  (ovviamente altri possono essere inseriti nel main, ma non seguono la logica del decide() )
 *
 * Ha un setUpActuator dove imposto la creazione del client/impostazione HW
 * 
 * e ha un'azione actuate che attua sull'attuatore
 *
 */

public abstract class ActuatorAgent extends ActuatorBaseAgent {

	private static final long serialVersionUID = -6629886365600815196L;

	//private ConcurrentLinkedQueue<Actuation> actuations = new ConcurrentLinkedQueue<Actuation>();


	@Override
	protected void init()
	{
		main();
		actuatorSetup();
		addResponders();
	//	decide(); TODO, e se lo rimettessi?
	}
		
		///////////////////////////////////////////
		///// Logical Functions methods /////********---> where implementations are influenced by the sensor the agent possess */
		//////////////////////////////////////////
		
	/** put here the behavior dealing with the main Service representing the HW device*/
	public void addResponders()
	{
		String[] agentServices = this.getServicesOffered();
	
		MessageTemplate mt1 = MessageTemplate.and(MessageTemplate.MatchLanguage(this.codec.getName()), MessageTemplate.MatchOntology(this.ontology.getName()));
		MessageTemplate mt2=MessageTemplate.MatchPerformative(ACLMessage.REQUEST);
		MessageTemplate mt1and2 = MessageTemplate.and(mt1, mt2);
			
	   if(agentServices != null)
		{
			//for each service create a responder and a template that matches each Service Name
			for(String serv:agentServices)
			{
				MessageTemplate mt= MessageTemplate.MatchConversationId(serv);
				MessageTemplate mtCustom = MessageTemplate.and(mt1and2, mt);
				addBehaviour(new Actuator_ARE_Responder(this,mtCustom));
			}
		}
		
	};

		
		/** needed to sample the input */
		public abstract  Decision actuate(Actuation obj);
	
			
			//////////////////////
			///// THE LOGIC /////	
			/////////////////////
		
		/** a simple LOGIC that every sensor agent should follow */
		@Override
		protected  void decide()
		{
	//		addBehaviour(new ActuatorLogicBeh(this, getLogicSpeed())); //take a decision every X sec
		}

		
		///////////////////////////////////
		///// getter and setters /////	
		///////////////////////////////////
/*		
		public ConcurrentLinkedQueue<Actuation> getActuationList(){
			return this.actuations;
		}
*/			
	
}

