package agents.typos;


import jade.core.behaviours.TickerBehaviour;
import ontology.concepts.Convertor;
import agents.typos.sensorBehaviours.SensorPollLogicBeh;

public abstract class SensorPollAgent extends SensorBaseAgent{
	private static final long serialVersionUID = 8190274050015097250L;
	

	@SuppressWarnings("serial")
	@Override
	protected void init()
	{
		main();
		sensorSetup();
		addBehaviour(new TickerBehaviour(this, getLogicSpeed()) {
			
			@Override
			protected void onTick() {
				SensorPollAgent agent=(SensorPollAgent)myAgent;
				Convertor cv=percept();
				if(cv != null)	agent.getToConvertList().add(cv);
				decide();
			}
		});
	}
	

	///////////////////////////////////////////
	///// Logical Functions methods /////********---> where implementations are influenced by the sensor the agent possess */
	//////////////////////////////////////////

	/** needed to sample the input */
	public abstract Convertor percept();
	

		//////////////////////
		///// THE LOGIC /////	
		/////////////////////
	
	/** a simple LOGIC that every sensor agent should follow */
	@Override
	protected  void decide()
	{
		addBehaviour(new SensorPollLogicBeh(this, getLogicSpeed())); //take a decision every 1sec
	}


	
}
