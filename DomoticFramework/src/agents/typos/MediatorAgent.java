package agents.typos;

import jade.core.behaviours.Behaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

import java.util.LinkedList;

import ontology.actions.Order;
import agents.basicAgent.baseBehaviour.Decision;
//import agents.typos.mediatorBehaviours.LogicManager;
import agents.typos.mediatorBehaviours.Mediator_ARE_Responder;

/**
 *  @author Fra
 * Un Mediator u� registare diversi servizi, per questo ha + responder 
 * (spostato Behaviour[] Responder_RE_Behaviours dal basic agent).
 * 
 * @param <T>
 */

public abstract class MediatorAgent extends ReactiveAgent {

	private static final long serialVersionUID = -6557821426694781362L;
	
	//private LinkedList<Order> toBeProcessed= new LinkedList<Order>();
	
	private Behaviour manageBehaviour;
   
	@Override
	protected void init()
	{
		main();
	//	manageBehaviour=new LogicManager(this, getLogicSpeed());
	//	addBehaviour(manageBehaviour);
		addResponders();
	}


	/** put here the behavior dealing with the main Service representing the HW device*/
	public void addResponders()
	{
		String[] agentServices = this.getServicesOffered();
	
		MessageTemplate mt1 = MessageTemplate.and(MessageTemplate.MatchLanguage(this.codec.getName()), MessageTemplate.MatchOntology(this.ontology.getName()));
		MessageTemplate mt2=MessageTemplate.MatchPerformative(ACLMessage.REQUEST);
		MessageTemplate mt1and2 = MessageTemplate.and(mt1, mt2);
			
	   if(agentServices != null)
		{
			//for each service create a responder and a template that matches each Service Name
			for(String serv:agentServices)
			{
				MessageTemplate mt= MessageTemplate.MatchConversationId(serv);
				MessageTemplate mtCustom = MessageTemplate.and(mt1and2, mt);
				addBehaviour(new Mediator_ARE_Responder(this,mtCustom));
			}
		}
		
	};
	

	/** this is the main of the agent class: put here behaviour and definitions*/
	protected abstract void main();
	
	/** here one should define the algorithm to process the action element 
	 * @return */
	public abstract Decision decide(Order order);

	
	/*
	public LinkedList<Order> getToBeProcessed() {
		return toBeProcessed;
	}


	public void setToBeProcessed(LinkedList<Order> toBeProcessed) {
		this.toBeProcessed = toBeProcessed;
	}
	*/
}
