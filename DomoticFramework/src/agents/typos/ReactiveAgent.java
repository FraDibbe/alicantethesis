package agents.typos;

import java.io.File;
import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

import ontology.actions.Do;
import ontology.actions.Order;
import ontology.concepts.StringMessage;
import utils.json.XML_SAX_parser;
import jade.content.Concept;
import jade.content.lang.Codec.CodecException;
import jade.content.onto.OntologyException;
import jade.core.AID;
import jade.lang.acl.ACLMessage;
import agents.basicAgent.BasicAgent;
import agents.basicAgent.FIPA_Initiator.FIPA_RE_Initiator;


public abstract class ReactiveAgent extends BasicAgent{
	
	private static final long serialVersionUID = -1312484509671107928L;

	   private long logicSpeed=200;
	   private String path="../../../deviceXML/"; //Standard path
	   private boolean SAXinitialized=false;
	   
	   //parser objects
	   /* Instantiate the SAX Parser */	 
		 SAXParserFactory spf = SAXParserFactory.newInstance();
		 SAXParser saxParser = null;
		 XMLReader xmlReader = null;
		 
	   //logger service
	   private String loggerServiceName="_:NOT SETTED:_";
	
	   
///////////////////////////////////////////////////////////////////////
///// Reactive agents should know information abaout the devices they uses /////
//////////////////////////////////////////////////////////////////////
		/* --->  the agent should look in a read only XML file where all the data about physical devices are store */
	
	public Concept findDevice(String deviceName, XML_SAX_parser handler)
	{
		 String filename=getPath()+deviceName+".txt";
		 	
		 if(!SAXinitialized) {
			 /*initialize the parser*/
			 
			//set namespace aware 
		  spf.setNamespaceAware(true);
		 
		  //create the SAXparser
			try {
				saxParser = spf.newSAXParser();
			} catch (ParserConfigurationException | SAXException e1) {
				e1.printStackTrace();
			}
		
		 //get the XML reader
		try {
			xmlReader = saxParser.getXMLReader();
		} catch (SAXException e1) {
			e1.printStackTrace();
		}
		
		SAXinitialized=true;
		 }
		 
		/* Set the handler */
		xmlReader.setContentHandler(handler);
		
		/* Parse the xml-data from our stream */
		 try {
			xmlReader.parse(convertToFileURL(filename));
		} catch (IOException | SAXException e) {
			e.printStackTrace();
		}
		 
		 /* Return the concept back */
		return  handler.getParsedData();	
	}

	
///////////////////////////////////////////////////////////////////////
///// Common Functions methods of a Reactive agent /////
//////////////////////////////////////////////////////////////////////
	/* --->  are used in the logic part of the agent */
		
		
	/* needed to send the value read to mediator/s */
	public void sendToMediators(Order actionAndData, boolean useProtocol) throws Exception
	{
		if(actionAndData.getService() == null || actionAndData.getService()=="") throw new Exception("NO SERVICE DEFINED FOR THIS OBJECT TO BE SEND!") ;
	//	if(!(actionAndData.getPerformative() > -2 && actionAndData.getPerformative() < 21) ) throw new Exception("NO PERFORMATIVE DEFINED FOR THIS OBJECT TO BE SEND!") ;
		if(actionAndData.getType() == null) throw new Exception("NO TYPE DEFINED FOR THIS OBJECT TO BE SEND!") ;
		
		ACLMessage msg = null;
		try {
			msg = createOntologyMessage(actionAndData.getPerformative(),actionAndData);
		} catch (CodecException | OntologyException e) {
					e.printStackTrace();
		}
		
	if( this.getAllMediators(actionAndData.getService()) != null  )
	{
			msg.setConversationId(actionAndData.getService());
			for(int i=0;  i< this.getAllMediators(actionAndData.getService()).size();i++ ){ 
				AID provediver_i = this.getAllMediators(actionAndData.getService()).get(i);
				if(provediver_i != null) msg.addReceiver(provediver_i);
				}
			
			if(useProtocol) this.addBehaviour(new FIPA_RE_Initiator(this, msg));
			else send(msg);
	}

 }
	
	
	/* needed to send the value read to agent/s actuator*/
	public void sendToActuators(Do actionAndData, boolean useProtocol) throws Exception
	{
		if(actionAndData.getService() == null || actionAndData.getService()=="") throw new Exception("NO SERVICE DEFINED FOR THIS OBJECT TO BE SEND!") ;
	//	if(!(actionAndData.getPerformative() > -2 && actionAndData.getPerformative() < 21) ) throw new Exception("NO PERFORMATIVE DEFINED FOR THIS OBJECT TO BE SEND!") ;
		
		ACLMessage msg = null;
		try {
			msg = createOntologyMessage(actionAndData.getPerformative(),actionAndData);
		} catch (CodecException | OntologyException e) {
					e.printStackTrace();
		}

		
	if( this.getAllActuators(actionAndData.getService()) != null )
	{
			msg.setConversationId(actionAndData.getService());
			for(int i=0;  i< this.getAllActuators(actionAndData.getService()).size();i++ ){ 
				AID provediver_i = this.getAllActuators(actionAndData.getService()).get(i);
				if(provediver_i != null) msg.addReceiver(provediver_i);
				}
			if(useProtocol)	this.addBehaviour(new FIPA_RE_Initiator(this, msg));
			else send(msg);
	}		

}
	
	public void sendToLoggers(StringMessage logMsg) throws Exception
	{
		
		if (this.getLogService()== "_:NOT SETTED:_") throw new Exception("NO LOG SERVICE DEFINED for this Agent. Use setLogService() first!");
			
		logMsg.setSender(this.getLocalName());
		
		Do action=new Do();
	 		
 		action.setWhat(logMsg);
 		action.setService(loggerServiceName);
 		action.setPerformative(ACLMessage.INFORM);
 		
 		sendToActuators(action,false);
	}
	
	
	//////////////////////////////
	//// Getters and setters ////
	////////////////////////////
	
	
	public void setLogService(String serviceName)
	{
		this.loggerServiceName = serviceName;
		this.addAService2Search(serviceName);
	}

	public String getLogService()
	{
		return loggerServiceName;
	}
		
	public long getLogicSpeed() {
		return logicSpeed;
	}

	public void setLogicSpeed(long logicSpeed) {
		this.logicSpeed = logicSpeed;
	}
	
	
	private String convertToFileURL(String filename) {
		String path = new File(filename).getAbsolutePath();
        if (File.separatorChar != '/') {
            path = path.replace(File.separatorChar, '/');
        }
    	
        if (!path.startsWith("/") && !(String.valueOf(path.charAt(0)).matches("[A-Z]{1}"))) {
        	;
            path = "/" + path;
        }
        return path;
    }

	public void setPath(String newPath)
	{
		this.path=newPath;
	}
	
	public String getPath()
	{
		return this.path;
	}
	
}
