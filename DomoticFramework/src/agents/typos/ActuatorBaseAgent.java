package agents.typos;


public abstract class ActuatorBaseAgent extends ReactiveAgent{
	
	private static final long serialVersionUID = -1312484509671107928L;
 
  
	/** this is the main of the agent class: put here behaviour and definitions*/
	protected abstract void main();

	/** some sensors needs a setup phase: example to register a listener: so put the definition here!*/
	protected abstract void actuatorSetup();
	
		
	//////////////////////
	///// THE LOGIC /////	
	/////////////////////
	/* --->  each kind of agent uses a different one */
	protected abstract void decide();
	
}
