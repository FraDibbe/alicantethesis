package agents.typos;


import java.util.concurrent.ConcurrentLinkedQueue;

import ontology.concepts.Convertor;
import ontology.concepts.Perception;
import agents.typos.sensorBehaviours.SensorLogicOneShBeh;

public abstract class SensorAgent extends SensorBaseAgent{
	private static final long serialVersionUID = 8190274050015097250L;
	
   private ConcurrentLinkedQueue<Perception> perceptions = new ConcurrentLinkedQueue<Perception>();
  
	@Override
	protected void init()
	{
		main();
		sensorSetup();
		//decide(); TODO: changed 
	}
	

	///////////////////////////////////////////
	///// Logical Functions methods /////********---> where implementations are influenced by the sensor the agent possess */
	//////////////////////////////////////////
	
	/** needed to sample the input */
	public abstract Convertor elaboratePerception(Perception obj);
	
		
		//////////////////////
		///// THE LOGIC /////	
		/////////////////////
	
	/** a simple LOGIC that every sensor agent should follow */
	@Override
	protected  void decide()
	{
		//addBehaviour(new SensorLogicBeh(this, getLogicSpeed())); //take a decision every 1sec
		addBehaviour(new SensorLogicOneShBeh(this));
	}

	
	
	///////////////////////////////////
	///// getter and setters /////	
	///////////////////////////////////
	
	public ConcurrentLinkedQueue<Perception> getPerceptioList(){
		return this.perceptions;
	}
		

	public void add_RAW_Perception(Perception perception_Raw)
	{
		perceptions.add(perception_Raw);
		this.decide();
	}
}
