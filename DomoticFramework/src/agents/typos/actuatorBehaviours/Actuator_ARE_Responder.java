package agents.typos.actuatorBehaviours;


import ontology.actions.Do;
import ontology.concepts.Actuation;
import ontology.predicates.Acepted;
import ontology.predicates.Refused;
import agents.basicAgent.baseBehaviour.Decision;
import agents.typos.ActuatorAgent;
import jade.content.ContentElement;
import jade.content.Predicate;
import jade.content.lang.Codec.CodecException;
import jade.content.onto.OntologyException;
import jade.content.onto.basic.Action;
import jade.domain.FIPAAgentManagement.NotUnderstoodException;
import jade.domain.FIPAAgentManagement.RefuseException;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.proto.SimpleAchieveREResponder;


public class Actuator_ARE_Responder extends SimpleAchieveREResponder {

	private static final long serialVersionUID = 3153080563791016062L;
	private ActuatorAgent myAgent;
	private Decision actuationResult;

	public Actuator_ARE_Responder(ActuatorAgent a, MessageTemplate mt) {
		super(a, mt);
		this.myAgent=a;
	}

	@Override
	protected ACLMessage prepareResponse(ACLMessage request)
			throws NotUnderstoodException, RefuseException {
		return null;
	}
	
	
	/** Here the ration effect need to be achieved */
	protected ACLMessage prepareResultNotification(ACLMessage request, ACLMessage response)
	{
			//extract information
			ContentElement ce = null;
			Do action=null;
			
			try {
				ce = myAgent.getContentManager().extractContent(request);
				
				if (ce instanceof Action && ((Action) ce).getAction() instanceof Do)
				{
					action =  ((Do)((Action) ce).getAction());
				}
				
				} catch (CodecException | OntologyException e) {
					e.printStackTrace();
				}
			
			//put the action to be converted in the queue
			if(action != null) 
				{
					Actuation act = new Actuation(action);
					act.setServiceType(request.getConversationId());
				
					//call actuate
					actuationResult = myAgent.actuate(act);
			}
			
		/* ***Risponde automaticamente al protocollo FIPA con un predicato basato sul valore di actuationResult** */
		return this.respondFIPA_RE(request, response, action, "__"+request.getConversationId() );
	}
	
	
	private ACLMessage respondFIPA_RE(ACLMessage request, ACLMessage response, Do action, String replyContent)
	{
		ACLMessage reply = request.createReply();
		
		reply.setLanguage(myAgent.codec.getName());
		reply.setOntology(myAgent.ontology.getName());

		Predicate pred;
		
		
		if(	actuationResult!= null && actuationResult.getDecisionExitus() && action != null )
		{
			pred = new Acepted();
			if(actuationResult.getStringValueOfDecision() != null)
				((Acepted) pred).setWhat(actuationResult.getStringValueOfDecision());
				else{
					String[] st=new String[0];
					((Acepted) pred).setWhat(st);
				}
			((Acepted) pred).setByWhom(myAgent.getLocalName());
			((Acepted) pred).setServiceName(action.getService());
			reply.setPerformative(ACLMessage.INFORM);
		}
		else
		{
			pred = new Refused();
			/*
			 * if(actuationResult.getStringValueOfDecision() != null)
				((Refused) pred).setWhat(actuationResult.getStringValueOfDecision());
				else{
					String[] st=new String[0];
					((Refused) pred).setWhat(st);
				}
			 * */
			//((Refused) pred).setWhat(actuationResult.getStringValueOfDecision());
			((Refused) pred).setByWhom(myAgent.getLocalName());
			((Refused) pred).setServiceName(action.getService());
			reply.setPerformative(ACLMessage.REFUSE);
		}
					
	// Let JADE convert from Java objects to string
	try {
		myAgent.getContentManager().fillContent(reply, pred);
	} catch (CodecException | OntologyException e) {
		e.printStackTrace();
	}
		
	  
	return reply;
	}

}
