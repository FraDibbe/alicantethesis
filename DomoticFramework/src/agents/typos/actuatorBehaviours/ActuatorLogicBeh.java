package agents.typos.actuatorBehaviours;


import agents.typos.ActuatorAgent;
import jade.core.behaviours.TickerBehaviour;

public class ActuatorLogicBeh extends TickerBehaviour {

	private static final long serialVersionUID = -8508490423912446948L;
	
	
	ActuatorAgent myAgent;
	
	public ActuatorLogicBeh(ActuatorAgent a, long period) {
		super(a, period);
		this.myAgent=a;
	}

	@Override
	protected void onTick() {
		
		if(!myAgent.getActuationList().isEmpty())
		{
			//extract one actuation object and activate the actuate() to elaborate it  
			//based on the Actuate service Type(normally for every actuatore agent only one service should be defined)
			//but could be more of them, so actuate() need to actuate in a differt way based on the service type
			myAgent.actuate(myAgent.getActuationList().poll());
		}
		
	}

}
