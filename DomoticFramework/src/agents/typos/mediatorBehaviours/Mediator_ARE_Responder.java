package agents.typos.mediatorBehaviours;

import ontology.actions.Order;
import ontology.predicates.Acepted;
import ontology.predicates.Refused;
import agents.basicAgent.baseBehaviour.Decision;
import agents.typos.MediatorAgent;
import jade.content.ContentElement;
import jade.content.Predicate;
import jade.content.lang.Codec.CodecException;
import jade.content.onto.OntologyException;
import jade.content.onto.basic.Action;
import jade.domain.FIPAAgentManagement.NotUnderstoodException;
import jade.domain.FIPAAgentManagement.RefuseException;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.proto.SimpleAchieveREResponder;


public class Mediator_ARE_Responder extends SimpleAchieveREResponder {

	private static final long serialVersionUID = 3153080563791016062L;
	private MediatorAgent myAgent;
	private Decision decisionResult;
	
	public Mediator_ARE_Responder(MediatorAgent a, MessageTemplate mt) {
		super(a, mt);
		this.myAgent=a;
	}

	@Override
	protected ACLMessage prepareResponse(ACLMessage request)
			throws NotUnderstoodException, RefuseException {
		return null;
	}
	
	
	/** Here the ration effect need to be achieved */
	protected ACLMessage prepareResultNotification(ACLMessage request, ACLMessage response)
	{
			//extract information
			ContentElement ce = null;
			Order action=null;
			
			try {
					ce = myAgent.getContentManager().extractContent(request);
					
					if (ce instanceof Action && ((Action) ce).getAction() instanceof Order)
					{
						action =  ((Order)((Action) ce).getAction());
					}
				
				} catch (CodecException | OntologyException e) {
					e.printStackTrace();
				}
			
			/* Before....
			//put the action to be converted in the queue
			if(action != null) 
				{
					myAgent.getToBeProcessed().add(action);  
				}*/
			
			/*Now....*/
			if(action != null) 
			{
				//call actuate
				decisionResult = myAgent.decide(action);
			}
		
		/* ***Risponde automaticamente al protocollo FIPA** */
		return this.respondFIPA_RE(request, response, action, "__"+request.getConversationId() );
	}
	
	
	private ACLMessage respondFIPA_RE(ACLMessage request, ACLMessage response, Order action, String replyContent)
	{
		ACLMessage reply = request.createReply();
		
		reply.setLanguage(myAgent.codec.getName());
		reply.setOntology(myAgent.ontology.getName());
		
		
		Predicate pred;
		
		
		if(	decisionResult != null && action != null && decisionResult.getDecisionExitus() )
		{
			pred = new Acepted();
			if(decisionResult.getStringValueOfDecision() != null)
			((Acepted) pred).setWhat(decisionResult.getStringValueOfDecision());
			else{
				String[] st=new String[0];
				((Acepted) pred).setWhat(st);
			}
			((Acepted) pred).setByWhom(myAgent.getLocalName());
			((Acepted) pred).setServiceName(action.getService());
			reply.setPerformative(ACLMessage.INFORM);
		}
		else
		{
			pred = new Refused();
			if(decisionResult.getStringValueOfDecision() != null)
				((Refused) pred).setWhat(decisionResult.getStringValueOfDecision());
				else{
					String[] st=new String[0];
					((Refused) pred).setWhat(st);
				}
			((Refused) pred).setByWhom(myAgent.getLocalName());
			((Refused) pred).setServiceName(action.getService());
			reply.setPerformative(ACLMessage.REFUSE);
		}
					
	// Let JADE convert from Java objects to string
	try {
		myAgent.getContentManager().fillContent(reply, pred);
	} catch (CodecException | OntologyException e) {
		e.printStackTrace();
	}
		
	  
	return reply;
	}

}
