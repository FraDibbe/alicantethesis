package agents.typos.mediatorBehaviours;

import java.util.Collections;
import java.util.Comparator;
import ontology.actions.Order;
import agents.typos.MediatorAgent;
import jade.core.behaviours.TickerBehaviour;



@SuppressWarnings("serial")
public class LogicManager extends TickerBehaviour {

	private MediatorAgent myAgent;
	
	public LogicManager(MediatorAgent a, long period) {
		super(a, period);
		this.myAgent=a;
	}

	@Override
	protected void onTick() {
		
	//	System.out.println(myAgent.getLocalName()+ " started to apply its policy....");
		
		if(!(myAgent.getToBeProcessed().isEmpty()))
		{
			Collections.sort(myAgent.getToBeProcessed(), this.sortListAlgorithm());
			
			//extract the order: it doesn't mean that the order should be processed now, only if yes it is deleted
			Order ord=myAgent.getToBeProcessed().getFirst();
			if(myAgent.decide(ord).getDecisionExitus()) myAgent.getToBeProcessed().remove(ord); //remove the element only if decide returns true
		}
	}

	
	/* algorithm to order the queue based on priority*/
	private Comparator<? super Order> sortListAlgorithm() {
		return new Comparator<Order>() {

			@Override
			public int compare(Order dev1, Order dev2) {
				return dev1.getPriority()-dev2.getPriority();
			}
		};
	}


}
