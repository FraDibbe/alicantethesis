package agents.typos;

import java.util.concurrent.ConcurrentLinkedQueue;

import ontology.concepts.Convertor;
import jade.content.Concept;


public abstract class SensorBaseAgent extends ReactiveAgent{
	
	private static final long serialVersionUID = -1312484509671107928L;
 
	
	  private ConcurrentLinkedQueue<Convertor> toConvert = new ConcurrentLinkedQueue<Convertor>();
	  private ConcurrentLinkedQueue<Concept> actionToDeliver = new ConcurrentLinkedQueue<Concept>();


	  /** some sensors needs a setup phase: example to register a listener: so put the definition here!*/
		protected abstract void sensorSetup();
		
		/** this is the main of the agent class: put here behaviour and definitions*/
		protected abstract void main();
	  
		/* needed to convert the the Ontology in the actual output*/
		public abstract Concept convert(Convertor conv);
	  
		
	//////////////////////
	///// THE LOGIC /////	
	/////////////////////
	/* --->  each kind of agent uses a different one */
	protected abstract void decide();
	
	
	
	
///////////////////////////////////
///// getter and setters /////	
///////////////////////////////////

	
public ConcurrentLinkedQueue<Convertor> getToConvertList(){
return this.toConvert;
}

public ConcurrentLinkedQueue<Concept> getActiontToDeliver(){
return this.actionToDeliver;
}

}
