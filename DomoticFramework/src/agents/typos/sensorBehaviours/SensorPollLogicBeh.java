package agents.typos.sensorBehaviours;

import ontology.actions.Do;
import ontology.actions.Order;
import agents.typos.SensorPollAgent;
import jade.content.Concept;
import jade.core.behaviours.OneShotBehaviour;

public class SensorPollLogicBeh extends OneShotBehaviour {

	private static final long serialVersionUID = 1049204537000774803L;
	
	SensorPollAgent myAgent;
	
	public SensorPollLogicBeh(SensorPollAgent a, long period) {
		super(a);
		this.myAgent=a;
	}


	@Override
	public void action() {
		if(!myAgent.getToConvertList().isEmpty())
		{
			Concept action= myAgent.convert(myAgent.getToConvertList().poll());
			
			//transition to next step: putting a Action item in the queue
			if(action != null) myAgent.getActiontToDeliver().add(action);  
		}
		
		if(!myAgent.getActiontToDeliver().isEmpty())
		{
			//take the family of the input
			
			Concept action=myAgent.getActiontToDeliver().poll();
			try{
			//discover the action type based of istanceOf or a TypeList (?)
			if(action instanceof Do) myAgent.sendToActuators(((Do) action),true);
			else if(action instanceof Order) myAgent.sendToMediators(((Order) action),true);
			}
			catch(Exception e){
				System.out.println(e.toString());
			}
		}
		
	}

}
