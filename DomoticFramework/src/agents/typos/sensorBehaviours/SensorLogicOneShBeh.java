package agents.typos.sensorBehaviours;

import ontology.actions.Do;
import ontology.actions.Order;
import ontology.concepts.Convertor;
import agents.typos.SensorAgent;
import jade.content.Concept;
import jade.core.behaviours.OneShotBehaviour;


public class SensorLogicOneShBeh extends OneShotBehaviour  {

	private static final long serialVersionUID = 1049204537000774803L;
	
	SensorAgent myAgent;
	
	public SensorLogicOneShBeh(SensorAgent a) {
		super(a);
		this.myAgent=a;
	}

	@Override
	public void action() {
		if(!myAgent.getPerceptioList().isEmpty())
		{
			Convertor conv= myAgent.elaboratePerception(myAgent.getPerceptioList().poll());
			
			//transition to next step: putting a Action item in the queue
			if(conv != null) myAgent.getToConvertList().add(conv);  
		}
		
		if(!myAgent.getToConvertList().isEmpty())
		{
			Concept action= myAgent.convert(myAgent.getToConvertList().poll());
			
			//transition to next step: putting a Action item in the queue
			if(action != null) myAgent.getActiontToDeliver().add(action);  
		}
		
		if(!myAgent.getActiontToDeliver().isEmpty())
		{
			//take the family of the input
			
			Concept action=myAgent.getActiontToDeliver().poll();
			
			//discover the action type based of istanceOf or a TypeList (?)
			try{
			if(action instanceof Do) myAgent.sendToActuators(((Do) action),true);
			else if(action instanceof Order) myAgent.sendToMediators(((Order) action),true);
			}
			catch(Exception e){
				System.out.println(e.toString());
			}
		}
		
		
	}



}
