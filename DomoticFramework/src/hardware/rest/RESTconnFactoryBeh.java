package hardware.rest;

import java.net.URISyntaxException;

import Client.DeviceClient;
import Client.DeviceCommandClient;
import Client.LogClient;
import Client.RESTClientIf;
import jade.core.behaviours.OneShotBehaviour;

public class RESTconnFactoryBeh extends OneShotBehaviour  {

	public enum REST_Client_Types { //TODO: ADD MORE
	    LOG,DEVICES, SCENES, LIGHTS,DEV_COMMANDS
	}
	
	private static final long serialVersionUID = 6341800468421862439L;

	private RESTClientIf client;
	private String hostname;
	private REST_Client_Types typeClient;
	private String user;
	private String password;

	public RESTconnFactoryBeh(REST_Client_Types StringClientType, String hostname, String usr, String pss)
	{
		this.typeClient=StringClientType;
		this.hostname=hostname;
		this.user=usr;
		this.password=pss;
	}

	//action() as factory method to return concrete the concrete REST client
	@Override
	public void action() 
	{
		try {
			switch(this.typeClient)
			{
			case LOG:
				this.client=new LogClient(this.hostname);
			case DEVICES:
				this.client=new DeviceClient(this.hostname);
				break;
			case SCENES:
				//this.client=new SceneClient(this.hostname); TODO!
				break;
			case LIGHTS:
				//this.client=new LightClient(this.hostname); TODO!
				break;
			case DEV_COMMANDS:
				this.client=new DeviceCommandClient<String>(this.hostname); 
				break;
			default:
				System.out.println("ERROR: type" + this.typeClient.toString() + "doesn't exist! Returning null!");
				this.client=null;
				break;  
		}
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//now authorize using the client created from factory
		client.setAuth(this.user, this.password);
		
	}

	
	public RESTClientIf getResult(){ //client now is ready to use!!
		return this.client;
	}
	
}
