package hardware.raspberry;

import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioFactory;
import com.pi4j.io.gpio.GpioPinDigitalInput;
import com.pi4j.io.gpio.GpioPinDigitalOutput;
import com.pi4j.io.gpio.Pin;
import com.pi4j.io.gpio.PinPullResistance;
import com.pi4j.io.gpio.PinState;
import com.pi4j.io.gpio.event.GpioPinListener;


public class GPIO_HW implements GPIO_HW_If{

	private final static int DelayTime=100; 
	GpioController controller;

	
	public GPIO_HW()
	{
		this.controller = GpioFactory.getInstance();
	}
	
	@Override
	public GpioController getController() {
		return this.controller;
	}
	
	@Override
	public void pinLow(Pin pin) {
		GpioPinDigitalOutput pinC=controller.provisionDigitalOutputPin(pin, pin.getName(), PinState.LOW);
		pinC.low();
		commit_delay();
		controller.unprovisionPin(pinC);
	}

	@Override
	public void pinHigh(Pin pin) {
		GpioPinDigitalOutput pinC=controller.provisionDigitalOutputPin(pin, pin.getName(), PinState.LOW);
		pinC.high();
		commit_delay();
		controller.unprovisionPin(pinC);
	}

	@Override
	public void pinToggle(Pin pin) {
		GpioPinDigitalOutput pinC=controller.provisionDigitalOutputPin(pin, pin.getName(), PinState.LOW);
		pinC.toggle();
		commit_delay();
		controller.unprovisionPin(pinC);
	}

	@Override
	public void releaseGPIOcontroller(GpioController controller) {
		this.controller.shutdown();
		commit_delay();
	}

	
	private void commit_delay()
	{
		try {
			Thread.sleep(DelayTime);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void sensePin(Pin pin, GpioPinListener callback) {
		
		// provision gpio pin  as an input pin with its internal pull down resistor enabled
        GpioPinDigitalInput pinC = controller.provisionDigitalInputPin(pin, PinPullResistance.PULL_DOWN);

        // attach callback to ,and register, gpio pin listener
        pinC.addListener(callback);
	}
	
	
	public boolean isDigitalPinHIGH(Pin pin)
	{
		boolean va;
		
		GpioPinDigitalInput pinC=controller.provisionDigitalInputPin(pin,PinPullResistance.PULL_DOWN);
		PinState state=pinC.getState();
		if(state.isHigh()) va=true;
		else  va=false;
		controller.unprovisionPin(pinC);
		return va;
	}
	
}
