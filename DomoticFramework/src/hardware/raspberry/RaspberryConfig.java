package hardware.raspberry;

import java.util.HashMap;

import com.pi4j.io.gpio.Pin;
import com.pi4j.io.gpio.RaspiPin;

//TODO: quando creo la classe devo leggere e costruire l'hashmap in base a un file di config, diverso u ogni rasp

public class RaspberryConfig {
	
	public static HashMap<String,Pin> PinConfiguration; //a map between functionality/deviceName and Raspberry physical pin
	static
    {
		RaspberryConfig.PinConfiguration= new HashMap<String,Pin>();

		//functionalities
		PinConfiguration.put("buttonWashingMachine", RaspiPin.GPIO_29); 
		PinConfiguration.put("buttonFridge", RaspiPin.GPIO_28); 
		PinConfiguration.put("buttonAirConditioning", RaspiPin.GPIO_27); 
		PinConfiguration.put("buttonBoiler", RaspiPin.GPIO_26);
				
		//devices
		PinConfiguration.put("fridge", RaspiPin.GPIO_03); 
		PinConfiguration.put("washingmachine", RaspiPin.GPIO_02); 
		PinConfiguration.put("airconditioning", RaspiPin.GPIO_05); 
		PinConfiguration.put("boiler", RaspiPin.GPIO_04); 
		PinConfiguration.put("pinPresence1", RaspiPin.GPIO_22); 
		PinConfiguration.put("pinPresence2", RaspiPin.GPIO_23);
		
		PinConfiguration.put("ledPresence1", RaspiPin.GPIO_14); 
		PinConfiguration.put("ledPresence2", RaspiPin.GPIO_13); 
    }

	//questo pu� essere uguale per ogni raspberry e quindi non scritto su file
	
	public static HashMap<String,String> functionality2device; //a map between button and deviceName
	static
    {
		RaspberryConfig.functionality2device= new HashMap<String,String>();
		
		functionality2device.put("buttonWashingMachine", "washingmachine"); 
		functionality2device.put("buttonFridge", "fridge"); 
		functionality2device.put("buttonAirConditioning", "airconditioning");
		functionality2device.put("buttonBoiler", "boiler");
		functionality2device.put("pinPresence1", "ledPresence1"); 
		functionality2device.put("pinPresence2", "ledPresence2"); 
    }
		
	
}
