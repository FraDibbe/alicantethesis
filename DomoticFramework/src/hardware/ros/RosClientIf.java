package hardware.ros;

import edu.wpi.rail.jrosbridge.messages.Message;
import edu.wpi.rail.jrosbridge.services.ServiceResponse;

public interface RosClientIf {
	//public Ros getClientIstance();
	public boolean isConnectionEstablished();
	public void callROSservice(String service, String srvType, String JSONreq, ROSCallback<ServiceResponse> callback);
	public void subscribeROStopic(String topic, String msgType, ROSCallback<Message> callback);
}
