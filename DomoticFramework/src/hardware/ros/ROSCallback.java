package hardware.ros;

public interface ROSCallback<T> {

	public void handleROStopic(T message);
}
