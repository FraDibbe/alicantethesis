package ontology;


import ontology.actions.Do;
import ontology.actions.Order;
import ontology.actions.Say;
import ontology.actions.SetScene;
import ontology.actions.ShutDown;
import ontology.actions.TurnOn;
import ontology.concepts.Actuation;
import ontology.concepts.CommandConcept;
import ontology.concepts.Convertor;
import ontology.concepts.DeviceConcept;
import ontology.concepts.EnergyDeviceConcept;
import ontology.concepts.Perception;
import ontology.concepts.RoomConcept;
import ontology.concepts.StringMessage;
import ontology.predicates.Acepted;
import ontology.predicates.FIPADecison;
import ontology.predicates.Refused;
import jade.content.onto.BeanOntology;
import jade.content.onto.Ontology;

@SuppressWarnings("serial")
public class DomoticOntology extends BeanOntology {
	
	private static Ontology theInstance = new DomoticOntology("DomoticOntology");

	
	public static Ontology getInstance() {
	return theInstance;
	}
	
	
	private DomoticOntology(String name) {
	super(name);
	
	try {
	
		/*
	//discover the path	
		List<String> classNames = new ArrayList<String>();
		ZipInputStream zip = new ZipInputStream(new FileInputStream("/home/pi/Desktop/Domo.jar"));
		for (ZipEntry entry = zip.getNextEntry(); entry != null; entry = zip.getNextEntry()) {
		    if (!entry.isDirectory() && entry.getName().endsWith(".class")) {
		        // This ZipEntry represents a class. Now, what class does it represent?
		        String className = entry.getName().replace('/', '.'); // including ".class"
		        classNames.add(className.substring(0, className.length() - ".class".length()));
		    }
		}
		
		for(String classa : classNames)
		{
			System.out.println(classa);
		}
		*/
	// Add all ontological classes included in a package
	//add("ontology.actions");
	
		//Actions
	add(Do.class);	
	add(Order.class);
	add(Say.class);
	add(SetScene.class);
	add(ShutDown.class);
	add(TurnOn.class);
	
		//Concepts
	add(Actuation.class);
	add(CommandConcept.class);
	add(Convertor.class);
	add(DeviceConcept.class);
	add(EnergyDeviceConcept.class);
	add(Perception.class);
	add(RoomConcept.class);
	add(StringMessage.class);
	
		//Predicates
	add(Acepted.class);
	add(FIPADecison.class);
	add(Refused.class);

	
	/*
	add("src.ontology.actions");
	add("src.ontology.concepts");
	add("ontology.predicates");*/
	
	}
	catch (Exception e) {
	e.printStackTrace();
	
	}
	}

}
