package ontology.predicates;

import jade.content.Predicate;

public abstract class FIPADecison implements Predicate {

	private static final long serialVersionUID = 3113906631013666336L;

	private String[] what;
	private String byWhom;
	private String serviceName;
	
	public String[] getWhat() {
		return what;
	}
	public void setWhat(String... what) {
		this.what = what;
	}
	public String getByWhom() {
		return byWhom;
	}
	public void setByWhom(String byWhom) {
		this.byWhom = byWhom;
	}
	public String getServiceName() {
		return serviceName;
	}
	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}
	
}
