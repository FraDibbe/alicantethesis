package ontology.actions;

import jade.content.AgentAction;
import jade.content.Concept;
import jade.lang.acl.ACLMessage;

public class Order implements AgentAction {

	public enum OrderType{DIRECT, INDIRECT, WRITE, READ}
	
	private static final long serialVersionUID = 6952735729303227697L;
	
	private Concept what;
	private OrderType type;
	private String service;
	private int performative=ACLMessage.REQUEST;
	private int priority;
	
	public OrderType getType() {
		return type;
	}
	public void setType(OrderType type) {
		this.type = type;
	}
	public String getService() {
		return service;
	}
	public void setService(String service) {
		this.service = service;
	}
	public int getPerformative() {
		return performative;
	}
	public void setPerformative(int performative) {
		this.performative = performative;
	}
	public int getPriority() {
		return priority;
	}
	public void setPriority(int priority) {
		this.priority = priority;
	}
	public Concept getWhat() {
		return what;
	}
	public void setWhat(Concept what) {
		this.what = what;
	}
	
	

}
