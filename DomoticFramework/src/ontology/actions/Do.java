package ontology.actions;


import jade.content.AgentAction;
import jade.content.Concept;
import jade.content.onto.annotations.Slot;
import jade.lang.acl.ACLMessage;

public class Do implements AgentAction{

	private static final long serialVersionUID = -1229289596638555540L;

	private Concept what;
	private String HW_dependend_command;
	private String service;
	private int performative= ACLMessage.REQUEST;
	private String value="";
	
	public Concept getWhat() {
		return what;
	}

	public void setWhat(Concept what) {
		this.what = what;
	}
	
	@Slot(mandatory = false)
	public String getHW_dependend_command() {
		return HW_dependend_command;
	}
	@Slot(mandatory = false)
	public void setHW_dependend_command(String hW_dependend_command) {
		HW_dependend_command = hW_dependend_command;
	}

	public String getService() {
		return service;
	}

	public void setService(String service) {
		this.service = service;
	}

	public int getPerformative() {
		return performative;
	}

	public void setPerformative(int performative) {
		this.performative = performative;
	}
	@Slot(mandatory = false)
	public String getValue() {
		return value;
	}
	@Slot(mandatory = false)
	public void setValue(String value) {
		this.value = value;
	}

	
}
