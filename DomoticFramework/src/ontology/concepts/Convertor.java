package ontology.concepts;

import jade.content.Concept;

public class Convertor implements Concept{

	private static final long serialVersionUID = 2623698652830077008L;

	private Object[] ObjectsOfConvertion;
	
	public Convertor(Object... ObjectOfConvertion) {
		this.ObjectsOfConvertion=ObjectOfConvertion;
	}

	public Object[] getObjectsOfConvertion() {
		return ObjectsOfConvertion;
	}

	public void setObjectsOfConvertion(Object[] objectsOfConvertion) {
		ObjectsOfConvertion = objectsOfConvertion;
	}

	
}
