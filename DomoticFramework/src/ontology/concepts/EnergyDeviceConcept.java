package ontology.concepts;

import jade.content.onto.annotations.Slot;


public class EnergyDeviceConcept extends DeviceConcept{

	private static final long serialVersionUID = -8946227404279354709L;
	
	private int instaneusPower;
	private int kWh;
	
	
	public int getInstantPower() {
		return instaneusPower;
	}
	public void setInstantPower(int instantPower) {
		this.instaneusPower = instantPower;
	}
	@Slot(mandatory = false)
	public int getkWh() {
		return kWh;
	}
	@Slot(mandatory = false)
	public void setkWh(int kWh) {
		this.kWh = kWh;
	}


}
