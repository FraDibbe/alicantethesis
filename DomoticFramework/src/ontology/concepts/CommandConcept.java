package ontology.concepts;

import jade.content.Concept;

public class CommandConcept implements Concept{

	private static final long serialVersionUID = 2623698652830077008L;

	private String func;
	private String deviceID;
	private String value;
	
	
	public String getFunc() {
		return func;
	}
	public void setFunc(String func) {
		this.func = func;
	}
	public String getDeviceID() {
		return deviceID;
	}
	public void setDeviceID(String deviceID) {
		this.deviceID = deviceID;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	
	public String toString()
	{
		return func+":"+deviceID+":"+value;
		
	}
	
	
}
