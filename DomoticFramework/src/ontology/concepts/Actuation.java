package ontology.concepts;


import ontology.actions.Do;
import jade.content.Concept;

public class Actuation implements Concept{

	private static final long serialVersionUID = 2623698652830077008L;

	private Do actionRequested;
	private String ServiceType;
	
	public Actuation(Do ObjectOfActuation) {
		this.setActionRequested(ObjectOfActuation);
	}

	
	public String getServiceType() {
		return ServiceType;
	}

	public void setServiceType(String serviceType) {
		ServiceType = serviceType;
	}


	public Do getActionRequested() {
		return actionRequested;
	}


	public void setActionRequested(Do actionRequested) {
		this.actionRequested = actionRequested;
	}

	
}
