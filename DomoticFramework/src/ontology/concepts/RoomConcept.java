package ontology.concepts;

import jade.content.Concept;
import jade.content.onto.annotations.AggregateSlot;

public class RoomConcept implements Concept{

	private static final long serialVersionUID = 8676506280346640400L;
	
	private int roomID;
	private int[] userIDinRoom;
	private String roomName="";
	
	public int getRoomID() {
		return roomID;
	}
	public void setRoomID(int roomID) {
		this.roomID = roomID;
	}
	@AggregateSlot(cardMin = 0, cardMax = 40)
	public int[] getUserIDinRoom() {
		return userIDinRoom;
	}
	@AggregateSlot(cardMin = 0, cardMax = 40)
	public void setUserIDinRoom(int[] userIDinRoom) {
		this.userIDinRoom = userIDinRoom;
	}
	public String getRoomName() {
		return roomName;
	}
	public void setRoomName(String roomName) {
		this.roomName = roomName;
	}
	
}
