package ontology.concepts;

import jade.content.Concept;
import jade.content.onto.annotations.Slot;

public class DeviceConcept implements Concept{

	private static final long serialVersionUID = -8946227404279354709L;
	
	private int deviceId;
	private String deviceName;
	private String type;
	private int priority;
	private String userName="";
	private int userID;
	private String actualValue="";
	
	private RoomConcept position;
	
	public int getDeviceId() {
		return deviceId;
	}
	public void setDeviceId(int deviceId) {
		this.deviceId = deviceId;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public int getPriority() {
		return priority;
	}
	public void setPriority(int priority) {
		this.priority = priority;
	}
	@Slot(mandatory = false)
	public String getUserName() {
		return userName;
	}
	@Slot(mandatory = false)
	public void setUserName(String userName) {
		this.userName = userName;
	}
	@Slot(mandatory = false)
	public int getUserID() {
		return userID;
	}
	@Slot(mandatory = false)
	public void setUserID(int userID) {
		this.userID = userID;
	}
	public String getDeviceName() {
		return deviceName;
	}
	public void setDeviceName(String deviceName) {
		this.deviceName = deviceName;
	}
	@Slot(mandatory = false)
	public RoomConcept getPosition() {
		return position;
	}
	@Slot(mandatory = false)
	public void setPosition(RoomConcept position) {
		this.position = position;
	}
	@Slot(mandatory = false)
	public String getActualValue() {
		return actualValue;
	}
	@Slot(mandatory = false)
	public void setActualValue(String actualValue) {
		this.actualValue = actualValue;
	}

}
