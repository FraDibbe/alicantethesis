package ontology.concepts;

import jade.content.Concept;
import jade.content.onto.annotations.Slot;

public class StringMessage implements Concept{

	private static final long serialVersionUID = -6694017067628533438L;

	
	private String message="";
	private String sender="";
	private String separator="";
	private boolean separated=false;
	
	/*
	public StringMessage(String author,String message) {
		this.message=message;
		this.sender=author;
	}*/
	

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	@Slot(mandatory = false)
	public String getSeparator() {
		return separator;
	}
	@Slot(mandatory = false)
	public void setSeparator(String separator) {
		this.separator = separator;
	}

	public boolean isSeparted() {
		return separated;
	}

	public void setSeparted(boolean separated) {
		this.separated = separated;
	}

	public String getSender() {
		return sender;
	}

	public void setSender(String sender) {
		this.sender = sender;
	}
	

}
