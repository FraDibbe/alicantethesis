package utils.rest;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.apache.commons.codec.binary.Base64;
import org.apache.http.HttpEntity;

public class RestUtils
{
	public static String uriEncode(String path) {
		String result = null;
		URI uri;
		try {
			uri = new URI(null, null, path, null);
			result = uri.getRawPath();
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}
		return result;
	}
	
	public static String convertStreamToString(InputStream is)
	{
		BufferedReader reader = new BufferedReader(new InputStreamReader(is));
		StringBuilder sb = new StringBuilder();

		String line = null;
		
		try {
			while ((line = reader.readLine()) != null) {
				sb.append(line + "\n");
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				is.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return sb.toString();
	}
	
	public static byte[] convertStreamToByteArray(InputStream is, int length) throws IOException
    {
    	byte[] tmp = new byte[length];
    	int offset = 0;
    	
    	do {
    		offset += is.read(tmp, offset, length - offset);
    	} while (offset < length);
    	
    	
    	return tmp;
    }
    
    public static boolean convertEntityToBool(HttpEntity entity) throws IllegalStateException, IOException
    {
    	InputStream instream = entity.getContent();
		String response = convertStreamToString(instream).trim();
		instream.close();
		return Boolean.parseBoolean(response);
    }
    
    public static String base64encode(String data) {
    	// FIXME: Android not found Base64.encodeBase64String method, so I replace it with next code
    	String encodedString = new String(Base64.encodeBase64(data.getBytes()));
    	String safeString = encodedString.replace('+','-').replace('/','_');
    	return safeString.trim();
	}
    
 // Return a 40 chars length string
 	public static String getHashSha1(String data) throws NoSuchAlgorithmException
 	{
 		MessageDigest md = MessageDigest.getInstance("SHA1");
 		String result = null;
 		
 		md.update(data.getBytes());
 		byte[] output = md.digest();
 	    result = bytesToHex(output);
 	    
 		return result;
 	}
 	
 	public static String bytesToHex(byte[] b)
 	{
 		char hexDigit[] = {'0', '1', '2', '3', '4', '5', '6', '7',
 				'8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};
 				
 		StringBuffer buf = new StringBuffer();
 			
 		for (int j=0; j<b.length; j++) {
 			buf.append(hexDigit[(b[j] >> 4) & 0x0f]);
 			buf.append(hexDigit[b[j] & 0x0f]);
 		}
 		return buf.toString();
 	}
}
