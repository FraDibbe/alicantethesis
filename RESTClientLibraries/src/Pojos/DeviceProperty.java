package Pojos;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;



/**         
 * @author Jose Padilla (<a href="mailto:jpadilla@dtic.ua.es">jpadilla@dtic.ua.es</a>)
 **/
@XmlAccessorType(XmlAccessType.NONE)
@XmlRootElement(name="property")
public final class DeviceProperty {
	
	private int deviceId;
	
	@XmlAttribute
	private String name;
	
	private ArrayList<String> value;
	
	private String converter;
	
	@XmlAttribute
	private PropertyType type;
	
	@XmlAttribute
	private AccessMode accessMode;
	
	
	// Needed for JAXB
	public DeviceProperty() {}
	
	public DeviceProperty(int deviceId, String name, PropertyType type, AccessMode accessMode) {
		this.deviceId = deviceId;
		this.name = name;
		this.type = type;
		this.accessMode = accessMode;
	}
	
	public DeviceProperty(int deviceId, String name, String converter, PropertyType type, AccessMode accessMode) {
		this.deviceId = deviceId;
		this.name = name;
		this.value = new ArrayList<String>();
		this.type = type;
		this.accessMode = accessMode;
		this.converter = converter;
	}
	
	public int getDeviceId() {
		return deviceId;
	}
	
	public String getName() {
		return name;
	}

	public List<String> getValues() {
		return value;
	}
	
	public String getConverter() {
		return converter;
	}

	public PropertyType getType() {
		return type;
	}
	
	public AccessMode getAccessMode() {
		return accessMode;
	}
}