package Pojos;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlValue;

/**
 * <p>La clase Service representa uno de los servicios de la vivienda.</p>
 * 
 * @author <a href="http://es.linkedin.com/in/miguelcabo">Miguel Cabo Diez -
 *         DTIC</a> Email: <a
 *         href="mailto:miguelcabodiez@gmail.com">miguelcabodiez@gmail.com</a>
 */

@XmlRootElement(name="service")
public class Service
{
	@XmlValue
	private String service = "";
	
	public Service(){this.service = "";}
	
	public Service(String service){this.service = service;}
	
	/**
	 * Obtiene el nombre del servicio
	 * @return Devuelve una cadena de texto con el nombre del servicio
	 */
	public String getName() {
		return service;
	}
}
