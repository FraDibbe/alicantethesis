package Pojos;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/* _______classe che rappresenta l'oggetto Log ___________ 
 * 
 * E' il MODEL del Log
 * 
 * */

/**
 * <p>La clase Log contiene los atributos que serán almacenados/recuperados en el registro
 * de actividad de un dispositivo</p>
 * 
 * @author <a href="http://es.linkedin.com/in/miguelcabo">Miguel Cabo Diez -
 *         DTIC</a> Email: <a
 *         href="mailto:miguelcabodiez@gmail.com">miguelcabodiez@gmail.com</a>
 *         
 * @author Jose Padilla (<a href="mailto:jpadilla@dtic.ua.es">jpadilla@dtic.ua.es</a>)
 * 
 */
@XmlType(propOrder = { "time", "source", "room", "deviceId", "deviceName", "functionality", "value" })
@XmlAccessorType(XmlAccessType.NONE)
@XmlRootElement
public class Log
{
	@XmlElement (name = "source")
	private String source;
	
	@XmlElement (name = "room")
	private String room;
	
	@XmlElement (name = "deviceId")
	private int deviceId;
	
	@XmlElement (name = "name")
	private String 	deviceName;
	
	@XmlElement (name = "functionality")
	private String 	functionality;
	
	@XmlElement (name = "value")
	private String 	value;
	
	@XmlElement (name = "time")
	private Date time;
	
	
	public Log(){}
	

	public Log(String source, int idDevice, String room, String device, String func, String value, String time) {
		
		this.source = source;
		deviceId = idDevice;
		this.room = room;
		deviceName = device;
		functionality = func;
		this.value = value;
		
		SimpleDateFormat format= new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssX");
		Date d = null;
		
		try {	
			d = format.parse(time);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		this.time = d;
	}
	
	public String getSource() {
		return source;
	}
	
	/**
	 * Obtiene el identificador del dispositivo asociado al registro
	 * @return Devuelve el identificador del dispositivo
	 */
	public int getDeviceId() {
		return deviceId;
	}
	
	public String getRoom() {
		return room;
	}
	
	/**
	 * Obtiene el nombre del dispositivo
	 * @return Devuelve el nombre del dispositivo
	 */
	public String getDeviceName() {
		return deviceName;
	}
	
	/**
	 * Obtiene el nombre de la funcionalidad
	 * @return Devuelve el nombre de la funcionalidad
	 */
	public String getFunctionality() {
		return functionality;
	}
	
	/**
	 * Obtiene el valor de la funcionalidad
	 * @return Devuelve el valor de la funcionalidad
	 */
	public String getValue() {
		return value;
	}
	
	/**
	 * Obtiene la fecha y hora del registro
	 * @return Devuelve la fecha y hora del registro
	 */
	public Date getTime() {
		return time;
	}
	
	@Override 
	public String toString()
	{
		return "Time: "+time.toString()+"\t"+
				"Source: "+source+"\t"+
				"Room: "+room+"\t"+
				"DeviceID: "+deviceId+"\t"+
				"Device Name: "+deviceName+"\t"+
				"Functionality: "+functionality+"\t"+
				"Value: "+value;
		
	}
	
}
