package Pojos;

import java.util.Collection;
import java.util.TreeMap;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


@XmlAccessorType(XmlAccessType.NONE)
@XmlRootElement(name = "properties")
public class PropertiesWrapper {
	
	protected TreeMap<String, DeviceProperty> properties = null;
	
	public PropertiesWrapper() {
		properties = new TreeMap<String, DeviceProperty>();
	}
	
	public DeviceProperty get(String name) {
		return properties.get(name);
	}
	
	public void put(String name, DeviceProperty property) {
		properties.put(name, property);
	}
	
	public boolean hasProperties() {
		return !properties.isEmpty();
	}
	
	@XmlAttribute(name="size")
	public int getSize() {
		return properties.size();
	}
	
	@XmlElement(name="property")
	public Collection<DeviceProperty> getCollection() {
		return properties.values();
	}
}
