package Pojos;

public enum PropertyType {
	
	STATIC,
	DYNAMIC;
	
	public static PropertyType parse(String property) {
		String myProp = property.toUpperCase();
			
		if (myProp.equals("DYNAMIC"))
			return DYNAMIC;
		else 
			return STATIC;
	}
}