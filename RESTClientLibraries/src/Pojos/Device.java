package Pojos;

import java.util.Collection;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


/**        
 * @author Jose Padilla (<a href="mailto:jpadilla@dtic.ua.es">jpadilla@dtic.ua.es</a>)
 **/

@XmlAccessorType(XmlAccessType.NONE)
@XmlRootElement
public class Device implements Comparable<Device>
{
	@XmlAttribute
	protected int id;

	@XmlAttribute
	protected String name;
	
	@XmlElement
	protected String technology;

	@XmlElement
	protected String ref; // referencia de fábrica (número de serie)
	
	@XmlElement
	protected String manufacturer;
	
	@XmlElement
	protected String brand;
	
	@XmlElement
	protected String model;
	
	@XmlElement
	protected String description;
	
	@XmlElement
	protected Date manufactureDate;

	@XmlElement
	protected Date lastRevisionDate;

	@XmlElement
	protected Date nextRevisionDate;
	
	@XmlAttribute
	protected String deviceType; // tipo dispositivo  (BinaryLighting / DimmableLighting / Blind / HAVC / Relay / Sensor / Door) 

	@XmlAttribute
	protected String componentType; // Tipo de componente (Sensor / Actuador / objeto / ...)
	
	@XmlElement
	protected List<String> services; // separados por ','

	@XmlElement
	protected String place;
	
	@XmlElement
	private PropertiesWrapper properties;
	
	
	// Need for JAXB
	public Device() {
		this.services = new LinkedList<String>();
		this.properties = new PropertiesWrapper();
	}
	
	public Device(String id, String name, String deviceType, String componentType) {
		this.id = Integer.parseInt(id);
		this.name = name;
		this.deviceType = deviceType;
		this.componentType = componentType;
		this.services = new LinkedList<String>();
		this.properties = new PropertiesWrapper();
	}
	
	public Device(int id, String name, String technology, String place, String componentType, String devicesType) {
		this.id = id;
		this.name = name;
		this.technology = technology;
		this.place = place;
		this.componentType = componentType;
		this.deviceType = devicesType;
		this.services = new LinkedList<String>();
		this.properties = new PropertiesWrapper();
	}
	
	
	/**
	 * Obtiene el identificador del dispositivo.
	 * @return Devuelve el identificador del dispositivo.
	 */
	public int getId() {
		return id;
	}

	/**
	 * Obtiene el nombre del dispositivo.
	 * @return Devuelve el nombre del dispositivo.
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * Obtiene la tecnología (Lonworks / KNX / ANALOGICO / DALI /...).
	 * @return Devuelve el tipo de tecnología (Lonworks / KNX / ANALOGICO / DALI /...).
	 */
	public String getTechnology() {
		return technology;
	}
	
	public void setTechnology(String technology) {
		this.technology = technology;
	}
	
	/**
	 * Obtiene la referencia de fábrica.
	 * @return Devuelve la referencia de fábrica.
	 */
	public String getRef() {
		return ref;
	}
	
	public void setRef(String ref) {
		this.ref = ref;
	}
	
	/**
	 * Obtiene el fabricante.
	 * @return Devuelve el nombre del fabricante.
	 */
	public String getManufacturer() {
		return manufacturer;
	}
	
	public void setManufacturer(String manufacturer) {
		this.manufacturer = manufacturer;
	}
	
	/**
	 * Obtiene la marca.
	 * @return Devuelve la marca del dispositivo
	 */
	public String getBrand() {
		return brand;
	}
	
	public void setBrand(String brand) {
		this.brand = brand;
	}
	
	/**
	 * Obtiene el modelo.
	 * @return Devuelve el modelo del dispositivo
	 */
	public String getModel() {
		return model;
	}
	
	public void setModel(String model) {
		this.model = model;
	}
	
	/**
	 * Obtiene la descripción.
	 * @return Devuelve la descripción del dispositivo
	 */
	public String getDescription() {
		return this.description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	/**
	 * Obtiene la fecha de fabricación.
	 * @return Devuelve la fecha de fabricación.
	 */
	public Date getManufactureDate() {
		return manufactureDate;
	}
	
	public void setManufactureDate(Date date) {
		manufactureDate = date;
	}
	
	/**
	 * Obtiene la fecha de la última revisión.
	 * @return Devuelve la fecha de la última revisión.
	 */
	public Date getLastRevisionDate() {
		return lastRevisionDate;
	}
	
	public void setLastRevisionDate(Date date) {
		lastRevisionDate = date;
	}

	/**
	 * Obtiene la fecha de la siguiente revisión.
	 * @return Devuelve la fecha de la siguiente revisión.
	 */
	public Date getNexRevisionDate() {
		return nextRevisionDate;
	}
	
	public void setNextRevisionDate(Date date) {
		nextRevisionDate = date;
	}

	/**
	 * Obtiene la estancia en la cual esta ubicado.
	 * @return Devuelve la estancia en la cual está ubicado.
	 */
	public String getPlace() {
		return place;
	}
	
	public void setPlace(String place) {
		this.place = place;
	}

	/**
	 * Obtiene los servicios a los que pertenece (Lighting / Sensing / Acting).
	 * @return Devuelve los servicios a los que pertenece (Lighting / Sensing / Acting).
	 */
	public List<String> getServices() {
		return services;
	}

	
	/**
	 * Obtiene el tipo de dispositivo (BinaryLighting / DimmableLighting
	 * / Blind / HAVC / Relay / Sensor / Door).
	 * @return Devuelve el tipo de dispositivo (BinaryLighting / DimmableLighting / Blind / HAVC / Relay / Sensor / Door)
	 */
	public String getDeviceType() {
		return deviceType;
	}

	/**
	 * Obtiene el tipo de componente (Sensor / Actuador / objeto / ...).
	 * @return Devuelve el tipo de componente (Sensor / Actuador / objeto / ...).
	 */
	public String getComponentType() {
		return this.componentType;
	}

	/**
	 * Obtiene las funcionalidades.
	 * @return Devuelve una lista con las funcionalidades que soporte el dispositivo
	 */
	public Collection<DeviceProperty> getProperties() {
		return properties.getCollection();
	}
	
	/**
	 * Obtiene la funcionalidad del dispositivo indicada
	 * @param name Nombre la funcionalidad a recuperar
	 * @return Devuelve la funcionaldad del dispositivo indicada como parámetro
	 */
	public DeviceProperty getProperty(String name) {
		return properties.get(name); 
	}
	
	public void addProperty(DeviceProperty property) {
		if (property != null)
			properties.put(property.getName(), property);
	}
	
	public void addService(String service) {
		if (services != null)
			services.add(service);
	}
	
	public int compareTo(Device d2) {
		if (id < d2.getId())
			return -1;
		else if (id == d2.getId())
			return 0;
		else
			return 1;
	}
	
	@Override 
	public String toString()
	{
		return " ID: "+id+"\t"+
				"Name: "+name+"\t"+
				"Place: "+place+"\t"+
				"Service: "+services.toString()+"\t"+
				"Technology:"+technology+"\t"+
				"Type: "+deviceType+"\t"+
				"Component Type: "+componentType;
	}
	
}