package Pojos;

public class Response<T> {

  private T value;
  private String type;
  
public T getValue() {
	return value;
}

public void setValue(T value) {
	this.value = value;
}

public String getType() {
	return type;
}

public void setType(String type) {
	this.type = type;
}
	
  
  
}
