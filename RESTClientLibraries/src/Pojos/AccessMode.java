package Pojos;

public enum AccessMode {
	
	READ(1),
	WRITE(2),
	READWRITE(3);
	
	private int code;
	
	private AccessMode(int code) {  
		this.code = code;  
	}  
	   
	public int getCode() {  
		return code;  
	}
	
	public static AccessMode parse(int code) {
		if (code == 1)
			return READ;
		else if (code == 2)
			return WRITE;
		else if (code == 3)
			return READWRITE;
		else 
			return READ;
	}
	
	public static AccessMode parse(String str) {
		if (str.equals("READ"))
			return READ;
		else if (str.equals("WRITE"))
			return WRITE;
		else if (str.equals("READWRITE"))
			return READWRITE;
		else 
			return READ;
	}
}