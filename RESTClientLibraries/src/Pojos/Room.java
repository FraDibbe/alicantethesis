package Pojos;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlValue;

/**
 * <p>La clase Room representa una de las estancias de la vivienda.</p>
 * 
 * @author <a href="http://es.linkedin.com/in/miguelcabo">Miguel Cabo Diez -
 *         DTIC</a> Email: <a
 *         href="mailto:miguelcabodiez@gmail.com">miguelcabodiez@gmail.com</a>
 * 
 */

@XmlRootElement(name = "room")
public class Room
{
	@XmlValue
	private String room = "";
	
	public Room(){this.room = "";};
	
	public Room(String room){this.room = room;}
	
	/**
	 * Obtiene el nombre de la habitación
	 * @return Devuelve una cadena de texto con el nombre de la habitación
	 */
	public String getName() {
		return room;
	}
}