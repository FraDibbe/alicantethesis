package parser;


import java.util.ArrayList;
import java.util.List;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import Pojos.Device;
/* _______classe che parserizza le risposte XML dei servizi REST del Device ___________ 
 * 
 * Parserizza XML e restituisce oggetti Java POJO
 * 
 * */

public class DeviceParseHandle extends DefaultHandler {
	
	//contains all the log entry found in the XML document
	private List<Device> deviceEntry;
	private Device currentDevice;
	
	private boolean elementDeviceCreation = false;
	
	private int currentId;
	private String currentName;
	private String currentTech;
	private String currentPlace;
	private String currentCmpType;
	private String currentDevType;
	private String currentService;
	
	private String label;

	public List<Device> getParsedData() {
		return deviceEntry;
	}

	public DeviceParseHandle() {
		super();
		deviceEntry= new ArrayList<Device>();
	}

	@Override
	public void startElement(String namespaceURI, String localName, String qName, Attributes atts) throws SAXException
	{
		super.startElement(namespaceURI, localName, qName, atts);
		
		// Process Device Info
		if (localName.equalsIgnoreCase("DEVICE")&&!elementDeviceCreation)	elementDeviceCreation=true;	
		//save the label value, used in this.characters() method
		label=localName.toLowerCase();
		if(!(label.equalsIgnoreCase("properties") || label.equalsIgnoreCase("property")) )manageAttributes(atts);
	}
	

	@Override
	public void endElement(String namespaceURI, String localName, String qName)	throws SAXException {
		super.endElement(namespaceURI, localName, qName);
		
		// Process Device Info
        if (localName.equalsIgnoreCase("DEVICE") && elementDeviceCreation)
        {
        	//construct the Log element and put it in the arraylist
        	currentDevice= new Device(currentId,currentName,currentTech,currentPlace,currentCmpType,currentDevType);
        	deviceEntry.add(currentDevice);
        	elementDeviceCreation=false;
        }
	}

	@Override
	public void characters(char ch[], int start, int length) throws SAXException {
		super.characters(ch, start, length);
        	
		String readed = new String(ch, start, length).toLowerCase();
		
		switch(label)
		{
		case "technology":		
			currentTech=readed;
			break;
		case "services":
			currentService=readed;
			break;		
		case "place":
			currentPlace=readed;
			break;		
		case "properties":
			//TODO: add a device Property object to the parser!
			break;		
		}
	}
	
	
	private void manageAttributes(Attributes attr)
	{
		int lenght= attr.getLength();
		
		for(int i=0; i< lenght;i++)
		{
			switch(attr.getLocalName(i).toLowerCase())
			{
			case "id":		
				currentId=Integer.parseInt(attr.getValue(i));
				break;
			case "name":
				currentName=attr.getValue(i);
				break;		
			case "devicetype":
				currentDevType=attr.getValue(i);
				break;		
			case "componenttype":
				currentCmpType=attr.getValue(i);
				break;		
			}
		
		}	
	}
	
	

}
