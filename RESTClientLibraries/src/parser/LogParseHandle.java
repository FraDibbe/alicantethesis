package parser;


import java.util.ArrayList;
import java.util.List;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import Pojos.Log;
/* _______classe che parserizza le risposte XML dei servizi REST del Log ___________ 
 * 
 * Parserizza XML e restituisce oggetti Java POJO
 * 
 * */

public class LogParseHandle extends DefaultHandler {
	
	//contains all the log entry found in the XML document
	private List<Log> logEntry;
	private Log currentLog;
	private boolean elementLogCreation = false;
	private String currentSource;
	private int currentIdDevice;
	private String currentRoom;
	private String currentDevice;
	private String currentFunc;
	private String currentValue;
	private String currentTime;
	private String label;

	public List<Log> getParsedData() {
		return logEntry;
	}

	public LogParseHandle() {
		super();
		logEntry= new ArrayList<Log>();
	}

	@Override
	public void startElement(String namespaceURI, String localName, String qName, Attributes atts) throws SAXException
	{
		super.startElement(namespaceURI, localName, qName, atts);
		
		// Process Log Info
		if (localName.equalsIgnoreCase("LOG")&&!elementLogCreation)	elementLogCreation=true;	
		//save the label value, used in characters() method
		label=localName.toLowerCase();
		
		
	}

	@Override
	public void endElement(String namespaceURI, String localName, String qName)	throws SAXException {
		super.endElement(namespaceURI, localName, qName);
		
		// Process Device Info
        if (localName.equalsIgnoreCase("LOG") && elementLogCreation)
        {
        	//construct the Log element and put it in the arraylist
        	currentLog= new Log(currentSource,currentIdDevice, currentRoom, currentDevice, currentFunc,currentValue,currentTime);
        	logEntry.add(currentLog);
        	elementLogCreation=false;
        }
	}

	@Override
	public void characters(char ch[], int start, int length) throws SAXException {
		super.characters(ch, start, length);
        	
		String readed = new String(ch, start, length).toLowerCase();
		
		switch(label)
		{
		case "time":		
			currentTime=readed.toUpperCase();
			break;
		case "source":
			currentSource=readed;
			break;		
		case "room":
			currentRoom=readed;
			break;		
		case "deviceId":
			currentIdDevice=Integer.parseInt(readed);
			break;		
		case "name":
			currentDevice=readed;
			break;			
		case "functionality":
			currentFunc=readed;
			break;			
		case "value":
			currentValue=readed;
			break;
		}
	}
	
	
	
	

}
