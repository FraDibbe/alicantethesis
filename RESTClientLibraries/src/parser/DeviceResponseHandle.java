package parser;


import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import Pojos.Response;
/* _______classe che parserizza le risposte XML dei servizi REST di Device ___________ 
 * 
 * Parserizza XML e restituisce oggetti Java POJO
 * 
 * */

public class DeviceResponseHandle<T> extends DefaultHandler {
	
	//contains all the log entry found in the XML document
	private Response<T> responseEntry;
	private boolean elementResponseCreation=false;
	private String label;

	public DeviceResponseHandle()
	{
		responseEntry=new Response<T>();
	}
	
	public Response<T> getParsedData() {
		return responseEntry;
	}

	@Override
	public void startElement(String namespaceURI, String localName, String qName, Attributes atts) throws SAXException
	{
		super.startElement(namespaceURI, localName, qName, atts);
		
		// Process Log Info
		if (localName.equalsIgnoreCase("VALUE")&&!elementResponseCreation)	elementResponseCreation=true;	
		//save the label value, used in characters() method
		label=localName.toLowerCase();
		manageAttributes(atts);
				
	}

	@Override
	public void endElement(String namespaceURI, String localName, String qName)	throws SAXException {
		super.endElement(namespaceURI, localName, qName);
		
		// Process Device Info
        if (localName.equalsIgnoreCase("VALUE") && elementResponseCreation)
        {
        	//finish to construct the element
        	elementResponseCreation=false;
        }
	}

	@SuppressWarnings("unchecked")
	@Override
	public void characters(char ch[], int start, int length) throws SAXException {
		super.characters(ch, start, length);
        	
		String readed = new String(ch, start, length).toLowerCase();
		
		if(label.equalsIgnoreCase("VALUE"))	responseEntry.setValue( (T) readed.toLowerCase());
		
	}
	
	
	private void manageAttributes(Attributes attr)
	{
		int lenght= attr.getLength();
		
		for(int i=0; i< lenght;i++)
		{
			if(attr.getLocalName(i).equalsIgnoreCase("TYPE")) responseEntry.setType(attr.getValue(i));
		}	
	}
	

}
