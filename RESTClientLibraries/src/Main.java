import java.io.IOException;
import java.net.URISyntaxException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.http.client.ClientProtocolException;

import Client.DeviceClient;
import Client.DeviceCommandClient;
import Client.LogClient;
import Client.exception.AuthRequiredException;
import Pojos.Device;
import Pojos.Log;



/* _______classe di test del client log ___________ 
 * 
 * Istanzia un LogClient
 * 
 * Richiede i servizi REST
 * 
 * Li scrive a schermo(console)
 * 
 * */

public class Main {


	
	public static void main(String[] args) {
		
		/*		 ******************* LOG TEST ************************
		LogClient client=null;
		
		try {
			client=new LogClient("shanon.iuii.ua.es/s/rest/");
			
			// Setting authorization credentials like user and password
			client.setAuth("fra.dibenedetto1@gmail.com", "27uaj45lM");
			
			
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		System.out.println("*** Client Log creato..... Executing Methods***"); 
		
		if(client != null){
			try {
				
				
				ArrayList<Log> logList=new ArrayList<Log>(client.getTodayLog());
				
				//print all logList values	
				for (Log logElem : logList)  System.out.println(logElem.toString());
				System.out.println("*** Method 1 executed ***"); 
				
				// Create a date formatter that can parse dates of the form yyyy-MM-dd.
		        SimpleDateFormat bartDateFormat = new SimpleDateFormat("yyyy-MM-dd");
		 
		        // Create a string containing a text date to be parsed.
		        String startDate = "2015-03-05";
		        String endDate = "2015-03-05";
		 
		        Date start=null,end=null;
		        
		        try {
		             start = bartDateFormat.parse(startDate);
		             end = bartDateFormat.parse(endDate);
		        }
		        catch(ParseException e)
		        {e.printStackTrace();}
		        
		        ArrayList<Log> logList2=new ArrayList<Log>(client.getLog(start, end));
				for (Log logElem : logList2)  System.out.println(logElem.toString());
				System.out.println("*** Method 2 executed . FINISHING ***"); 
				
			} catch (ClientProtocolException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (AuthRequiredException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			   
		}*/
		
				//	 ******************* COMMAND TEST ************************
		
			DeviceCommandClient<String> client=null;
		
		try {
			client=new DeviceCommandClient<String>("shanon.iuii.ua.es/s/rest/");
			
			// Setting authorization credentials like user and password
			client.setAuth("fra.dibenedetto1@gmail.com", "27uaj45lM");
			
			
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		System.out.println("*** Client Command creato..... Executing Methods***"); 
		
		if(client != null){
			try {
				
				
				String value =client.giveCommand(15, "switch", "1");
				//print all logList values	
				System.out.println("*** Method 1 executed *** ::: value get back: "+ value); 
				
				String value2 =client.getData(15,"dataSwitch");
				//print all logList values	
				System.out.println("*** Method 2 executed *** ::: value get back: "+ value2); 
				
			}
			catch (ClientProtocolException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (AuthRequiredException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		
		/*
		// ******************* DEVICE TEST ************************
		
		DeviceClient clientD=null;
		
		try {
			clientD=new DeviceClient("shanon.iuii.ua.es/s/rest/");
			
			// Setting authorization credentials like user and password
			clientD.setAuth("fra.dibenedetto1@gmail.com", "27uaj45lM");
			
			
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		System.out.println("*** Client Dev creato..... Executing Methods***"); 
		
		
		if(clientD != null){
			try {
				
				
				List<Device> devices =clientD.getDevicesForService("Lighting");
				//print all logList values	
				for (Device devElem : devices)  System.out.println(devElem.toString());
				System.out.println("*** Method 1 executed ***"); 
				
			}
			catch (ClientProtocolException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (AuthRequiredException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
						
		}
		
		*/
		
		
		
		
		
		
	}	
}
