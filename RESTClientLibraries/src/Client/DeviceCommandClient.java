package Client;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;



import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;



import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;

import parser.DeviceResponseHandle;
import utils.rest.RestUtils;
import Client.exception.AuthRequiredException;
import Pojos.Response;



/* Client per i servizi RESTful di LOG
 * 
 * Apre le connessini HTTP con autenticazione
 * 
 * Invia le richieste e aspetta i risultati
 * 
 * Richiama il parser e crea gli oggetti JAVA POJO corrispondenti
 * 
 * */

public class DeviceCommandClient<T> implements DeviceCommandClientInterface, RESTClientIf{
	
	private URI basicURI; 
	private String _base64auth = null;
	
	public DeviceCommandClient(String uri) throws URISyntaxException
	{
		this.basicURI = new URIBuilder()
					        .setScheme("http")
					        .setHost(uri+"home/")
					        //.setPath("/search")
					        .build();
	}

	
	public void setAuth(String username, String password)
	{
		_base64auth = RestUtils.base64encode(username + ":" + password);
	}

	

	@Override
	public String giveCommand(int id, String func, String value)
			throws ClientProtocolException, IOException, AuthRequiredException {
		
			Response<T> result;
		
			//create the URI of the RESTful service
				String restURI="device/"+id+"/write/"+func+"/"+value;
				System.out.println(basicURI+restURI);
				
				//request the service via GET
				CloseableHttpClient client=HttpClients.createDefault();
				CloseableHttpResponse response = executeRequest(client,basicURI+restURI);
				
				//read the response
				InputStream xmlStream = response.getEntity().getContent();
				result= parseDeviceAction(new InputSource(xmlStream));
				xmlStream.close();	
		    	client.close();
						    	
		    	//Parse and return a Log Object
		    	return  result.getValue().toString();
	}


	@Override
	public String getData(int id, String func)
			throws ClientProtocolException, IOException, AuthRequiredException {
		
			Response<T> result;
		
		//create the URI of the RESTful service
				String restURI="/device/"+id+"/read/dataSwitch";
				System.out.println(basicURI+restURI);
				
				//request the service via GET
				CloseableHttpClient client=HttpClients.createDefault();
				CloseableHttpResponse response = executeRequest(client,basicURI+restURI);
				
				//read the response
				InputStream xmlStream = response.getEntity().getContent();
				result= parseDeviceAction(new InputSource(xmlStream));
				xmlStream.close();	
		    	client.close();
				
		    	//Parse and return a Log Object
		    	return result.getValue().toString();
	}
	
			
	private CloseableHttpResponse executeRequest(CloseableHttpClient client, String request) throws IOException, AuthRequiredException
    {		
		HttpGet httpget = new HttpGet(request);
		
		if (_base64auth != null)
			httpget.addHeader("Authorization", "Basic " + _base64auth);
	
		CloseableHttpResponse response = client.execute(httpget);

		int responseCode = response.getStatusLine().getStatusCode();
			
		if (responseCode == 401) {
			client.close();
			throw new AuthRequiredException("Server require authentication");
		}
		
		return response;
    }
		
	@SuppressWarnings("unchecked")
	private Response<T> parseDeviceAction(InputSource source)
	{
		Response<T> result = null;
		
		try {
	    	SAXParserFactory spf = SAXParserFactory.newInstance();
	    	spf.setNamespaceAware(true);
			SAXParser sp = spf.newSAXParser();
			
			/* Get the XMLReader of the SAXParser we created. */
			XMLReader xr = sp.getXMLReader();
			
			/* Create a new ContentHandler and apply it to the XML-Reader*/ 
			DeviceResponseHandle<Boolean> myDeviceRespHandler = new DeviceResponseHandle<Boolean>();
			xr.setContentHandler(myDeviceRespHandler);
			
			/* Parse the xml-data from our stream */
			xr.parse(source);
	
			/* Our handler now provides the parsed data to us. */
			result = (Response<T>) myDeviceRespHandler.getParsedData();
		}
		catch (Exception e) {
			System.err.println(e.getMessage());
		}
		
		return result;
	}

	

}
