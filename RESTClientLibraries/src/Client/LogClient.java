package Client;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;



import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;

import parser.LogParseHandle;
import utils.rest.RestUtils;
import Client.exception.AuthRequiredException;
import Pojos.Device;
import Pojos.Log;
import Pojos.Room;


/* Client per i servizi RESTful di LOG
 * 
 * Apre le connessini HTTP con autenticazione
 * 
 * Invia le richieste e aspetta i risultati
 * 
 * Richiama il parser e crea gli oggetti JAVA POJO corrispondenti
 * 
 * */

public class LogClient implements LogClientInterface,RESTClientIf{
	
	private URI basicURI; 
	private String _base64auth = null;
	
	public LogClient(String uri) throws URISyntaxException
	{
		this.basicURI = new URIBuilder()
					        .setScheme("http")
					        .setHost(uri+"log/")
					        //.setPath("/search")
					        .build();
	}

	
	public void setAuth(String username, String password)
	{
		_base64auth = RestUtils.base64encode(username + ":" + password);
	}

	public List<Log> getTodayLog() throws IOException, AuthRequiredException {
		//get the date of today
		Date today = new Date();
		List<Log> result;
		
		
		//create the URI of the RESTful service
		String restURI=dateFormatter(today)+"/"+dateFormatter(today);
		System.out.println(basicURI+restURI);
		
		//request the service via GET
		CloseableHttpClient client=HttpClients.createDefault();
		CloseableHttpResponse response = executeRequest(client,basicURI+restURI);
		
		//read the response
		InputStream xmlStream = response.getEntity().getContent();
		result= parse(new InputSource(xmlStream));
		/*
		BufferedReader br = new BufferedReader(new InputStreamReader( response.getEntity().getContent() ));
        String xmlStream="",line;
        while ((line = br.readLine()) != null) 	xmlStream+=line;	
        System.out.println(xmlStream);
		*/
		xmlStream.close();	
    	client.close();
		
    	//Parse and return a Log Object
    	return result;
    	
	}

	public List<Log> getLog(Date start, Date end) throws IOException, AuthRequiredException {
		
		List<Log> result;
		
		//create the URI of the RESTful service
		String restURI="/"+dateFormatter(start)+"/"+dateFormatter(end);
		System.out.println(basicURI+restURI);
				
		//request the service via GET
		CloseableHttpClient client=HttpClients.createDefault();
		CloseableHttpResponse response = executeRequest(client,basicURI+restURI);
				
		//read the response
			InputStream xmlStream = response.getEntity().getContent();
			result= parse(new InputSource(xmlStream));
			xmlStream.close();	
	    	client.close();
			
	    //Parse and return a Log Object
	    return result;
	}


	public List<Log> getLog(Date start, Date end, Device device) throws IOException, AuthRequiredException {
		List<Log> result;
		
		//create the URI of the RESTful service
		String restURI="/"+dateFormatter(start)+"/"+dateFormatter(end)+"/device/"+device.getId();
		System.out.println(basicURI+restURI);
				
		//request the service via GET
		CloseableHttpClient client=HttpClients.createDefault();
		CloseableHttpResponse response = executeRequest(client,basicURI+restURI);
				
		//read the response
		InputStream xmlStream = response.getEntity().getContent();
		result= parse(new InputSource(xmlStream));
		xmlStream.close();	
	    client.close();
			
	    //Parse and return a Log Object
	    return result;
	}

	public List<Log> getLog(Date start, Date end, Room room) throws IOException, AuthRequiredException {
		List<Log> result;
		
		//create the URI of the RESTful service
		String restURI="/"+dateFormatter(start)+"/"+dateFormatter(end)+"/room/"+room.getName();
		System.out.println(basicURI+restURI);
				
		//request the service via GET
		CloseableHttpClient client=HttpClients.createDefault();
		CloseableHttpResponse response = executeRequest(client,basicURI+restURI);
				
		//read the response
		InputStream xmlStream = response.getEntity().getContent();
		result= parse(new InputSource(xmlStream));
		xmlStream.close();	
	    client.close();
			
	    //Parse and return a Log Object
	    return result;
	}
	
	
	private CloseableHttpResponse executeRequest(CloseableHttpClient client, String request) throws IOException, AuthRequiredException
    {		
		HttpGet httpget = new HttpGet(request);
		
		if (_base64auth != null)
			httpget.addHeader("Authorization", "Basic " + _base64auth);
	
		CloseableHttpResponse response = client.execute(httpget);

		int responseCode = response.getStatusLine().getStatusCode();
			
		if (responseCode == 401) {
			client.close();
			throw new AuthRequiredException("Server require authentication");
		}
		
		return response;
    }
	
	//format the dates objects
	private String  dateFormatter(Date date)
	{
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		return dateFormat.format(date).toString();		
	}
	
	
	private List<Log> parse(InputSource source)
	{
		List<Log> result = null;
		
		try {
	    	SAXParserFactory spf = SAXParserFactory.newInstance();
	    	spf.setNamespaceAware(true);
			SAXParser sp = spf.newSAXParser();
			
			/* Get the XMLReader of the SAXParser we created. */
			XMLReader xr = sp.getXMLReader();
			
			/* Create a new ContentHandler and apply it to the XML-Reader*/ 
			LogParseHandle myLogHandler = new LogParseHandle();
			xr.setContentHandler(myLogHandler);
			
			/* Parse the xml-data from our stream */
			xr.parse(source);
	
			/* Our handler now provides the parsed data to us. */
			result = myLogHandler.getParsedData();
		}
		catch (Exception e) {
			System.err.println(e.getMessage());
		}
		
		return result;
	}
	

}
