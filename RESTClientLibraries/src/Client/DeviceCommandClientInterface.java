package Client;

import java.io.IOException;


import org.apache.http.client.ClientProtocolException;

import Client.exception.AuthRequiredException;


public interface DeviceCommandClientInterface{
	

	public String giveCommand(int DeviceId, String func ,String value) throws ClientProtocolException, IOException,AuthRequiredException;
	public String getData(int DeviceId, String func) throws ClientProtocolException, IOException,AuthRequiredException;
	
}
