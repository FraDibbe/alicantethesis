package Client;

import java.io.IOException;
import java.util.List;

import Client.exception.AuthRequiredException;
import Pojos.Device;

public interface DeviceClientIf {

	public List<Device> getDevicesForService(String service) throws IOException, AuthRequiredException;
}
