package Client;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;


import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;



import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;

import parser.DeviceParseHandle;

import utils.rest.RestUtils;
import Client.exception.AuthRequiredException;
import Pojos.Device;



/* Client per i servizi RESTful di LOG
 * 
 * Apre le connessini HTTP con autenticazione
 * 
 * Invia le richieste e aspetta i risultati
 * 
 * Richiama il parser e crea gli oggetti JAVA POJO corrispondenti
 * 
 * */

public class DeviceClient implements DeviceClientIf, RESTClientIf{
	
	private URI basicURI; 
	private String _base64auth = null;
	
	public DeviceClient(String uri) throws URISyntaxException
	{
		this.basicURI = new URIBuilder()
					        .setScheme("http")
					        .setHost(uri+"home/")
					        //.setPath("/search")
					        .build();
	}

	
	public void setAuth(String username, String password)
	{
		_base64auth = RestUtils.base64encode(username + ":" + password);
	}

	@Override
	public List<Device> getDevicesForService(String service) throws IOException, AuthRequiredException {
		
		List<Device> result;
		
		//create the URI of the RESTful service
		String restURI="all/home-devices/service/"+service;
		System.out.println(basicURI+restURI);
		
		//request the service via GET
		CloseableHttpClient client=HttpClients.createDefault();
		CloseableHttpResponse response = executeRequest(client,basicURI+restURI);
		
		//read the response
		InputStream xmlStream = response.getEntity().getContent();
		result = parse(new InputSource(xmlStream));
		xmlStream.close();	
    	client.close();
		
    	//Parse and return a Log Object
    	return result;
	}


	
			
	private CloseableHttpResponse executeRequest(CloseableHttpClient client, String request) throws IOException, AuthRequiredException
    {		
		HttpGet httpget = new HttpGet(request);
		
		if (_base64auth != null)
			httpget.addHeader("Authorization", "Basic " + _base64auth);
	
		CloseableHttpResponse response = client.execute(httpget);

		int responseCode = response.getStatusLine().getStatusCode();
			
		if (responseCode == 401) {
			client.close();
			throw new AuthRequiredException("Server require authentication");
		}
		
		return response;
    }
		
	private List<Device> parse(InputSource source)
	{
		List<Device> result = null;
		
		try {
	    	SAXParserFactory spf = SAXParserFactory.newInstance();
	    	spf.setNamespaceAware(true);
			SAXParser sp = spf.newSAXParser();
			
			/* Get the XMLReader of the SAXParser we created. */
			XMLReader xr = sp.getXMLReader();
			
			/* Create a new ContentHandler and apply it to the XML-Reader*/ 
			DeviceParseHandle myDeviceHandler = new DeviceParseHandle();
			xr.setContentHandler(myDeviceHandler);
			
			/* Parse the xml-data from our stream */
			xr.parse(source);
	
			/* Our handler now provides the parsed data to us. */
			result = myDeviceHandler.getParsedData();
		}
		catch (Exception e) {
			System.err.println(e.getMessage());
		}
		
		return result;
	}

	

}
