package Client.exception;

public class AuthRequiredException extends Exception {
	
	public AuthRequiredException() {
		super();
	}
	
	public AuthRequiredException(String message) {
		super(message);
	}
	
	private static final long serialVersionUID = -2115339660001429396L;
}