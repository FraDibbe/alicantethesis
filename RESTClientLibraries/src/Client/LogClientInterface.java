package Client;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import org.apache.http.client.ClientProtocolException;

import Client.exception.AuthRequiredException;
import Pojos.Device;
import Pojos.Log;
import Pojos.Room;

public interface LogClientInterface {
	

	public List<Log> getTodayLog() throws ClientProtocolException, IOException,AuthRequiredException;
	public List<Log> getLog(Date start,Date end) throws IOException,AuthRequiredException;
	public List<Log> getLog(Date start,Date end, Device device)  throws IOException,AuthRequiredException;
	public List<Log> getLog(Date start,Date end, Room room) throws IOException, AuthRequiredException;


}
